# 1. PowerShellでSSHアクセスする方法

 目次

- [1. PowerShellでSSHアクセスする方法](#1-powershellでsshアクセスする方法)
  - [1.1. 概要説明](#11-概要説明)
  - [1.2. PowerShellでSSHアクセスする手順](#12-powershellでsshアクセスする手順)

## 1.1. 概要説明

 このファイルではWindows上のPowerShellでSSHアクセスをする方法を説明します。

## 1.2. PowerShellでSSHアクセスする手順

　PowerShellでSSHアクセスをするためには以下をOS:Windows[Windows 7以降標準搭載]上で実行してください。

- 1.Windows キーを押下すると検索窓が開きます。
- 2.「PowerShell」を検索する。
- 3.「Windows PowerShell＞開く」をクリックし選択する。
![検索窓上の「PowerShell」の検索結果](image.png)
![検索窓上の「PowerShell」をクリックした結果](image-1.png)
- 4.下記のコマンドを入力する。

```powershell
ssh -p [ssh ポート番号] [ユーザー名]@[IPアドレス/DNS名]
#例、sshポート番号:2221、ユーザー名:reki、IPアドレス:192.168.0.10の場合は以下
ssh -p 2221 reki@192.168.0.10
```

- 5.上記のコマンドを実行すると、Windows上でSSH接続できます。

以上

- [ ] [README.mdへ戻る](./../README.md)
