# 1. Debianを11から12へ変更する方法

目次  

- [1. Debianを11から12へ変更する方法](#1-debianを11から12へ変更する方法)
  - [1.1. 概要説明](#11-概要説明)
  - [1.2. Debian を 11 から 12 へ変更する手順](#12-debian-を-11-から-12-へ変更する手順)

## 1.1. 概要説明

 このファイルでは、Debianを11から12へ変更をする方法を説明します。  

## 1.2. Debian を 11 から 12 へ変更する手順

- 1. Debian 11 から 12 へ変更するには、以下の手順を実施してください。  

まず端末がWindoowsの場合は、以下の手順を実施してDebianへSSH接続できるようにします。  

- [ ] [PowerShellでSSHアクセスする方法](./002_PowerShellSSHAccess.md)  

Linuxの場合は以下です。  

- [ ] [LinuxにおいてSSHアクセスする方法](./004_LinuxSSHAccess.md)  

``` shell
#backupを取っておく
cp -pf /etc/apt/sources.list /etc/apt/sources.list.backup
#Debianを11から12へ変更する設定
sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list
#Debianを11から12へ変更のためにパッケージを更新
apt update -y
#Debianを11から12へ変更のためにパッケージのinstall
apt upgrade -y

```

「libc6:amd64 を設定」画面がでますが、はいを選択します。
![「libc6:amd64 を設定」画面](image-2.png)

しばらく待ちます。

以下のエラーが発生した場合は、以下の手順を実施してください。

``` shell
処理中にエラーが発生しました:
 polkitd
 polkitd-pkla
 pkexec
 gdm3
 packagekit
 policykit-1
 packagekit-tools
 rtkit
 colord
 network-manager
 software-properties-common
 malcontent
 gnome-software
 malcontent-gui
 synaptic
 gstreamer1.0-packagekit
 gnome-color-manager
 modemmanager
 network-manager-gnome
 software-properties-gtk
E: Sub-process /usr/bin/dpkg returned an error code (1)

#解決策
#apt update
ヒット:1 http://security.debian.org/debian-security bookworm-security InRelease
ヒット:2 http://deb.debian.org/debian bookworm InRelease
ヒット:3 http://deb.debian.org/debian bookworm-updates InRelease
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています... 完了
状態情報を読み取っています... 完了
アップグレードできるパッケージが 94 個あります。表示するには 'apt list --upgradable' を実行してください。
N: Repository 'Debian bookworm' changed its 'non-free component' value from 'non-free' to 'non-free non-free-firmware'
N: More information about this can be found online in the Release notes at: https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.html#non-free-split
#↑の通り,'non-free' to 'non-free non-free-firmware'へ変更する
nano /etc/apt/source.list

sed -i 's/non-free/non-free non-free-firmware/g' /etc/apt/sources.list

#以下のようにWarningメッセージが出なければOK.
 apt update
ヒット:1 http://deb.debian.org/debian bookworm InRelease
ヒット:2 http://deb.debian.org/debian bookworm-updates InRelease
ヒット:3 http://security.debian.org/debian-security bookworm-security InRelease
取得:4 http://deb.debian.org/debian bookworm-updates/non-free-firmware Sources [2,076 B]
取得:5 http://deb.debian.org/debian bookworm-updates/non-free-firmware amd64 Packages [616 B]
取得:6 http://deb.debian.org/debian bookworm-updates/non-free-firmware Translation-en [384 B]
取得:7 http://security.debian.org/debian-security bookworm-security/non-free-firmware Sources [796 B]
取得:8 http://security.debian.org/debian-security bookworm-security/non-free-firmware amd64 Packages [688 B]
取得:9 http://security.debian.org/debian-security bookworm-security/non-free-firmware Translation-en [472 B]
5,032 B を 0秒 で取得しました (11.6 kB/s)
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています... 完了
状態情報を読み取っています... 完了
アップグレードできるパッケージが 94 個あります。表示するには 'apt list --upgradable' を実行してください。

#アップグレードを試す　エラーが出ても気にしない
sudo apt upgrade --without-new-pkgs
#完全なアップグレードを実行する
sudo apt full-upgrade
#しばらく待つ
処理中にエラーが発生しました:
 polkitd
 polkitd-pkla
 pkexec
 gnome-initial-setup
 gdm3
 packagekit
 policykit-1
 gnome-core
 packagekit-tools
 rtkit
 colord
 gnome
 network-manager
 software-properties-common
 malcontent
 gnome-software
 malcontent-gui
 synaptic
 gstreamer1.0-packagekit
 gnome-color-manager
 modemmanager
 gnome-control-center
 network-manager-gnome
 software-properties-gtk
E: Sub-process /usr/bin/dpkg returned an error code (1)
#変わらないので以下で調査する
##パッケージの状態を確認する
dpkg --audit
以下のパッケージは展開されましたが、まだ設定されていません。
これらのパッケージが正常に動作するためには、dpkg --configure か
dselect の設定 (configure) メニューオプションを使って設定を完了
させなければなりません:
 colord               system service to manage device colour profiles -- system
 gdm3                 GNOME Display Manager
 gnome                Full GNOME Desktop Environment, with extra components
 gnome-color-manager  Color management integration for the GNOME desktop enviro
 gnome-control-center utilities to configure the GNOME desktop
 gnome-core           GNOME Desktop Environment -- essential components
 gnome-initial-setup  Initial GNOME system setup helper
 gnome-software       Software Center for GNOME
 gstreamer1.0-packagekit GStreamer plugin to install codecs using PackageKit
 malcontent           framework for parental control of applications
 malcontent-gui       GUI to configure malcontent
 modemmanager         D-Bus service for managing modems
 network-manager      network management framework (daemon and userspace tools)
 network-manager-gnome network management framework (GNOME frontend)
 packagekit           Provides a package management service
 packagekit-tools     Provides PackageKit command-line tools
 pkexec               run commands as another user with polkit authorization
 policykit-1          transitional package for polkitd and pkexec
 polkitd-pkla         Legacy "local authority" (.pkla) backend for polkitd
 rtkit                Realtime Policy and Watchdog Daemon
 software-properties-common manage the repositories that you install software f
 software-properties-gtk manage the repositories that you install software from
 synaptic             Graphical package manager

以下のパッケージは最初の設定中に問題が発生したため、設定が終了していません。
dpkg --configure <パッケージ> か dselect で設定 (configure) メニューオプショ
ンを使って設定作業を再試行しなければなりません:
 polkitd              framework for managing administrative policies and privil
##個別のパッケージの設定を再構成する
#フォーマット
dpkg --configure [パッケージ名]
##今回は以下を実施
dpkg --configure polkitd
polkitd (122-3) を設定しています ...
Failed to check if group polkitd already exists: Connection refused
id: `polkitd': そのようなユーザは存在しません
chown: ユーザ指定が不正: `polkitd:root'
dpkg: パッケージ polkitd の処理中にエラーが発生しました (--configure):
 installed polkitd package post-installation script subprocess returned error exit status 1
処理中にエラーが発生しました:
 polkitd
##問題のあるパッケージのスクリプトを削除する
##/var/lib/dpkg/info ディレクトリ内の該当するパッケージの .postinst、.postrm、.prerm ファイルを削除してから再度設定を試してみてください。
##つまり以下のフォーマットで調査
ls /var/lib/dpkg/info | grep [パッケージ名]
##今回は以下を実施
ls /var/lib/dpkg/info | grep polkitd
polkitd-pkla.list
polkitd-pkla.md5sums
polkitd-pkla.postinst
polkitd.list
polkitd.md5sums
polkitd.postinst
polkitd.postrm
polkitd.preinst
polkitd.prerm

rm /var/lib/dpkg/info/polkitd.postinst /var/lib/dpkg/info/polkitd.postrm /var/lib/dpkg/info/polkitd.prerm
##再度個別のパッケージの設定を再構成する
dpkg --configure polkitd
polkitd (122-3) を設定しています ...
##問題ないので以下を実施

#完全なアップグレードを実行する
sudo apt full-upgrade

#しばらく待つ
処理中にエラーが発生しました:
 polkitd-pkla
E: Sub-process /usr/bin/dpkg returned an error code (1)

##先ほどと同じく調査
# dpkg --audit
以下のパッケージは最初の設定中に問題が発生したため、設定が終了していません。
dpkg --configure <パッケージ> か dselect で設定 (configure) メニューオプショ
ンを使って設定作業を再試行しなければなりません:
 polkitd-pkla         Legacy "local authority" (.pkla) backend for polkitd

dpkg --configure polkitd-pkla
polkitd-pkla (122-3) を設定しています ...
chown: グループ指定が不正: 'root:polkitd'
dpkg: パッケージ polkitd-pkla の処理中にエラーが発生しました (--configure):
 installed polkitd-pkla package post-installation script subprocess returned error exit status 1
処理中にエラーが発生しました:
 polkitd-pkla
ls /var/lib/dpkg/info | grep polkitd-pkla
polkitd-pkla.list
polkitd-pkla.md5sums
polkitd-pkla.postinst
rm /var/lib/dpkg/info/polkitd-pkla.postinst
dpkg --configure polkitd-pkla
#完全なアップグレードを実行する
sudo apt full-upgrade
#エラーがでなければOK！
```

以上

- [ ] [README.mdへ戻る](./../README.md)  
