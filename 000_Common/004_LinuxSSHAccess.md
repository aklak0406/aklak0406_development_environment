# 1. LinuxにおいてSSHアクセスする方法

目次

- [1. LinuxにおいてSSHアクセスする方法](#1-linuxにおいてsshアクセスする方法)
  - [1.1. 概要説明](#11-概要説明)
  - [1.2. Linuxからsshアクセスする手順](#12-linuxからsshアクセスする手順)

## 1.1. 概要説明

 このファイルでは、端末Linuxからsshアクセスへ変更をする方法を説明します。  

## 1.2. Linuxからsshアクセスする手順

LinuxでSSHアクセスをするためには以下実行してください。  

- 1.GUIならterminalを開きます。CUIであればすでにterminalを開いている状態ですのでそのままで問題はありません。  
- 2.下記のコマンドを入力する。  

```shell
ssh -p [ssh ポート番号] [ユーザー名]@[IPアドレス/DNS名]
#例、sshポート番号:2221、ユーザー名:reki、IPアドレス:192.168.0.10の場合は以下
ssh -p 2221 reki@192.168.0.10
```

- 3.上記のコマンドを実行すると、Linux上でSSH接続できます。  

以上

- [ ] [README.mdへ戻る](./../README.md)
