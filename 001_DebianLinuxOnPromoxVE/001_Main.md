# 1. DebianLinuxOn上でPromoxVEを運用する

- [ ] [README.mdへ戻る](./../README.md)

## 1.1. 目次

- [1. DebianLinuxOn上でPromoxVEを運用する](#1-debianlinuxon上でpromoxveを運用する)
  - [1.1. 目次](#11-目次)
  - [1.2. 概要説明](#12-概要説明)
  - [1.3. PromoxVEの構成図](#13-promoxveの構成図)
  - [1.4. 表紙](#14-表紙)
  - [以下はいずれ記載する。](#以下はいずれ記載する)
  - [nano temp.](#nano-temp)
- [画像ファイルのパスを指定](#画像ファイルのパスを指定)
- [画像を読み込む](#画像を読み込む)
- [x1, y1 = parse\_file('temp.log.1')](#x1-y1--parse_filetemplog1)
- [x2, y2 = parse\_file('temp.log.2')](#x2-y2--parse_filetemplog2)
- [x3, y3 = parse\_file('temp.log')](#x3-y3--parse_filetemplog)
- [plt.plot(x2, y2, label='CPUファン強制停止')](#pltplotx2-y2-labelcpuファン強制停止)
- [plt.plot(x3, y3, label='CPUファン強制停止 + 熱伝導シート')](#pltplotx3-y3-labelcpuファン強制停止--熱伝導シート)
- [plt.show()](#pltshow)
- [画像をプロット](#画像をプロット)
- [画像を保存](#画像を保存)
- [fdisk -l /dev/sdb](#fdisk--l-devsdb)

## 1.2. 概要説明

 Debian GNU/Linux 11を使用してPromoxVEという仮想化のアプライアンスを物理サーバまたはVMへインストール/初期設定/LXC/アプリケーション/セキュリティ/ユーザ管理/構成管理/ファイル管理/PromoxVEのUpgradeを示す。
PromoxVEとはKVM/QEMU/LXCを用いたDebianのディスストリビューションをベースにしたOSSの仮想化アプライアンスである。
商用利用は専用の契約とサブスクパッケージの追加が必要だが、なくても通常のDebianの範囲内+LXC/QEMUの管理コンソール程度なら無償利用できる。

## 1.3. PromoxVEの構成図

```mermaid
%%flowchart LR
graph RL

%%グループとサービス
subgraph GC[物理PC or VirtualBox]
  subgraph GV[OS:Debian11 on PromoxVE★これがインストールの担務箇所]
    subgraph GS1[LXCコンテナ]
      LXC1("例<br>On Docker<br>Continer<br>Ubuntsu<br>20.04")
    end
    subgraph GS2[KVM/QEMU仮想マシン]
      VM1("例 VM<br>Fedora<br>17")
    end
  end
end

%%サービス同士の関係
GS1 --- GS2

%%グループのスタイル
classDef SGC fill:none,color:#345,stroke:#345
class GC SGC

classDef SGV fill:none,color:#0a0,stroke:#0a0
class GV SGV

classDef SGPrS fill:#def,color:#07b,stroke:none
class GS2 SGPrS

classDef SGPuS fill:#efe,color:#092,stroke:none
class GS1 SGPuS

%%サービスのスタイル
classDef SOU fill:#aaa,color:#fff,stroke:#fff
class OU1 SOU

classDef LXC fill:#84e,color:#fff,stroke:#none
class LXC1 LXC

classDef SCP fill:#e83,color:#fff,stroke:none
class VM1 SCP

 ```

## 1.4. 表紙

- [ ] [インストール](./002_Install.md)

- [ ] [初期設定](./003_Init.md)

- [ ] [LXC](./004_LXC.md)

以下はいずれ記載する。
---
ベンチマークでCPU温度を確認する。

su -
cd tmp
#環境インストール
apt install python3-full fonts-noto-cjk
pip3 install matplotlib pillow
python3 -m venv venv

nano temp.
---
import subprocess
import sys
from time import sleep, time
from datetime import datetime
import json

""" CPU温度と周波数を計測、記録する """
def main():
    duration = int(sys.argv[1])
    starttime = time()
    while time() - starttime < duration:
        now = datetime.now().strftime("%H:%M:%S")
        temps = coretemp()
        freqs = cpufreq()
        avgtemp = sum(temps)/len(temps)
        maxtemp = max(temps)
        avgfreq = round(sum(freqs)/len(freqs), 2)
        maxfreq = max(freqs)
        print(now, avgtemp, maxtemp, avgfreq, maxfreq)
        sleep(3)


def coretemp():
    sensorlog = json.loads(subprocess.check_output(["sensors", "-j", "coretemp-isa-0000"]))
    coretemp = sensorlog["coretemp-isa-0000"]
    temperatures = [
        coretemp["Core 0"]["temp2_input"],
        coretemp["Core 1"]["temp3_input"],
        coretemp["Core 2"]["temp4_input"],
        coretemp["Core 3"]["temp5_input"]
    ]
    return temperatures


def cpufreq():
    freqs = []
    with open("/proc/cpuinfo") as fp:
        for row in fp:
            if row.startswith("cpu MHz"):
                freqs.append(float(row.strip().split(":")[1]))
    return freqs


if __name__ == "__main__":
    main()
---
nano parse_temp.py
ーーー
""" 計測結果のグラフ化に使用したスクリプト """

from datetime import datetime
import sys
from matplotlib import rcParams
import matplotlib.pyplot as plt
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Hiragino Maru Gothic Pro', 'Yu Gothic', 'Meirio', 'Takao', 'IPAexGothic', 'IPAPGothic', 'VL PGothic', 'Noto Sans CJK JP']

from matplotlib.image import imread

def main():

# 画像ファイルのパスを指定
#img_path = 'temp.jpg'
# 画像を読み込む
#img = imread('temp.jpg')

#    x1, y1 = parse_file('temp.log.1')
    x1, y1 = parse_file('temp.log')
#    x2, y2 = parse_file('temp.log.2')
#    x3, y3 = parse_file('temp.log')

    plt.plot(x1, y1, label='CPUファン稼働(購入時の状態)')
 #   plt.plot(x2, y2, label='CPUファン強制停止')
 #   plt.plot(x3, y3, label='CPUファン強制停止 + 熱伝導シート')
    plt.title('CPU温度(℃): sysbench 4スレッド900秒 + 無負荷300秒')
    plt.xlabel('経過時間(秒)')
    plt.ylabel('CPU温度(℃)')
    plt.legend()
    plt.grid()
#    plt.show()
# 画像をプロット
#plt.imshow(img)
# 画像を保存
    plt.savefig('temp_t2.jpg')


def parse_file(filename):
    x = []
    y = []
    with open(filename) as fp:
        fields = fp.readline().split(' ')
        time_start = datetime.strptime(fields[0], '%H:%M:%S')
        x.append(0)
        y.append(float(fields[2]))
        for row in fp:
            fields = row.strip().split(' ')
            timestamp = datetime.strptime(fields[0], '%H:%M:%S')
            x.append((timestamp - time_start).seconds)
            y.append(float(fields[2]))
    return x, y


if __name__ == '__main__':
    main()
ーーー

参考HP①
https://gist.github.com/nknytk/424c50b38ad16e9059e4aef6014da5ec

#実行する
source venv/bin/activate
sysbench cpu --threads=4 --time=900 run > perf.log & disown
python3 -u temp.py 1200 | tee temp.log
python3 -u parse_temp.py
#出力したCPU温度の画像をアクセスできる場所へ移動。
cp temp_t2.jpg /home/reki

---
---
USBメモリをddコマンドでディスク丸ごとバックアップする方法

例.Debian 12のrootfilesystem一式backupしたい。

#1st Step:下記のコマンドでディスクの空き容量を把握する
df -h
今回はざっくり900GBくらいを0で空いたスペースを埋めたい
#2nd Step: ddコマンドで空き容量を確認する
$ fallocate -l 900G example
#3rd Step:電源を切り、USBメモリを抜く
#4th Step:別のPC[Linux]へ挿入する
#5th Step:ddコマンドbackupしつつ圧縮する
# fdisk -l /dev/sdb
Disk /dev/sdb: 953.87 GiB, 1024209543168 bytes, 2000409264 sectors
Disk model: SSD-PUT/N
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 6F33178F-47FE-49D1-B569-CA3C66D7A4B3

apt-get install pigz
dd if=/dev/sdb bs=16MB iflag=nocache oflag=nocache,dsync | pv | pigz > /home/reki/download/deb12_20240519_GPDPOCKET_bkp.image.gz
2.42GiB 0:00:29 [65.5MiB/s] 
上のように進捗まででてくる...便利.

---
GPDへpromoxveをインストール
echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg

sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list

sed -i -e 's/bullseye/bookworm/g' /etc/apt/sources.list.d/pve-install-repo.list

apt update -y && apt dist-upgrade -y

ip -aでiPアドレス確認
/etc/hostsを変更 192.168.1.136はＧＰＤのＩＰアドレス
127.0.1.1      GPD
↓
#127.0.1.1      GPD
192.168.1.136 GPD GPD

---
Promoxのネットワーク設定

---

- [ ] [README.mdへ戻る](./../README.md)
