# インストールする

- [ ] [DebianLinuxOn上でPromoxVEを運用する FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [インストールする](#インストールする)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [Debianのインストール](#debianのインストール)
    - [Install用のディスクもしくはUSBメモリを作成する](#install用のディスクもしくはusbメモリを作成する)
    - [作成したディスクもしくはUSBメモリで起動させる](#作成したディスクもしくはusbメモリで起動させる)
    - [Debianのインストール画面に従い、インストールする](#debianのインストール画面に従いインストールする)

## 概要説明

 Debian GNU/Linux 11/PromoxVEのインストールをする

## Debianのインストール

Debian GNU/Linux 11/PromoxVEをインストールするには、以下の手順を実施する必要があります。

- Debian GNU/Linux 11のインストールをする。
  - Install用のディスクもしくはUSBメモリを作成する。
  - 作成したディスクもしくはUSBメモリで起動させる。
  - Debianのインストール画面に従い、インストールする。

### Install用のディスクもしくはUSBメモリを作成する

下記のWebサイトがDebianの最新版になる。
<https://get.debian.org/debian-cd/current-live/amd64/iso-hybrid/>

↑の現時点の執筆時点[2024/4/20]では以下が最新。
<https://get.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.5.0-amd64-standard.iso>

Debian 11をDownloadする場合は以下を参照
<https://www.debian.org/releases/bullseye/debian-installer/>
↑のIntel/AMD製のCPUなら「full DVD sets」の「amd64」をクリックしてDownloadする。
↑の現時点の執筆時点[2024/4/20]では以下が最新。
<https://cdimage.debian.org/cdimage/archive/11.9.0/amd64/iso-dvd/debian-11.9.0-amd64-DVD-1.iso>

以下はLinuxマシンでのUSBメモリのInstall方法

```shell
#ISO Image Downloadする
wget https://cdimage.debian.org/cdimage/archive/11.9.0/amd64/iso-dvd/debian-11.9.0-amd64-DVD-1.iso

#USBメモリを挿入する前に以下実行
lsblk
#現在のdisk[例./deb/sda]を把握する
#USBメモリをLinux PCへ挿入する
#再度以下を実行
lsblk
#USBメモリのDiskを把握する。[例/dev/sdb]
#以下のコマンドでDebianのISOイメージ[debian-11.9.0-amd64-DVD-1.iso]をUSBメモリ[例/dev/sdb]へInstall
sudo dd if=debian-11.9.0-amd64-DVD-1.iso of=/dev/sdb bs=4096
#しばらく終わるまで待つ
#特にエラー等なければOK！
```

Windowsの場合は以下のサイトを参照してUSBメモリにインストールすればよい。
<https://www.si-linux.co.jp/techinfo/index.php?DD%20for%20Windows>

### 作成したディスクもしくはUSBメモリで起動させる

インストール対象のPCへUSBメモリを挿入する。
ＰＣの電源をＯＮ。
ＰＣの起動ディスクをＵＳＢメモリを指定する。
↑の具体的な手順はＰＣのマザーボードによって異なるため、各ＰＣのマニュアルを参照してもらうことで説明を割愛するが参考までに一例を画像ベースで載せておく

一例. ASUSのマザーボード
DEL キーもしくは F2 キーを打鍵し, UEFI BIOS セットアップメニューを起動 (一瞬で画面が切り替わるため, DEL もしくは F2 キーを連打すると良い).
![alt text](image.png)
「ブートメニュー(F8)」をクリック (起動順位を設定しなくても直接ブートすることが可能となる).
![alt text](image-1.png)
「ブートメニュー」が表示されたら, 起動に使用するメディアを選択し, クリック (今回は USBメモリなので, "UEFI : UFD 2.0 Silicon-Power4G (3748MB)").
![alt text](image-2.png)
インストール画面が起ち上がるので, インストールメニューの[graphical install] を選択して Enter を押下する
![alt text](image-3.png)

### Debianのインストール画面に従い、インストールする

「Japanese 日本語」 を選択
![alt text](image-4.png)

場所の選択　「日本」を選択
![alt text](image-5.png)
キーボードの設定　「日本」を選択
![alt text](image-6.png)
ネットワークの設定　自動でネットワークの設定が始まるので自身にあったネットワーク設定を行う。以下は一例
![alt text](image-7.png)
ユーザとパスワードのセットアップ
![alt text](image-8.png)
![alt text](image-9.png)
![alt text](image-10.png)
![alt text](image-11.png)
ディスクのパーティショニング
![alt text](image-12.png)
ソフトウェアの選択
![alt text](image-13.png)
![alt text](image-14.png)

以上で終了です。
再起動して、USBメモリを抜いた上で起動することを確認してください。

よく使用するsshサーバは有効にしておく。
GUIは基本使用しないのでCLIに変更

```shell
#管理者になる
su -
#今の設定を確認
systemctl get-default
#変更する
systemctl set-default multi-user.target
```

- [ ] [DebianLinuxOn上でPromoxVEを運用する FIle:001_Main.mdへ戻る](./001_Main.md)
