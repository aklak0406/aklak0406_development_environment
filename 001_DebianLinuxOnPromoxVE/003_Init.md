# 1. 初期設定

- [ ] [DebianLinuxOn上でPromoxVEを運用する FIle:001_Main.mdへ戻る](./001_Main.md)

## 1.1. 目次

- [1. 初期設定](#1-初期設定)
  - [1.1. 目次](#11-目次)
  - [1.2. 概要説明](#12-概要説明)
  - [1.3. Promox VE化する](#13-promox-ve化する)

## 1.2. 概要説明

sshでログインする。
特に説明はしないが、[Installする](./002_Install.md)で設定した一般ユーザでログインしたうえで作業すること
またInstall後の初期設定では管理者権限も必要なのでrootユーザのパスワードも予め控えておくこと

## 1.3. Promox VE化する

```shell
#Promox VEのapt リポジトリを登録する。
echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

#リポジトリキーを追加
wget https://enterprise.proxmox.com/debian/proxmox-release-bullseye.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg

#リポジトリとシステム更新を行う。
apt update && apt full-upgrade

#Promox VEのパッケージをインストールする
apt install proxmox-ve postfix open-iscsi

#下記の失敗
pve-ha-manager (3.6.1) のトリガを処理しています ... 処理中にエラーが発生しました: pve-manager proxmox-ve E: Sub-process /usr/bin/dpkg returned an error code (1)

#確認
#デーモン確認、どうやらOK。
root@deb:~#  systemctl status pvedaemon.service
● pvedaemon.service - PVE API Daemon
     Loaded: loaded (/lib/systemd/system/pvedaemon.service; enabled; vendor pre>
     Active: active (running) since Sun 2023-06-04 17:02:23 JST; 1min 48s ago
   Main PID: 25470 (pvedaemon)
      Tasks: 4 (limit: 23928)
     Memory: 135.0M
        CPU: 825ms
     CGroup: /system.slice/pvedaemon.service
             tq25470 pvedaemon
             tq25471 pvedaemon worker
             tq25472 pvedaemon worker
             mq25473 pvedaemon worker
#デーモンのログでエラーでている。
journalctl -xe
・
・略
・
 6月 04 17:04:27 deb pveproxy[25868]: worker 39616 started
 6月 04 17:04:27 deb pveproxy[39616]: /etc/pve/local/pve-ssl.key: failed to load local private key (key_file or key) at /usr/share/perl5/PVE/APIServer/AnyEvent.pm line 1998.　←
#↑を検索するとpve-clusterが起動していないのではとわかる。
 以下pve-clusterの結果
root@deb:~#  systemctl status -l pve-cluster
● pve-cluster.service - The Proxmox VE cluster filesystem
     Loaded: loaded (/lib/systemd/system/pve-cluster.service; enabled; vendor preset: enabled)
     Active: failed (Result: exit-code) since Sun 2023-06-04 17:02:31 JST; 3min 16s ago
    Process: 25867 ExecStart=/usr/bin/pmxcfs (code=exited, status=255/EXCEPTION)
        CPU: 8ms

#さらに以下のエラーメッセージで検索すると...
/etc/pve/local/pve-ssl.key: failed to load local private key (key_file or key) at /usr/share/perl5/PVE/APIServer/AnyEvent.pm line 1998.
以下のページのエラーが参考になるとわかった。
<https://forum.proxmox.com/threads/etc-pve-local-pve-ssl-key-failed-to-load-local-private-key-key_file-or-key-at-usr-share-perl5.48943/>

#解決策
#具体的にはnano /etc/hostsで編集し、以下をコメントアウト
127.0.1.1       deb.debhost     deb
↓
#127.0.1.1      deb.debhost     deb
10.0.2.15 deb.debhost.promox.com deb
#↑のIPアドレスはVirtualboxのDHCPが割当たっているIPアドレス。
#物理マシンなら、物理サーバに割当たるIPアドレスでOK。
#↑のIPアドレスが返ってくることを確認
hostname --ip-address
10.0.2.15

#再度トライ
apt install proxmox-ve postfix open-iscsi

#systemctl status -l pve-clusterで以下になっていればOK
Active: active (running) since Sun 2023-06-04 17:07:49 JST; 9s ago

#再起動
reboot
#IP割当たらず、接続できなくなった。
nano /etc/network/interfaces
iface enp0s3 inet manual
↓
#iface enp0s3 inet manual
iface enp0s3 inet dhcp
systemctl restart ifup@enp0s3
#接続OK

#ifupしないので以下を変更
nano /etc/NetworkManager/NetworkManager.conf
[ifupdown]
managed=false
↓
managed=true

#もしくは直接/etc/network/interfacesを編集してもよい。

#↑でできなければ以下の公式ページ通りにする。
<https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_11_Bullseye>

#以下のWebUIへアクセスする。
https://127.0.0.1:8006

#初回はrootユーザでアクセスすること。ログイン後ユーザ作成すればそのユーザでもアクセス可能。

```

- [ ] [DebianLinuxOn上でPromoxVEを運用する FIle:001_Main.mdへ戻る](./001_Main.md)
