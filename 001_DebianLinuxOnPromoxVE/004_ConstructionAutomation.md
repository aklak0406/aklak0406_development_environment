# 1. 環境構築を自動化する

- [ ] [README.mdへ戻る](./../README.md)

## 1.1. 目次

- [1. 環境構築を自動化する](#1-環境構築を自動化する)
  - [1.1. 目次](#11-目次)
  - [1.2. 概要説明](#12-概要説明)
  - [1.3. 前提条件](#13-前提条件)
    - [1.3.1. 前提条件の検討](#131-前提条件の検討)
  - [1.4. Ansibleのサーバを構築・動作](#14-ansibleのサーバを構築動作)
    - [1.4.1. OracleのVirtualBoxで環境をインストール](#141-oracleのvirtualboxで環境をインストール)
    - [1.4.2. Dockerコンテナをインストール](#142-dockerコンテナをインストール)
    - [1.4.3. AnsibleのサーバをDockerコンテナで構築(チュートリアル)](#143-ansibleのサーバをdockerコンテナで構築チュートリアル)
    - [1.4.4. AnsibleのサーバをDockerコンテナで構築(リモートに対して)](#144-ansibleのサーバをdockerコンテナで構築リモートに対して)
  - [1.5.本番運用について](#15本番運用について)
  - [1.5.1.ProxmoxVEの構築](#151proxmoxveの構築)
  - [1.5.1.1.ProxmoxVE内でOpenwrt構築](#1511proxmoxve内でopenwrt構築)
  - [1.5.1.2.ProxmoxVE内でWazuh構築](#1512proxmoxve内でwazuh構築)
  - [1.6.まとめ](#16まとめ)

## 1.2. 概要説明

現在運用している環境は以下で、構築手順は手作業です。  
・ProxmoxVE  
・Openwrtのインストール  
・Yocto環境の構築  
・Buildroot環境の構築  
・jenkins環境の構築  
etc.  
私は時々環境作り直したりする場合にメモや思い出したりして構築していますが、記載を忘れてしまったので再構築ができないことがあります。  
かつて、一度は構築できたはずなのに...  

少なくとも、OSのインストール、ドライバーインストールは自動では困難なのでこれは自分自身でやりますが、その後のパッケージインストール、OSアップデート、アップグレード等の手順は自動化できるはずです。  

## 1.3. 前提条件

前提条件の検討[#131-前提条件の検討](#131-前提条件の検討)より、以下の条件を満たす必要があります。  

- scriptにより自動化  
- 非対話的に実行  
- 導入の容易化  

### 1.3.1. 前提条件の検討

  まず、何に困っているのか確認していきましょう。  
自動化といえばコマンドをまとめてscriptにすることが一般的です。  
大抵の作業ではscript化すれば問題ないでしょう。  
ただ、script化されない多くの作業では以下のような課題があることが多いです。  

- ①GUIしか利用しない操作はscript化できない  
- ②コマンドが対話的に実行する必要があるので、script化が難しい  
- ③script化するノウハウがないまたはscript化はできるが、後継者不足でメンテナンスができない  

①はもちろんGUIをコマンドラインベースに置き換えできないか検討・調査は必要ですが、一般的でないアプリケーションの場合、特に商用アプリケーションは古いものはGUI前提であることが多いです。  
しかし、最近だと商用アプリケーションでもAPI化して、curlなどのコマンドラインベースに置き換えることができる場合も多いですので、まずは検討・調査できないかを考えてみてください。  

この自動化できるポイントは何か？ツール観点で検討してみましょう。  

- Ansible
  特徴:冪等性(べきとうせい) [ある操作を1回実行しても複数回実行しても同じ結果になる性質です]を担保するように作られています。  
  管理対象サーバに対して「何を実行するのか」ではなく、「どのような状態があるべき姿なのか」ということを記述します。  
  エージェントレスなため、基本的にPythonが使用可能でSSHの通信ができればよい  
  Ansibleにおける一連の処理はPlaybookという単位にまとめられ、PlaybookはYAML形式で記述される  
  モジュールにより、AWS/Azure/GCP/OpenStackをはじめとするクラウド関連モジュール等の拡張が可能  
  サーバ対応OS:Linux debianやubuntsuなど  
  Ansibleの設定先のOSはLinux Debian UbuntsuなどはもとよりOpenwrt,Windowsも可能  
  しかし、設定内容の中身であるOS固有値は対応できないので、設定内容でOS差分はある。  

- Jenkins
  特徴:CI/CDのためのツールです。よくテスト自動化に利用され、最近ではソフトのbuild自動化からテスト自動化そして運用へ投入するまでに利用されたりしています。  
  環境を一から構築するようにシナリオ設定すれば可能です。  
  より効果てきに利用する場合は、JenkinsのAgentツールを自動化させたい対象へインストールする必要があります。  
  もしくはsshなどのコンソール設定をすれば設定可能です。  
  また、自動化する環境次第ですが、環境構築がさらに必要な場合もありますので、何を自動化したいのかしっかり見定めることが必要です。  

基本的に上のツールで共通することは、自動化するポイントが異なるということです。  
AnsibleはMW周りの構築自動化に特化したツールであり、JenkinsはCI/CDのためのツールです。また、DockerやVagrantなどのコンテナや仮想環境を自動構築するツールもありますが、それぞれ、自動化する前に共通的にソフトのインストールなどが必要になります。  

この中で一番簡単にOSインストール後、環境構築に向いているものはAnsibleでしょう。  
頻繁になんども環境構築をやり直すのであれば、Ansibleはよいツールです。  
ただ、Ansibleを利用したいケースは例えば、従業員へ提供するＰＣの設定を一律同じ設定にして提供したいとか台数が多い場合に有効です。  
正直、個人レベルであれば、早々同じ環境を頻繁にやり直すことは多くないため、Ansibleは必要ないです。  

また、Ansibleと同じようなツールとして、ChefやPuppetなどもあります。
これらは専用のAgentツールが必要であるため、サーバとAgentの保守が必要になります。  
主にバージョンを合わせる必要があります。  

## 1.4. Ansibleのサーバを構築・動作

Ansibleのサーバを構築してみましょう。  
このサーバを構築することで、インフラ再構築が軽減化します。  

作業の軽減が目的なので、DockerコンテナとしてAnsibleのサーバを構築していきます。  

### 1.4.1. OracleのVirtualBoxで環境をインストール  

目的:VirtualBoxにてDebian系の環境を構築し、その後Dockerコンテナ環境を作るために必要なものをインストールします。  

Host側はWindows環境のため、以下をDownloadします。  
最新のDebianのnetinstのISOイメージです。  
<https://www.debian.org/CD/netinst/#netinst-stable>  
amd64を選択しDownloadします。  

今回は「debian-12.7.0-amd64-netinst.iso」でした。  

その後以下のサイトの手順通り、Oracle VM VirtualboxをDownloadして、installします。  
<https://qiita.com/HITOSUKE/items/fd6975420ddc6d336405>  

仮想マシンの作成の通り、VirtualBoxへ「debian-12.7.0-amd64-netinst.iso」をインストールします。  
<https://qiita.com/HITOSUKE/items/5cc5c6a75e88317796f3>  

``` shell

su -
apt update
apt upgrade
#install後,sshサーバへ接続したいのでinstall
apt install openssh-server

```

### 1.4.2. Dockerコンテナをインストール  

Dockerコンテナをインストールします。  
下記を参照してインストールします。  
<https://docs.docker.com/engine/install/debian/>

### 1.4.3. AnsibleのサーバをDockerコンテナで構築(チュートリアル)  

AnsibleのサーバをDockerコンテナで構築し、localhost[自分自身]で実行します。  
localhostで行う理由はまずは、Dockerコンテナを実行するための環境を作成するためです。  
このシナリオではlocalhostに対してFile:/tmp/test.txtに「"Hello, World!"」が書き込まれていることを確認します。  

``` shell
#作業用ディレクトリを作成
mkdir -p DCK/Ansible
#作業用ディレクトリへ移動
cd DCK/Ansible

#Dockerfileを作成
cat <<EOF > Dockerfile
FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y ansible && \
    apt-get clean

WORKDIR /root

COPY test.yml /root/test.yml

CMD ["ansible-playbook", "-i", "localhost,", "-c", "local", "/root/test.yml"]
EOF

#Ansibleのplaybookを作成
cat <<EOF > test.yml
---
- name: Test Ansible Playbook
  hosts: localhost
  tasks:
    - name: Write "Hello, World!" to the file
      ansible.builtin.copy:
        dest: /tmp/test.txt
        content: "Hello, World!"
EOF

#Dockerコンテナを構築
docker build -t ansible-container .
#Dockerコンテナの実行
docker run --name ansible-container ansible-container
#コンソール出力結果

PLAY [Test Ansible Playbook] ***************************************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [Write "Hello, World!" to the file] ***************************************
changed: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

#上ではわからないので、dockerコンテナにログインしながらやってみる。
docker run -it ansible-container bash
ansible-playbook -i localhost -c local /root/test.yml
[WARNING]: Unable to parse /root/localhost as an inventory source
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [Test Ansible Playbook] *********************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************************************
ok: [localhost]

TASK [Write "Hello, World!" to the file] *********************************************************************************************************************************************
changed: [localhost]

PLAY RECAP ***************************************************************************************************************************************************************************
localhost                  : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

# cat /tmp/test.txt
Hello, World!root@a42fcf408529:~#
#上でHello, World!がFile:/tmp/test.txtに書き込まれているのを確認できた。
```

### 1.4.4. AnsibleのサーバをDockerコンテナで構築(リモートに対して)  

　今度はリモートのPC[dockerコンテナ]を用意して、AnsibleのDockerコンテナからリモートのPCに対してAnsibleを実行できるようにしていきます。  

この操作ができるようになれば、AnsibleサーバにあるPlaybook通りにリモートのPCへコマンドを送信し、パッケージのインストール、設定ファイル等の書き込みができます。

まず、リモートのPC用のDockerコンテナを作成します。  
このコンテナはsshサーバとpythonをインストールしておきます。  

``` shell
cd
#作業用ディレクトリを作成
mkdir -p DCK/AnsibleAgentTest
#作業用ディレクトリへ移動
cd DCK/AnsibleAgentTest

#Dockerfileを作成
cat <<EOF > Dockerfile
# ベースイメージとしてUbuntuを使用
FROM ubuntu:latest

# 必要なパッケージのインストール
RUN apt-get update && apt-get install -y \
    openssh-server \
    python3 \
    python3-pip

# SSHサーバの設定
RUN mkdir /var/run/sshd
RUN echo 'root:password' | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSHサーバの起動スクリプト
CMD ["/usr/sbin/sshd", "-D"]

# ポート22を開放
EXPOSE 22

EOF

#Dockerコンテナを構築
docker build -t ssh-python-container .

#Dockerコンテナの実行
docker run -d -p 2222:22 --name my-ssh-python-container ssh-python-container

#sshサーバの接続テスト
ssh root@localhost -p 2222
#コンソール出力結果は以下であとはpasswordを入力すればログイン可能。
root@localhost's password:

```

続いてはAnsibleのDockerコンテナ -> リモートのPC用のDockerコンテナに対してAnsibleを実行できるようにしていきます。  

``` shell
#今回はDockerコンテナのsshサーバなので以下のコマンドでIPアドレスを確認します。
docker inspect my-ssh-python-container | grep IPAddress | tail -1 | grep -oP '(?<="IPAddress": ")[^"]*'
172.17.0.2

#別のTerminalを開いて下記のコマンドを実行
cd
cd DCK/Ansible
#Ansibleサーバを起動
docker run -it ansible-container bash

#エディタ:nanoをインストール
apt update -y && apt upgrade -y
apt install nano

#DockerコンテナのAnsibleの設定
mkdir -p /etc/ansible
nano /etc/ansible/hosts
[sshservers]
172.17.0.2
[sshservers:vars]
ansible_port=22
ansible_user=root
ansible_password=password
#ansible_ssh_private_key_file=~/.ssh/id_rsa

#your_username=root
#ansible_password=password

#sshpassをインストールこれは「ansible all -m ping -k」に必要です。
sudo apt install sshpass
#上は以下のエラー対処
172.17.0.2 | FAILED! => {
    "msg": "to use the 'ssh' connection type with passwords or pkcs11_provider, you must install the sshpass program"
}
#一度だけログインする
ssh root@172.17.0.2
#コンソール出力
The authenticity of host '172.17.0.2 (172.17.0.2)' can't be established.'
ED25519 key fingerprint is SHA256:NLdNVm5G9irqwO4a0Tv8QzAEw7O4G5RhV1cGGFNwmJY.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ED25519) to the list of known hosts.
#上で下記のエラーは解消される
#上は以下のエラー対処
172.17.0.2 | FAILED! => {
    "msg": "Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host."

#AnsibleのSSHログインが可能か確認する。-kでSSHのログインを確認するという意味。
ansible all -m ping -k
172.17.0.2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}

#Ansibleのplaybookを作成
cat <<EOF > test2.yml
---
- name: Test2 Ansible Playbook
  hosts: sshservers
  tasks:
    - name: Write "Hello, World!" to the file
      ansible.builtin.copy:
        dest: /tmp/test.txt
        content: "Hello, World!"
EOF

#Ansibleのplaybookを実行
ansible-playbook -i /etc/ansible/hosts /root/test2.yml
#コンソール出力結果

PLAY [Test2 Ansible Playbook] ********************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************************************
ok: [172.17.0.2]

TASK [Write "Hello, World!" to the file] *********************************************************************************************************************************************
changed: [172.17.0.2]

PLAY RECAP ***************************************************************************************************************************************************************************
172.17.0.2                 : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

#又別のTerminalを開いて下記のコマンドを実行
ssh root@172.17.0.2
root@172.17.0.2's password:
cat /tmp/test.txt
Hello, World!root@02c029a341de:~#
#上の通り、Hello, World!が/tmp/test.txtにWriteされていることを確認した。

```

## 1.5.本番運用について

 本番運用をしていきます。  

## 1.5.1.ProxmoxVEの構築

  Ansibleによる自動的にProxmoxVEの環境構築する場合について考えてみましょう。
この章では仮想マシンをもとに構築できるか確認していきますので、仮想マシン間でネットワークを構築していきます。

- 1.[Oracle Virtual BoxにDebianをインストール](#141-oracleのvirtualboxで環境をインストール)することでProxmoxVEの環境構築先を作成する  
- 2.Ansibleのためにpython3をインストールする  

``` shell

su -
apt update
apt upgrade
#install後, AnsibleのPlaybookを実行するため、python3をinstall
apt install python3
```

- 3.参考文献①の通りGuest間で通信できるようにする。
- 4.

参考文献
①VirtualBoxを使って２つのゲストOS間で通信してみる
<https://qiita.com/areaz_/items/c9075f7a0b3e147e92f2>

## 1.5.1.1.ProxmoxVE内でOpenwrt構築

## 1.5.1.2.ProxmoxVE内でWazuh構築

## 1.6.まとめ

 以上で、Ansibleを用いてリモートにあるPCの環境を操作することができます。  
実際に有効な操作をするには、pakageやその他の複雑な操作[あるファイルにある文字列の置換など]や対話的な操作を行う必要がでてくることもあります。  

今回は、Ansibleを用いて操作するための環境構築とテストができました。  

今後はAnsibleを利用することで、環境構築を自動化することが可能です。  
既存でscriptでまとめていた内容も自動化できるので既に環境がある場合でも取り込みしやすいです。  

毎回0ベースから構築することもできるため、設定の不整合なども解消でき、さらに、Upgradeもできます。  

- [ ] [README.mdへ戻る](./../README.md)
