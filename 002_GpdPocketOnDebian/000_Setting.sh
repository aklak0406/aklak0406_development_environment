#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#このscriptはrootユーザで行ってください。
#また、OSはDebian12にまずは限定しています。

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###

#bluetoothは利用しないため、読み込まないようにする
lsmod|grep -i bluetooth

echo "blacklist btrtl 
blacklist btqca       
blacklist btintel     
blacklist hci_uart    
blacklist btbcm       
blacklist bnep        
blacklist rfcomm      
blacklist bluetooth   
"| tee -a /etc/modprobe.d/blacklist.conf
lsmod|grep -i bluetooth

#intel_sst_acpiがエラー常にするので利用しないため、読み込まないようにする
lsmod|grep -i intel_sst_acpi
echo "blacklist snd_intel_sst_acpi 
blacklist snd_soc_acpi_intel_match       
blacklist snd_soc_sst_cht_bsw_rt5645     
"| tee -a /etc/modprobe.d/blacklist.conf
lsmod|grep -i intel_sst_acpi

#backupを取る。
cp -pf /etc/apt/sources.list /etc/apt/sources.list.old1
#Debian 12用にnon Freeなパッケージもインストールする[FirmwareはOpenではないため]
cat > /etc/apt/sources.list << EOL
deb https://deb.debian.org/debian bookworm main contrib non-free non-free-firmware
deb-src  https://deb.debian.org/debian bookworm main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security bookworm-security main non-free non-free-firmware contrib
deb-src http://security.debian.org/debian-security bookworm-security main non-free non-free-firmware contrib
deb http://deb.debian.org/debian/ bookworm-updates main non-free non-free-firmware contrib
deb-src http://deb.debian.org/debian/ bookworm-updates main non-free non-free-firmware contrib
EOL

#パッケージマネージャー:aptを更新
apt update -y
apt upgrade -y

#GPD PocketのWIFIを利用するためにブロードコムのFirmwareをインストールする。
apt install -y firmware-brcm80211
#そのままでは全部のfirmwareのリンクがないので設定する
cd /lib/firmware/brcm
ln -s brcmfmac4356-pcie.bin brcmfmac4356-pcie.gpd-win-pocket.bin
ln -s brcmfmac4356-pcie.clm_blob brcmfmac4356-pcie.gpd-win-pocket.clm_blob

#USB Type C LAN r8152のFirmwareをインストールする
apt install -y firmware-realtek

#USBWIFI:WN-G150UMを認識させる
cat > /etc/udev/rules.d/network_drivers.rules << EOL
ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="04bb", ATTR{idProduct}=="094c", RUN+="/sbin/modprobe -qba rtl8192cu"
EOL
cat > /etc/modprobe.d/network_drivers.conf << EOL
install rtl8192cu /sbin/modprobe --ignore-install rtl8192cu $CMDLINE_OPTS; /bin/echo "04bb 094c" > /sys/bus/usb/drivers/rtl8192cu/new_id
EOL

#GPD PocketのWIFIをWIFIアクセスポイントとして利用する
#パッケージ:hostapdをインストールする
apt install -y hostapd
#hostapdを11acの5GHzの設定をする
#---------------------------------------------------------------------#
cat > /etc/hostapd/wlp1s0.conf << EOL
#インタフェース名
interface=wlp1s0
#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MYBRCMWIFI
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=a
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=36
wpa=2 # WPA2
#認証パスワード
wpa_passphrase=wifipasswd
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定(11acを使うにあたってこれは省略できない)
ieee80211n=1
require_ht=1
ht_capab=[MAX-AMSDU-3839][HT40+][SHORT-GI-20][SHORT-GI-40][DSSS_CCK-40]

# 11acの有効化
ieee80211ac=1

# [MAX-AMSDU-3839]:
# [SHORT-GI-80]: 80GHzショートGI
vht_capab=[MAX-AMSDU-3839][SHORT-GI-80]

# ステーション側が11acをサポートしていなければ接続を拒否する
require_vht=1
# チャネル幅
# 0: 20/40 MHz
# 1: 80 MHz
# 2: 160 MHz
# 3: 80+80 MHz
vht_oper_chwidth=1
# 中心周波数
# 5Ghz+(5*index)
# channel+6を指定すればいいらしい
vht_oper_centr_freq_seg0_idx=42
EOL
#---------------------------------------------------------------------#


#次回起動時に自動起動
systemctl enable hostapd@wlp1s0.service

#2.4GHzと5GHzの無線LANを1つのインタフェース名まとめる
apt install bridge-utils
cat >> /etc/network/interfaces << EOL
auto br0
allow-hotplug br0
iface br0 inet static
  address 10.0.0.1/24
  bridge_stp off
  bridge_ports wlp1s0
  bridge_maxwait 30
EOL

# DHCPサーバーのインストール
apt install -y isc-dhcp-server
mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.org
#DHCPサーバの設定
#---------------------------------------------------------------------#
cat > /etc/dhcp/dhcpd.conf << EOL
default-lease-time 600;
max-lease-time 7200;
INTERFACES="br0";
option domain-name "";
max-lease-time 7200;
log-facility local7;

subnet 10.0.0.0 netmask 255.255.255.0 {
    range 10.0.0.2 10.0.0.254;
    option routers 10.0.0.1;
    option domain-name-servers 8.8.8.8;
}
EOL
#---------------------------------------------------------------------#
#DHCPサーバーの対象となるインタフェース名を設定
mv /etc/default/isc-dhcp-server  /etc/default/isc-dhcp-server.org
cat > /etc/default/isc-dhcp-server << EOL
INTERFACESv4="br0"
INTERFACESv6=""
EOL

# rfkillのソフト許可設定
apt install -y rfkill
touch /etc/rc.local
chmod a+x /etc/rc.local
#---------------------------------------------------------------------#
cat > /etc/rc.local << EOL
#!/bin/sh -e

#rc.local内部に以下の一文を記載
rfkill unblock all

#ifconfig wlp1s0 10.0.0.1 netmask 255.255.255.0 up
#再起動時にhostapdを起動する
#systemctl restart hostapd.service

EOL
#---------------------------------------------------------------------#
systemctl enable rc-local.service

#iptablesの設定[ルータ用]
apt install -y iptables
apt install -y net-tools
#---------------------------------------------------------------------#
#詳細は以下参考
#URL:https://ja.manpages.org/iptables/8
#PPPDのインタフェース名を指定
#PPP=   ;
#PPP_IP=$(ifconfig $PPP | awk '/inet / {print $2}')
#WAN側のインタフェース名を指定
WAN=enx00e04c6800a1   ;
WAN_IP=$(ifconfig $WAN | awk '/inet / {print $2}')
#WirelessLAN側のインタフェース名を指定
WLAN=wlp1s0 ;
#LAN側全部
BR0=br0 ;
#意味:設定をクリア
#チェイン無指定なのでテーブル内の全部のチェインに対し、ルール全部消去
iptables -F 
#チェイン無指定なのでテーブルにある全部のチェインを削除
iptables -X 
#すべてのチェインのパケットカウンタとバイトカウンタをゼロにする
iptables -Z
###########################################################################
#ポリシー #                                                                
###########################################################################
#意味:入力(受信)パケットを全部許可するポリシー
#詳細: チェイン:INPUTのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P INPUT ACCEPT           
#意味:転送パケットを全部許可するポリシー
#詳細: チェイン:FORWARDのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P FORWARD ACCEPT         
#意味:出力(送信)パケットを全部許可するポリシー
#詳細: チェイン:OUTPUTのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P OUTPUT ACCEPT          
###########################################################################
#INPUT    #                                                                
###########################################################################
#意味:新しいチェイン:INPUT_PINGを作成する
iptables -N INPUT_PING
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:icmpを対象、 ICMPタイプ8（エコー要求またはping）のパケットにマッチしたパケットをINPUT_PINGというチェインへジャンプさせる」である
iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j INPUT_PING 
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて既存の接続（ESTABLISHED）および関連パケット（RELATED）を許可[ACCEPT]する」である
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、無効なパケット（INVALID）を破棄[DROP]する」である
#-j LOG～は対象となるパケットをログ出力する
iptables -A INPUT -m state --state INVALID -j LOG --log-prefix "IPTDRP_IN_INVALID" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A INPUT -m state --state INVALID -j DROP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、インターフェースが$BR0である場合に新しい接続（NEW）のパケットを許可[ACCEPT]する」である
iptables -A INPUT -i $BR0 -m state --state NEW -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、インターフェースがloである場合に新しい接続（NEW）のパケットを許可[ACCEPT]する」である
iptables -A INPUT -i lo -m state --state NEW -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:udpを対象、UDPプロトコルのポート67から68への通信を許可する
#つまり、DHCPクライアントがDHCPサーバと通信する際に使用されるポートを許可する」である
iptables -A INPUT -p udp -m udp --sport 67 --dport 68 -j ACCEPT

#ポート開放している定義:ssh[22],smtps[587],Clamd[3310]
iptables -A INPUT -p tcp -m multiport --dports 22,3310 -m tcp -j ACCEPT
iptables -A INPUT -p tcp -m multiport --sports 3310 -m tcp -j ACCEPT

#意味:新しいチェイン:INPUT_ICMPを作成する
iptables -N INPUT_ICMP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:icmpを対象にしてマッチしたパケットをINPUT_PINGというチェインへジャンプさせる」である
iptables -A INPUT -p icmp -j INPUT_ICMP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「すべてのINPUTパケットを破棄[DROP]」である
iptables -A INPUT -j LOG --log-prefix "IPTDRP_IN_DROP:" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A INPUT -j DROP
###########################################################################
#FORWARD    #                                                              
###########################################################################
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「TCPプロトコルのパケットを対象, SYNフラグが設定され、RSTフラグが設定されていないパケットにマッチする場合、
#マッチしたパケットのMSS（Maximum Segment Size）をPMTU（Path Maximum Transmission Unit）に制限する」である
#TCPMSS（最大セグメントサイズ）という意味
#MSSの計算式は[ MSS = MTU - 20(TCPヘッダ) - 20(IPヘッダ)]となる
#一般的に、イーサネット環境ではMSS値は1500 - 20 - 20 = 1460バイトとなる
#この値は、TCP通信における最大ペイロードサイズを制御するために重要となる
#↑のPMTUに制限するとはTCPとIPヘッダ分の40byteを引いた値に自動で適切に制限するという意味である
iptables -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて既存の接続（ESTABLISHED）および関連パケット（RELATED）を許可[ACCEPT]する」である
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0以外かつ送信元のインタフェース名:$PPPの場合はパケットを破棄[DROP]する」である
if [ "$PPP" ]
then
  iptables -A FORWARD ! -i $BR0 -o $PPP -j DROP
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0以外かつ送信元のインタフェース名:$WANの場合はパケットを破棄[DROP]する」である
iptables -A FORWARD ! -i $BR0 -o $WAN -j DROP
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0かつ送信元のインタフェース名:$BR0の場合はパケットを許可[ACCEPT]する」である
iptables -A FORWARD -i $BR0 -o $BR0 -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、無効なパケット（INVALID）を破棄[DROP]する」である
iptables -A FORWARD -m state --state INVALID -j LOG --log-prefix "IPTDRP_FW_INVALID" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A FORWARD -m state --state INVALID -j DROP
#意味:新しいチェイン:SECURITYを作成する
iptables -N SECURITY
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名$PPPはチェイン:SECURITYというチェインへジャンプさせる」である
if [ "$PPP" ]
then
  iptables -A FORWARD -i $PPP -j SECURITY
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0のパケットは許可[ACCEPT]する」である
iptables -A FORWARD -i $BR0 -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「conntrack モジュールを使用して、パケットの状態を追跡し、DNAT (宛先 NAT) の状態に一致するパケットを対象とする。
#つまり、着信パケットの宛先アドレスが変更されている場合にこのルールが適用されるためNATのパケットは許可[ACCEPT]する」である
iptables -A FORWARD -m conntrack --ctstate DNAT -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「すべてのFORWARDパケットを破棄[DROP]」である
iptables -A INPUT -j LOG --log-prefix "IPTDRP_FW_DROP" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A FORWARD -j DROP
###########################################################################
#OUTPUT    #                                                              
###########################################################################
#意味:新しいチェイン:OUTPUT_DNSを作成する
iptables -N OUTPUT_DNS
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「UDPプロトコルのUDPモジュールを使用して、dport:53のパケット[DNS関連]のU32モジュールを使用することで
#パケットの内容を解析します
#パケットのバイトオフセット 0 から 22 ビット目までのビットパターンを検索し、特定の条件を満たすパケットを選択する
#つまり、DNS トラフィックを特定の条件でフィルタリングした上でチェイン:OUTPUT_DNSというチェインへジャンプさせる」である
iptables -A OUTPUT -p udp -m udp --dport 53 -m u32 --u32 "0x0>>0x16&0x3c@0x8>>0xf&0x1=0x0" -j OUTPUT_DNS
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「TCPプロトコルのUDPモジュールを使用して、dport:53のパケット[DNS関連]のU32モジュールを使用することで
#パケットの内容を解析します
#パケットのバイトオフセット 0 から 22 ビット目までのビットパターンを検索し、特定の条件を満たすパケットを選択する
#つまり、DNS トラフィックを特定の条件でフィルタリングした上でチェイン:OUTPUT_DNSというチェインへジャンプさせる」である
iptables -A OUTPUT -p tcp -m tcp --dport 53 -m u32 --u32 "0x0>>0x16&0x3c@0xc>>0x1a&0x3c@0x8>>0xf&0x1=0x0" -j OUTPUT_DNS
#意味:新しいチェイン:OUTPUT_IPを作成する
iptables -N OUTPUT_IP
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「チェイン:OUTPUTはチェイン:OUTPUT_IPというチェインへジャンプさせる」である
iptables -A OUTPUT -j OUTPUT_IP
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルかつ ICMP タイプ 8（エコーリクエスト）のパケットを対象、マッチした場合、元のチェーンに戻す」である
iptables -A INPUT_ICMP -p icmp -m icmp --icmp-type 8 -j RETURN
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルかつ ICMP タイプ 13（通信がフィルタリングにより禁止されている）のパケットを対象、
#マッチした場合、元のチェーンに戻す」である
iptables -A INPUT_ICMP -p icmp -m icmp --icmp-type 13 -j RETURN
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルを許可[ACCEPT]する」である
iptables -A INPUT_ICMP -p icmp -j ACCEPT
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:INPUT_PINGの最後の順番にルール追加する
#ルールは「インタフェース名:PPPかつICMPプロトコルのパケットの場合、破棄[DROP]する」である
if [ "$PPP" ]
then
  iptables -A INPUT_PING -i $PPP -p icmp -j DROP
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#チェイン:INPUT_PINGの最後の順番にルール追加する
#ルールは「インタフェース名:WANかつICMPプロトコルのパケットの場合、破棄[DROP]する」である
iptables -A INPUT_PING -i $WAN -p icmp -j DROP
#意味:新しいチェイン:logdrop_dnsを作成する
iptables -N logdrop_dns
#意味:チェイン:OUTPUT_DNSの最後の順番にルール追加する
#ルールは「下記のパケットの場合、チェイン:logdrop_dnsというチェインへジャンプさせる」である
# DNSパケット内で一致させるための16進数文字列を指定しています。この16進数文字列は、ドメイン名 “pouytuyiopkjfnf.com” のASCII表現に対応しています。
# Boyer-Moore アルゴリズムを文字列の一致検索に使用
# 検索範囲はパケットの先頭から65535バイト
# 大文字小文字を区別しない検索
#他と同様なので差分のみ
iptables -A OUTPUT_DNS -m string --hex-string "|10706f697579747975696f706b6a666e6603636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “rfjejnfnjefje.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d72666a656a6e666a6e65666a6503636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “rfjemassssaqark.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|1131306166646d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “7mfsdfasdmkgmrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f376d667364666173646d6b676d726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “8masaxsssaqrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d386d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “9fdmasaxsssaqrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f3966646d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “efbthmoiuykmkjkjgt.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|1265666274686d6f6975796b6d6b6a6b6a677403636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “hackucdt.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|086861636b7563647403636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “linwudi.f3322.net” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|076c696e77756469056633333232036e657400|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “lkjhgfdsatryuio.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f6c6b6a68676664736174727975696f03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “mnbvcxzzz12.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0b6d6e627663787a7a7a313203636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “q111333.top” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|077131313133333303746f7000|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “sq520.f3322.net” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|057371353230056633333232036e657400|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “uctkone.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|077563746b6f6e6503636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “zxcvbmnnfjjfwq.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0e7a786376626d6e6e666a6a66777103636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “eummagvnbp.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0a65756d6d6167766e627003636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “routersasus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0b726f75746572736173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “www.router-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|037777770b726f757465722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “www	asuslogin.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0377777709617375736c6f67696e03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “repeatar-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d72657065617461722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “ww1.router-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|037777310b726f757465722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#意味:新しいチェイン:logdrop_ipを作成する
iptables -N logdrop_ip
#意味:チェイン:OUTPUT_IPの最後の順番にルール追加する
#ルールは「ディスティネーションIPアドレス範囲:193.201.224.0/24の場合、チェイン:logdrop_ipというチェインへジャンプさせる」である
#193.201.224.0/24はウクライナです。
#↑と同じなので差分のみ
iptables -A OUTPUT_IP -d 193.201.224.0/24 -j logdrop_ip
#51.15.120.245/32はフランスです。
iptables -A OUTPUT_IP -d 51.15.120.245/32 -j logdrop_ip
#45.33.73.134/32はアメリカです。
iptables -A OUTPUT_IP -d 45.33.73.134/32 -j logdrop_ip
#190.115.18.28はベリーズです。
iptables -A OUTPUT_IP -d 190.115.18.28/32 -j logdrop_ip
#51.159.52.250/32はフランスです。
iptables -A OUTPUT_IP -d 51.159.52.250/32 -j logdrop_ip
#190.115.18.86/32はベリーズです。
iptables -A OUTPUT_IP -d 190.115.18.86/32 -j logdrop_ip
#意味:チェイン:SECURITYの最後の順番にルール追加する
if [ "$PPP" ]
then
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:SYNフラグが設定されたパケットに一致した場合,
  #1秒間に1パケットに制限をかける。制限がない場合はそのまま処理し、制限がある場合はDROPされるである
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m limit --limit 1/sec -j RETURN
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:SYNフラグが設定されたパケットに一致した場合,DROPする
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j DROP
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:RSTフラグが設定されたパケットに一致した場合,
  #1秒間に1パケットに制限をかける。制限がない場合はそのまま処理し、制限がある場合はDROPされるである
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -m limit --limit 1/sec -j RETURN
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:RSTフラグが設定されたパケットに一致した場合,DROPする
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j DROP
  #ルールは「ICMPパケットかつecho-request[icmp-type 8]の場合、1秒間に1パケットに制限し、
  #制限がない場合はそのまま処理し、制限がある場合はDROPされるである」
  iptables -A SECURITY -p icmp -m icmp --icmp-type 8 -m limit --limit 1/sec -j RETURN
  #ルールは「ICMPパケットかつecho-request[icmp-type 8]の場合、DROPされるである」
  iptables -A SECURITY -p icmp -m icmp --icmp-type 8 -j DROP
  #元のチェーンに戻す
  iptables -A SECURITY -j RETURN
fi
#意味:新しいチェイン:logacceptを作成する
iptables -N logaccept
#入ってくるパケット全部ログ出力対象
#iptables -A INPUT -j logaccept
#通過するパケット全部ログ出力対象
#iptables -A FORWARD -j logaccept
#出ていくパケット全部ログ出力対象
#iptables -A OUTPUT -j logaccept
#パケットのログ出力
iptables -A logaccept -m state --state NEW -j LOG --log-prefix "ACCEPT " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logaccept -j ACCEPT

#意味:チェイン:logdropの最後の順番にルール追加する
#ルールは「チェイン:logdropのパケットをログ出力してDROPする」である
#iptables -A logdrop -m state --state NEW -j LOG --log-prefix "DROP " --log-tcp-sequence --log-tcp-options --log-ip-options
#iptables -A logdrop -j DROP
#意味:チェイン:logdrop_dnsの最後の順番にルール追加する
#ルールは「チェイン:logdrop_dnsのパケットをログ出力してDROPする」である
iptables -A logdrop_dns -j LOG --log-prefix "IPTDRP_LDN_DROP_DNS " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logdrop_dns -j DROP
#意味:チェイン:logdrop_ipの最後の順番にルール追加する
#ルールは「チェイン:logdrop_ipのパケットをログ出力してDROPする」である
iptables -A logdrop_ip -j LOG --log-prefix "IPTDRP_LDI_DROP_IP " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logdrop_ip -j DROP

###########################################################################
#nat    #                                                              
###########################################################################
if [ "$PPP" ]
then
  #PPPのIP以外のものをMASQUERADEする
  iptables -t nat -A POSTROUTING ! -s $PPP_IP/32 -o $PPP -j MASQUERADE
fi
#WAN_IP以外のものをMASQUERADEする
iptables -t nat -A POSTROUTING ! -s $WAN_IP/32 -o $WAN -j MASQUERADE
#BR0のものはを10.0.0.0/24サブネット内のものはMASQUERADEする
iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -d 10.0.0.0/24 -o $BR0 -j MASQUERADE

#設定したiptablesの設定を永続化する
apt install -y iptables-persistent
#下記のコマンドで現在のiptablesの設定を「/etc/iptables/rules.v4」へ反映する。
iptables-save > /etc/iptables/rules.v4
#---------------------------------------------------------------------#

#IPv6の停止とIPv4のフォワーディング設定をする[ルーティングに必要]
cat >> /etc/sysctl.conf << EOL
#IPv6をDisable
net.ipv6.conf.all.disable_ipv6 = 1
#IPv4のフォワーディング有効
net.ipv4.ip_forward = 1
EOL

# サーバの起動する
systemctl enable isc-dhcp-server.service

#次回起動時に一番最後に実行するscript 
cat > /etc/rc.local.latest << EOL
#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

###
# USB LANが結構な頻度で認識が遅れDHCP割り当てができず接続ができないので、起動の一番最後にUSB切断,接続をするように変更しました。
###
ip link show | grep enx00e04c6800a
# 直前のコマンドの終了ステータスを確認
if [ $? -ne 0 ]; then
   #enx00e04c6800aが見つからない場合[USB LANが認識遅くなっている]
   #dmesg | grep 1d6b | grep 0003
   #[    4.559522] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
   VenDerID="1d6b"
   ProductID="0003"
   lsusb -d $VenDerID:$ProductID
   # 直前のコマンドの終了ステータスを確認
   if [ $? -eq 0 ]; then
      dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}'
      # 直前のコマンドの終了ステータスを確認
      if [ $? -eq 0 ]; then
         BusID=`dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}' | sed 's/.$//'`
         echo "BusID=$BusID"
         echo $BusID | tee /sys/bus/usb/drivers/usb/unbind
         echo $BusID | tee /sys/bus/usb/drivers/usb/bind
      fi
   fi
fi
set -eE -o functrace

#インタフェース:br0へwlp1s0を追加
brctl addif br0 wlp1s0
#インタフェース:br0へwlx00a0b0f3d7c0を追加
brctl addif br0 wlx00a0b0f3d7c0

### ここまで処理実行に必要なコマンドラインを記載する  ###

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

EOL
systemctl --system enable rc-local-latest.service
### ここまで処理実行に必要なコマンドラインを記載する  ###

#再起動させて反映する
reboot

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace
