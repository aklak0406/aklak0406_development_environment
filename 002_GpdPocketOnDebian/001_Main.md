# GPDPKTのDebianの初期設定

- [ ] [README.mdへ戻る](./../README.md)

## 目次

- [GPDPKTのDebianの初期設定](#gpdpktのdebianの初期設定)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [Gpd Pocketの周辺回りのネットワーク構成図](#gpd-pocketの周辺回りのネットワーク構成図)
  - [表紙](#表紙)

## 概要説明

 Debian GNU/Linux 12を使用してGpdPocketをサーバ利用できるようした例を示す。

## Gpd Pocketの周辺回りのネットワーク構成図

最終イメージはAsusRouterとの置き換えだが、現時点の実験環境を記載する。  
現時点の環境はGPDポケットにUSB-LANを接続しルータにした。  

```mermaid
flowchart LR

%%外部のインターネット
OU1[光回線 ONU>WAN側 <br> PPPoE]

%%グループとサービス
subgraph GC[家庭内LAN]
  subgraph GV[Subnet:192.168.1.0/24]
    subgraph GS1[ASUS Router]
      NW1{{"WAN IP:DHCP"}}
      NW2{{"LAN IP:192.168.1.2"}}
    end
    subgraph GS2[GPD pocket]
      NW3{{"WAN IP:192.168.1.10"}}
      NW4{{"LAN IP:10.0.0.1"}}
    end
  end
  subgraph GV2[Subnet:10.0.0.0/24]
    subgraph GS3[例.ノートPC]
      NW5{{"LAN IP:10.0.0.2"}}
    end
  end
end

%%サービス同士の関係
OU1 <----> NW1
NW1 <----> NW2
NW2 <--LANケーブル--> NW3
NW3 <--仮想Bridge--> NW4
NW4 <--LANケーブル--> NW5

%%グループのスタイル
classDef SGC fill:none,color:#345,stroke:#345
class GC SGC

classDef SGV fill:none,color:#0a0,stroke:#0a0
class GV,GV2 SGV

classDef SGPsS fill:#cef,color:#0Ab,stroke:none
class GS3 SGPsS

classDef SGPrS fill:#1ef,color:#07b,stroke:none
class GS2 SGPrS

classDef SGPuS fill:#Afe,color:#092,stroke:none
class GS1 SGPuS

%%サービスのスタイル
classDef SOU fill:#aaa,color:#fff,stroke:#none
class OU1 SOU

classDef SNW fill:#84d,color:#fff,stroke:none
class NW1,NW2,NW3,NW4,NW5 SNW

 ```

## 表紙

- [ ] [インストール](./../001_DebianLinuxOnPromoxVE/002_Install.md)  
    ↑のインストールでGpdPocketの場合は有線LANないので、USBLANで作者の環境は代用しています。  
    USBLANは自動でDriver認識しました。  
    WIFIの場合は下記を参考にすればインストールはできそうです。[未検証]  
<https://www.netfort.gr.jp/~tosihisa/notebook/doku.php/debian/20170816_debian9_gpdpocket>  
　　なお、作者の場合はUSBLANを使用してインストール後に↑の手順を参考にしてWIFIのみ有効化させました。  

- [ ] [ブロードコムのWIFI設定](./002_BrcmWiFiSetting.md)  

- [ ] [RealtekのWIFI設定](./003_RealtekWiFiSetting.md)  

- [ ] [セキュリティの設定](./004_SecuritySetting.md)  

- [ ] [汎用的なErrorを加味したShellScriptについて](./005_ShellScript.md)  

- [ ] [GUI->CUIへデフォルト設定する](./006_FromGuiToCui.md)  

- [ ] [USB-LANの起動が遅い場合がありDHCP割り当てができず接続できない場合の対処](./007_USBLANConnectSetting.md)  

- [ ] [DebianノートPCでふたを閉じてもスリープさせない設定](./008_DebianNotePCSleepSetting.md)  

- [ ] [無線APソフトhostapdで2つの無線AP5GHzと2.4GHzを作成する](./009_HostapdDualSetting.md)  

- [ ] [エラーしているbluetoothデバイスを無効化する](./010_DisableBluetoothSetting.md)  

- [ ] [エラーしているACPIデバイスを無効化する](./011_DisableACPISetting.md)  

- [ ] [2つの無線AP間で同一DHCPを利用する](./012_SameDhcpSetting.md)  

ここから下は工事中  

- [ ] [有線LANルータにする](./013_WanToLanSetting.md)

- [ ] [無料のIDS/IPSを導入する](./007_USBLANConnectSetting.md)

- [ ] [機器の設定値をまとめて収集する.](./007_USBLANConnectSetting.md)

- [ ] [機器の状態をまとめて収集する.](./007_USBLANConnectSetting.md)

---
PromoxVE DHCP 失敗するため固定IPにする

nano /etc/network/interfaces

iface enx00e04c6800a1 inet dhcp
auto br0
allow-hotplug br0
iface br0 inet static
  address 10.0.0.1/24
  bridge_stp off
  bridge_ports eth0　★eth0にUSB LAN Type Aの方に名前が変わっていた
  bridge_maxwait 30

auto vmbr0
allow-hotplug vmbr0
iface vmbr0 inet static
         address 192.168.1.10/24
         gateway 192.168.1.2
         dns-nameservers 192.168.1.2 8.8.8.8
         bridge-ports enx00e04c6800a1 ★USB LAN Type Cの方
         bridge-stp off
         bridge-fd 0

nano /etc/iptables/rules.v4

enx00e04c6800a1 -> vmbr0へ名前変更

注意:vmbr0の方のインタフェースからはssh接続できなくなったので、br0の方で対応する

---

OpenwrtをPromoxVeでRouter機能だけ利用する方法

現状のNetworkインタフェース
仮想インタフェース名:br0  実体:eth0★USB LAN Type Aの方
仮想インタフェース名:vmbr0  実体:enx00e04c6800a1★USB LAN Type Cの方

やりたいこと
vmbr0これをWAN
LANをvmbr1に変更
vmbr1はbr0仮想インタフェースとは別名かつ別IPにする。

---

- [ ] [README.mdへ戻る](./../README.md)
