# ブロードコムのWIFI設定

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [ブロードコムのWIFI設定](#ブロードコムのwifi設定)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [non-freeのパッケージ設定ファイルへ追加する](#non-freeのパッケージ設定ファイルへ追加する)
    - [ブロードコムのファームウェアをインストールする](#ブロードコムのファームウェアをインストールする)
    - [既存のWIFIルータへWIFI接続する場合](#既存のwifiルータへwifi接続する場合)
    - [WIFIアクセスポイントとして利用する場合](#wifiアクセスポイントとして利用する場合)
      - [hostapdをインストール](#hostapdをインストール)
      - [DHCPサーバーのインストール](#dhcpサーバーのインストール)
      - [rfkillのソフト許可設定](#rfkillのソフト許可設定)
      - [rfkillを起動時に自動で設定できるようにrc.localを設定する](#rfkillを起動時に自動で設定できるようにrclocalを設定する)
      - [NAT設定をする](#nat設定をする)
      - [サーバの起動する](#サーバの起動する)
      - [hostapd\_cliで確認する](#hostapd_cliで確認する)
    - [iptablesの設定](#iptablesの設定)

## 概要説明

GpdPocketというノートパソコンはブロードコム製のWIFIチップを使用している。
LinuxOSではブロードコム製のLinuxDriverを使用した上でhotapedなどのWIFIソフトを使用してWIFI接続できるようにする。

## 設定

### non-freeのパッケージ設定ファイルへ追加する

まず始めにブロードコムのドライバーをInstallするために
non-freeのパッケージをパッケージマネージャー:aptの設定ファイルへ追加する。
追加理由は、OSがDebianであるので、基本パッケージはfreeのものしかない。ブロードコムのドライバーはバイナリだけのためフリーではないパッケージに入っているので追加する。

```shell
#バックアップを取る
cp -pf /etc/apt/sources.list /etc/apt/sources.list.old1

#non-free,non-free-firmwareへ変更する。
nano /etc/apt/sources.list
deb http://security.debian.org/debian-security bookworm-security main non-free non-free-firmware contrib
deb-src http://security.debian.org/debian-security bookworm-security main non-free non-free-firmware contrib

# bookworm-updates, to get updates before a point release is made;
# see https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_updates_and_backports
deb http://deb.debian.org/debian/ bookworm-updates main non-free non-free-firmware contrib
deb-src http://deb.debian.org/debian/ bookworm-updates main non-free non-free-firmware contrib

#更新コマンドを実行してエラーがないことを確認する
apt update
```

### ブロードコムのファームウェアをインストールする

```shell
apt-get install firmware-brcm80211
#上そのままだと以下のエラーがでるので、設定が必要
#エラー内容
firmware: failed to load brcm/brcmfmac4356-pcie.gpd-win-pocket.bin
firmware: failed to load brcm/brcmfmac4356-pcie.gpd-win-pocket.clm_blob


#設定
#grubのおまじない
nano /etc/default/grub
---
##GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX_DEFAULT="video=efifb fbcon=rotate:1"
cd /lib/firmware/brcm
---
#Bootloader:GrubをUpdateする。
update-grub

#brcmfmac4356-pcie.bin,brcmfmac4356-pcie.clm_blobはあるのでシンボリックリンクを設定する
cd /lib/firmware/brcm
ln -s brcmfmac4356-pcie.bin brcmfmac4356-pcie.gpd-win-pocket.bin
ln -s brcmfmac4356-pcie.clm_blob brcmfmac4356-pcie.gpd-win-pocket.clm_blob
#再起動させる
reboot
#boot時,brcmfmac関連でエラーでてないこと。
dmesg
```

これでブロードコムのLinux Driverのインストールは完了です。

### 既存のWIFIルータへWIFI接続する場合

```shell
#WIFIのssidとパスワードを入力する
wpa_passphrase ssid名 ssidのパスワード
#以下へファイル出力されることを確認
cat /etc/wpa_supplicant/wpa_supplicant.conf
#エラーない場合、以下の設定をする
#ip aコマンドにより、事前にインタフェース名:wlp1s0を確認。
#端末によって無線WIFI名が異なるが、Wirelessのwが付いた名称になることが多い。

nano /etc/network/interfaces
---
auto wlp1s0
iface wlp1s0 inet dhcp
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
---
#再起動してipが割り振られているか確認する。
reboot
ip a
```

### WIFIアクセスポイントとして利用する場合

この設定をする場合は[既存のWIFIルータへWIFI接続する場合](#既存のwifiルータへwifi接続する場合)を設定しないでください。

#### hostapdをインストール

　hostapdは無線LANデバイスをAP/認証サーバーとして機能させるためのオープンソースソフトウェアです。
なお、無線LANデバイスの仕様上対応は11a,b,g,n,acまでとなる。

```shell
#hostapdのインストール
sudo apt-get install hostapd
#hostapdの設定する
nano /etc/hostapd/hostapd.conf
#11n 2.4GHzの場合
---
#インタフェース名
interface=wlp1s0
#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MYBRCMWIFI_24G
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=g
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=7
wpa=2 # WPA2
#認証パスワード
wpa_passphrase=wifipasswd2
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定(11acを使うにあたってこれは省略できない)
ieee80211n=1
#CLI有効化
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
---

#11ac 5GHz[規格上5GHzのみ]の場合
---
#インタフェース名
interface=wlp1s0
#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MYBRCMWIFI
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=a
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=36
wpa=2 # WPA2
#認証パスワード
wpa_passphrase=wifipasswd
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定(11acを使うにあたってこれは省略できない)
ieee80211n=1
require_ht=1
ht_capab=[MAX-AMSDU-3839][HT40+][SHORT-GI-20][SHORT-GI-40][DSSS_CCK-40]

# 11acの有効化
ieee80211ac=1

# [MAX-AMSDU-3839]:
# [SHORT-GI-80]: 80GHzショートGI
vht_capab=[MAX-AMSDU-3839][SHORT-GI-80]

# ステーション側が11acをサポートしていなければ接続を拒否する
require_vht=1
# チャネル幅
# 0: 20/40 MHz
# 1: 80 MHz
# 2: 160 MHz
# 3: 80+80 MHz
vht_oper_chwidth=1
# 中心周波数
# 5Ghz+(5*index)
# channel+6を指定すればいいらしい
vht_oper_centr_freq_seg0_idx=42
#CLI有効化
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
---

#hostapdの設定ファイルの場所の設定
nano /etc/default/hostapd
---
DAEMON_CONF="/etc/hostapd/hostapd.conf"
---
hostapd_wlp1s0_11ac5Ghz.conf
hostapd_wlx00a0b0f3d7c0_11n2.4GHz.conf

```

#### DHCPサーバーのインストール

```shell
#DHCPサーバーのインストール
apt install isc-dhcp-server
#DHCPサーバーの設定
nano /etc/dhcp/dhcpd.conf
---
default-lease-time 600;
max-lease-time 7200;
INTERFACES="wlp1s0";
option domain-name "";
max-lease-time 7200;
log-facility local7;

subnet 10.0.0.0 netmask 255.255.255.0 {
    range 10.0.0.10 10.0.0.20;
    option routers 10.0.0.1;
    option domain-name-servers 8.8.8.8;
}
---
#DHCPサーバーの対象となるインタフェース名を設定
nano /etc/default/isc-dhcp-server
---
INTERFACESv4="wlp1s0"
---

```

#### rfkillのソフト許可設定

```shell
#[rfkill command not found]がでるため以下を実行する
#rfkillインストール
apt install rfkill
#以下を実行するとSoft blocked: yesになっているのでnoへ変更する
rfkill list
rfkill unblock all
#ちなみにSoft blocked: yesのままだとスマホでいうと機内モードのようになるため、電波を出さなくなります。

```

#### rfkillを起動時に自動で設定できるようにrc.localを設定する

```shell
touch /etc/rc.local
chmod a+x /etc/rc.local

#rfkill後にwlp1s0もUpすればDHCPも割り振られる。
nano /etc/rc.local
---
#!/bin/sh -e

#rc.local内部に以下の一文を記載
rfkill unblock all

ifconfig wlp1s0 10.0.0.1 netmask 255.255.255.0 up
---
#次回から自動起動するように実行する
systemctl enable rc-local.service

```

#### NAT設定をする

```shell
#firewallの有効にしている場合
#現在のFirewall設定を削除する。
iptables -t nat -F         && \
iptables -t mangle -F      && \
iptables -F                && \
iptables -X                && \
iptables -P INPUT ACCEPT   && \
iptables -P FORWARD ACCEPT && \
iptables -P OUTPUT ACCEPT

#以下のように無線WANのサブネット[10.0.0.0/24]をすべて有線LAN側[eth0]へIPマスカレードを行う
iptables -t nat -A POSTROUTING -s 10.0.0.0/255.255.255.0 -o eth0 -j MASQUERADE
#↑は永続てきではないため、必要あれば,ルール追加すること。
iptables-save > /etc/iptables/rules.v4
#ただし、お試しなら↑でよいですが、しっかりファイヤーウォールの設定はすべきなので設定値を見直してください。
```

#### サーバの起動する

```shell
#hostapdサーバの起動
systemctl start hostapd
#DHCPサーバーの起動
systemctl start isc-dhcp-server.service

#起動OKなら、次回から自動起動にする
systemctl enable hostapd
systemctl enable isc-dhcp-server.service
```

#### hostapd_cliで確認する

```shell
hostapd_cli -iwlp1s0 -p/var/run/hostapd

#接続している機器のMACアドレスがわかる。
>list
#後は設定値の反映が確認できる。
>interface
wlp1s0
> status
state=ENABLED
phy=phy0
freq=2442
num_sta_non_erp=0
num_sta_no_short_slot_time=0
num_sta_no_short_preamble=0
olbc=0
num_sta_ht_no_gf=0
num_sta_no_ht=0
num_sta_ht_20_mhz=0
num_sta_ht40_intolerant=0
olbc_ht=0
ht_op_mode=0x0
cac_time_seconds=0
cac_time_left_seconds=N/A
channel=7
edmg_enable=0
edmg_channel=0
secondary_channel=0
ieee80211n=1
ieee80211ac=0
ieee80211ax=0
beacon_int=100
dtim_period=2
ht_caps_info=000c
ht_mcs_bitmask=ffff0000000000000000
supported_rates=02 04 0b 16 0c 12 18 24 30 48 60 6c
max_txpower=20
bss[0]=wlp1s0
bssid[0]=56:a4:23:5d:84:3d
ssid[0]=MYBRCMWIFI
num_sta[0]=1

> driver_flags
000000003804FAC4:
DFS_OFFLOAD
AP
SET_KEYS_AFTER_ASSOC_DONE
P2P_CONCURRENT
P2P_CAPABLE
AP_TEARDOWN_SUPPORT
P2P_MGMT_AND_NON_P2P
VALID_ERROR_CODES
OFFCHANNEL_TX
BSS_SELECTION
IBSS
RADAR
DEDICATED_P2P_DEVICE

> get_config
bssid=56:a4:23:5d:84:3d
ssid=MYBRCMWIFI
wps_state=disabled
wpa=2
key_mgmt=WPA-PSK
group_cipher=CCMP
rsn_pairwise_cipher=CCMP

> all_sta
5e:f7:6b:91:dc:80
flags=[AUTH][ASSOC][AUTHORIZED]
aid=0
capability=0x0
listen_interval=0
supported_rates=02 04 0b 16
timeout_next=NULLFUNC POLL
dot11RSNAStatsSTAAddress=5e:f7:6b:91:dc:80
dot11RSNAStatsVersion=1
dot11RSNAStatsSelectedPairwiseCipher=00-0f-ac-4
dot11RSNAStatsTKIPLocalMICFailures=0
dot11RSNAStatsTKIPRemoteMICFailures=0
wpa=2
AKMSuiteSelector=00-0f-ac-2
hostapdWPAPTKState=11
hostapdWPAPTKGroupState=0
rx_packets=3789
tx_packets=2686
rx_bytes=763377
tx_bytes=1533652
inactive_msec=29000
signal=0
rx_rate_info=10
tx_rate_info=10
connected_time=2386
supp_op_classes=51515354737475767778797a7b808182

```

### iptablesの設定

　再起動後もiptablesの設定を有効化するための方法のみ記載する。なお、ほかのファイヤーウォールルールが原因で無線LANは接続できるが、インターネットへアクセスできない場合がある。
その場合は原因調査が必要になる。

```shell
#設定したiptablesの設定を永続化する。
apt install iptables-persistent
#下記のコマンドで現在のiptablesの設定を「/etc/iptables/rules.v4」へ反映する。
iptables-save > /etc/iptables/rules.v4

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
