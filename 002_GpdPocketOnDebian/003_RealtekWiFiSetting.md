# ブロードコムのWIFI設定

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [ブロードコムのWIFI設定](#ブロードコムのwifi設定)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [non-freeのパッケージ設定ファイルへ追加する](#non-freeのパッケージ設定ファイルへ追加する)
    - [Realtek製のファームウェアをインストールする](#realtek製のファームウェアをインストールする)

## 概要説明

私のGpdPocketというノートパソコンにUSB無線WIFIとしてRealtek製のWIFIチップを使用しているので
LinuxOSではRealtek製のLinuxDriverを使用した上でhotapedなどのWIFIソフトを使用してWIFI接続できるようにする。

## 設定

### non-freeのパッケージ設定ファイルへ追加する

下記を参照する。

- [ブロードコムのWIFI設定>non-freeのパッケージ設定ファイルへ追加する](./002_BrcmWiFiSetting.md#non-freeのパッケージ設定ファイルへ追加する)

### Realtek製のファームウェアをインストールする

```shell
apt update
apt install firmware-realtek
#エラー:E: Package 'firmware-realtek' has no installation candidate
#↑のため以下を実施
nano /etc/apt/sources.list
---
#deb http://deb.debian.org/debian/ bookworm main
deb https://deb.debian.org/debian bookworm main contrib non-free non-free-firm
#deb-src http://deb.debian.org/debian/ bookworm main
deb-src  https://deb.debian.org/debian bookworm main contrib non-free non-free-fi
---
apt update
apt install firmware-realtek

#WIFI:IO-DATA WN-G150UMのドライバの読み込み
modprobe rtl8192cu
#modprobe rtl8192cuが読み込めているかチェック
modinfo rtl8192cu
#エラーでなければOK

nano /etc/udev/rules.d/network_drivers.rules
---
ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="04bb", ATTR{idProduct}=="094c", RUN+="/sbin/modprobe -qba rtl8192cu"
---

nano /etc/modprobe.d/network_drivers.conf
---
install rtl8192cu /sbin/modprobe --ignore-install rtl8192cu $CMDLINE_OPTS; /bin/echo "04bb 094c" > /sys/bus/usb/drivers/rtl8192cu/new_id
---

reboot
```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
