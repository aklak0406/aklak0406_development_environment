# セキュリティの設定

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [セキュリティの設定](#セキュリティの設定)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [iptablesの設定,端末として](#iptablesの設定端末として)
    - [iptablesの設定,ルータとして](#iptablesの設定ルータとして)

## 概要説明

LinuxOSではデフォルトでiptablesというファイヤーウォールを使用しています。これは、データの安全性を確保するためのものです。
今回はなるべき一般的な設定を行おうと思いますが、セキュリティの設定は日々変わっていきますのでご留意ください。

## 設定

### iptablesの設定,端末として

　再起動後もiptablesの設定を有効化するための方法のみ記載する。なお、ほかのファイヤーウォールルールが原因で無線LANは接続できるが、インターネットへアクセスできない場合がある。
その場合は原因調査が必要になる。

```shell
#ポート開放しているものは最低限,22[ssh,制御のコンソール操作用途],80/443[http/https、主にaptやwgetなどのパッケージインストール用途],またこのポートあてのhttpのDOS攻撃用のルール記載。123[ntp],53[DNS、これもaptやwgetなど]とした。
      iptables -F && \
      iptables -X && \
      iptables -P INPUT DROP                                                                                                                                                                                                                  && \
      iptables -P FORWARD DROP                                                                                                                                                                                                                && \
      iptables -P OUTPUT ACCEPT                                                                                                                                                                                                               && \
      iptables -A INPUT -i lo -j ACCEPT                                                                                                                                                                                                       && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "DropInPktDataNotFound:"                                                                                                                   && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP                                                                                                                                                        && \
      iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "DropInPktSYNflood:"                                                                                                         && \
      iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j DROP                                                                                                                                          && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "DropInPktStealthScan:"                                                                                                 && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j DROP                                                                                                                                     && \
      iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m hashlimit --hashlimit-upto 1/min --hashlimit-burst 10 --hashlimit-mode srcip --hashlimit-name t_icmp --hashlimit-htable-expire 120000 -j LOG --log-prefix "DropPingOfDeathAttack:"   && \
      iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m hashlimit --hashlimit-upto 1/min --hashlimit-burst 10 --hashlimit-mode srcip --hashlimit-name t_icmp --hashlimit-htable-expire 120000 -j ACCEPT                                      && \
      iptables -A INPUT -f -j LOG --log-prefix 'fragment_packet:'                                                                                                                                                                             && \
      iptables -A INPUT -f -j DROP                                                                                                                                                                                                            && \
      iptables -N HTTP_DOS                                                                                                                                                                                                                    && \
      iptables -A HTTP_DOS -p tcp -m multiport --dports 80,443 -m hashlimit --hashlimit 1/s --hashlimit-burst 100 --hashlimit-htable-expire 300000 --hashlimit-mode srcip --hashlimit-name t_HTTP_DOS -j RETURN                               && \
      iptables -A HTTP_DOS -j LOG --log-prefix "http_dos_attack: "                                                                                                                                                                            && \
      iptables -A HTTP_DOS -j DROP                                                                                                                                                                                                            && \
      iptables -A INPUT -p tcp -m multiport --dports 80,443 -j HTTP_DOS                                                                                                                                                                       && \
      iptables -A INPUT -p tcp -m multiport --dports 113 -j REJECT --reject-with tcp-reset                                                                                                                                                    && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --set                                                                                                                                               && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --rcheck --seconds 60 --hitcount 5 -j LOG --log-prefix "ssh_brute_force: "                                                                          && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --rcheck --seconds 60 --hitcount 5 -j REJECT --reject-with tcp-reset                                                                                && \
      iptables -A INPUT -d 224.0.0.1       -j LOG --log-prefix "drop_broadcast: "                                                                                                                                                             && \
      iptables -A INPUT -d 224.0.0.1       -j DROP                                                                                                                                                                                            && \
      iptables -A INPUT -p udp -m multiport --sports 53,123 -m udp -j ACCEPT                                                                                                                                                                  && \
      iptables -A INPUT -p tcp -m multiport --dports 22,587,3310 -m tcp -j ACCEPT                                                                                                                                                             && \
      iptables -A INPUT -p tcp -m multiport --sports 80,443,587,3310 -m tcp -j ACCEPT

#設定したiptablesの設定を永続化する。
apt install iptables-persistent
#下記のコマンドで現在のiptablesの設定を「/etc/iptables/rules.v4」へ反映する。
iptables-save > /etc/iptables/rules.v4
```

iptablesの概要図

```mermaid
flowchart LR

%%グループとサービス
subgraph GC1[Network Interface]
  subgraph IP1[Incomming<br>Packet]
  end
  subgraph OP1[Outgoing<br>Packet]
  end
end
subgraph PR1[PREROUTING]
  subgraph RAW1[raw]
  end
  subgraph MG1[mangle]
  end
  subgraph NAT1[nat]
  end
end
subgraph RT1[ROUTING]
end
subgraph FW1[FORWARD]
  subgraph MG2[mangle]
  end
  subgraph fl1[fillter]
  end
end
subgraph IT1[INPUT]
  subgraph MG3[mangle]
  end
  subgraph fl2[fillter]
  end
end
subgraph LP1[Local<br>Process]
end
subgraph RT2[ROUTING]
end
subgraph OP2[OUTPUT]
  subgraph RAW2[raw]
  end
  subgraph MG4[mangle]
  end
  subgraph NAT2[nat]
  end
  subgraph fl3[fillter]
  end
end
subgraph RT3[ROUTING]
end
subgraph PR2[POSTROUTING]
  subgraph MG5[mangle]
  end
  subgraph NAT3[nat]
  end
end
%%サービス同士の関係
IP1  --> RAW1
RAW1 --> MG1
MG1  --> NAT1
NAT1 --> RT1
RT1  --> MG2
MG2  --> fl1
RT1  --> MG3
MG3  --> fl2
fl2  --> LP1
LP1  --> RT2
RT2  --> RAW2
RAW2 --> MG4
MG4  --> NAT2
NAT2 --> fl3
fl1  --> RT3
fl3  --> RT3
RT3  --> MG5
MG5  --> NAT3
NAT3 --> OP1

%%グループのスタイル
classDef SGC fill:none,color:#345,stroke:#345
class GC1,PR1,RT1,FW1,IT1,LP1,RT2,OP2,RT3,PR2 SGC

classDef SGV fill:none,color:#0a0,stroke:#0a0
class IP1,OP1,RAW1,MG1,NAT1,MG2,fl1,MG3,,RAW2,MG4,NAT2,fl2,fl3,MG5,NAT3 SGV

 ```

参考文献
<https://christina04.hatenablog.com/entry/iptables-outline>

### iptablesの設定,ルータとして

下記の設定は現在[2024/5/3時点]で利用しているAsusのROuterの有効になっている設定を参考にした。
AsusのROuterとの大きく異なる点はClamdのport[3301],sshのport[22],clamdのウィルス検知時のメール用のport[587]をWAN向けに開放している。
実際の運用時はメール用のport以外は閉じたほうがよいかもしれない。

```shell
#詳細は以下参考
#URL:https://ja.manpages.org/iptables/8
#PPPDのインタフェース名を指定
#PPP=   ;
#PPP_IP=$(ifconfig $PPP | awk '/inet / {print $2}')
#WAN側のインタフェース名を指定
WAN=enx00e04c6800a1   ;
WAN_IP=$(ifconfig $WAN | awk '/inet / {print $2}')
#WirelessLAN側のインタフェース名を指定
WLAN=wlp1s0 ;
#LAN側全部
BR0=br0 ;
#意味:設定をクリア
#チェイン無指定なのでテーブル内の全部のチェインに対し、ルール全部消去
iptables -F 
#チェイン無指定なのでテーブルにある全部のチェインを削除
iptables -X 
#すべてのチェインのパケットカウンタとバイトカウンタをゼロにする
iptables -Z
###########################################################################
#ポリシー #                                                                
###########################################################################
#意味:入力(受信)パケットを全部許可するポリシー
#詳細: チェイン:INPUTのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P INPUT ACCEPT           
#意味:転送パケットを全部許可するポリシー
#詳細: チェイン:FORWARDのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P FORWARD ACCEPT         
#意味:出力(送信)パケットを全部許可するポリシー
#詳細: チェイン:OUTPUTのポリシー[-P]を指定したターゲット:ACCEPTに設定する。ACCEPTは許可という意味である
iptables -P OUTPUT ACCEPT          
###########################################################################
#INPUT    #                                                                
###########################################################################
#意味:新しいチェイン:INPUT_PINGを作成する
iptables -N INPUT_PING
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:icmpを対象、 ICMPタイプ8（エコー要求またはping）のパケットにマッチしたパケットをINPUT_PINGというチェインへジャンプさせる」である
iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j INPUT_PING 
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて既存の接続（ESTABLISHED）および関連パケット（RELATED）を許可[ACCEPT]する」である
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、無効なパケット（INVALID）を破棄[DROP]する」である
#-j LOG～は対象となるパケットをログ出力する
iptables -A INPUT -m state --state INVALID -j LOG --log-prefix "IPTDRP_IN_INVALID" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A INPUT -m state --state INVALID -j DROP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、インターフェースが$BR0である場合に新しい接続（NEW）のパケットを許可[ACCEPT]する」である
iptables -A INPUT -i $BR0 -m state --state NEW -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、インターフェースがloである場合に新しい接続（NEW）のパケットを許可[ACCEPT]する」である
iptables -A INPUT -i lo -m state --state NEW -j ACCEPT
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:udpを対象、UDPプロトコルのポート67から68への通信を許可する
#つまり、DHCPクライアントがDHCPサーバと通信する際に使用されるポートを許可する」である
iptables -A INPUT -p udp -m udp --sport 67 --dport 68 -j ACCEPT

#ポート開放している定義:ssh[22],smtps[587],Clamd[3310]
iptables -A INPUT -p tcp -m multiport --dports 22,587,3310 -m tcp -j ACCEPT
iptables -A INPUT -p tcp -m multiport --sports 587,3310 -m tcp -j ACCEPT

#意味:新しいチェイン:INPUT_ICMPを作成する
iptables -N INPUT_ICMP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「パケット:icmpを対象にしてマッチしたパケットをINPUT_PINGというチェインへジャンプさせる」である
iptables -A INPUT -p icmp -j INPUT_ICMP
#意味:チェイン:INPUTの最後の順番にルール追加する
#ルールは「すべてのINPUTパケットを破棄[DROP]」である
iptables -A INPUT -j LOG --log-prefix "IPTDRP_IN_DROP:" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A INPUT -j DROP
###########################################################################
#FORWARD    #                                                              
###########################################################################
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「TCPプロトコルのパケットを対象, SYNフラグが設定され、RSTフラグが設定されていないパケットにマッチする場合、
#マッチしたパケットのMSS（Maximum Segment Size）をPMTU（Path Maximum Transmission Unit）に制限する」である
#TCPMSS（最大セグメントサイズ）という意味
#MSSの計算式は[ MSS = MTU - 20(TCPヘッダ) - 20(IPヘッダ)]となる
#一般的に、イーサネット環境ではMSS値は1500 - 20 - 20 = 1460バイトとなる
#この値は、TCP通信における最大ペイロードサイズを制御するために重要となる
#↑のPMTUに制限するとはTCPとIPヘッダ分の40byteを引いた値に自動で適切に制限するという意味である
iptables -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて既存の接続（ESTABLISHED）および関連パケット（RELATED）を許可[ACCEPT]する」である
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0以外かつ送信元のインタフェース名:$PPPの場合はパケットを破棄[DROP]する」である
if [ "$PPP" ]
then
  iptables -A FORWARD ! -i $BR0 -o $PPP -j DROP
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0以外かつ送信元のインタフェース名:$WANの場合はパケットを破棄[DROP]する」である
iptables -A FORWARD ! -i $BR0 -o $WAN -j DROP
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0かつ送信元のインタフェース名:$BR0の場合はパケットを許可[ACCEPT]する」である
iptables -A FORWARD -i $BR0 -o $BR0 -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「パケットの状態[state]に基づいて、無効なパケット（INVALID）を破棄[DROP]する」である
iptables -A FORWARD -m state --state INVALID -j LOG --log-prefix "IPTDRP_FW_INVALID" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A FORWARD -m state --state INVALID -j DROP
#意味:新しいチェイン:SECURITYを作成する
iptables -N SECURITY
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名$PPPはチェイン:SECURITYというチェインへジャンプさせる」である
if [ "$PPP" ]
then
  iptables -A FORWARD -i $PPP -j SECURITY
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「インタフェース名:$BR0のパケットは許可[ACCEPT]する」である
iptables -A FORWARD -i $BR0 -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「conntrack モジュールを使用して、パケットの状態を追跡し、DNAT (宛先 NAT) の状態に一致するパケットを対象とする。
#つまり、着信パケットの宛先アドレスが変更されている場合にこのルールが適用されるためNATのパケットは許可[ACCEPT]する」である
iptables -A FORWARD -m conntrack --ctstate DNAT -j ACCEPT
#意味:チェイン:FORWARDの最後の順番にルール追加する
#ルールは「すべてのFORWARDパケットを破棄[DROP]」である
iptables -A INPUT -j LOG --log-prefix "IPTDRP_FW_DROP" --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A FORWARD -j DROP
###########################################################################
#OUTPUT    #                                                              
###########################################################################
#意味:新しいチェイン:OUTPUT_DNSを作成する
iptables -N OUTPUT_DNS
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「UDPプロトコルのUDPモジュールを使用して、dport:53のパケット[DNS関連]のU32モジュールを使用することで
#パケットの内容を解析します
#パケットのバイトオフセット 0 から 22 ビット目までのビットパターンを検索し、特定の条件を満たすパケットを選択する
#つまり、DNS トラフィックを特定の条件でフィルタリングした上でチェイン:OUTPUT_DNSというチェインへジャンプさせる」である
iptables -A OUTPUT -p udp -m udp --dport 53 -m u32 --u32 "0x0>>0x16&0x3c@0x8>>0xf&0x1=0x0" -j OUTPUT_DNS
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「TCPプロトコルのUDPモジュールを使用して、dport:53のパケット[DNS関連]のU32モジュールを使用することで
#パケットの内容を解析します
#パケットのバイトオフセット 0 から 22 ビット目までのビットパターンを検索し、特定の条件を満たすパケットを選択する
#つまり、DNS トラフィックを特定の条件でフィルタリングした上でチェイン:OUTPUT_DNSというチェインへジャンプさせる」である
iptables -A OUTPUT -p tcp -m tcp --dport 53 -m u32 --u32 "0x0>>0x16&0x3c@0xc>>0x1a&0x3c@0x8>>0xf&0x1=0x0" -j OUTPUT_DNS
#意味:新しいチェイン:OUTPUT_IPを作成する
iptables -N OUTPUT_IP
#意味:チェイン:OUTPUTの最後の順番にルール追加する
#ルールは「チェイン:OUTPUTはチェイン:OUTPUT_IPというチェインへジャンプさせる」である
iptables -A OUTPUT -j OUTPUT_IP
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルかつ ICMP タイプ 8（エコーリクエスト）のパケットを対象、マッチした場合、元のチェーンに戻す」である
iptables -A INPUT_ICMP -p icmp -m icmp --icmp-type 8 -j RETURN
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルかつ ICMP タイプ 13（通信がフィルタリングにより禁止されている）のパケットを対象、
#マッチした場合、元のチェーンに戻す」である
iptables -A INPUT_ICMP -p icmp -m icmp --icmp-type 13 -j RETURN
#意味:チェイン:INPUT_ICMPの最後の順番にルール追加する
#ルールは「 ICMP プロトコルを許可[ACCEPT]する」である
iptables -A INPUT_ICMP -p icmp -j ACCEPT
#意味:$PPPインタフェースが定義されていれば設定する。
#チェイン:INPUT_PINGの最後の順番にルール追加する
#ルールは「インタフェース名:PPPかつICMPプロトコルのパケットの場合、破棄[DROP]する」である
if [ "$PPP" ]
then
  iptables -A INPUT_PING -i $PPP -p icmp -j DROP
fi
#意味:チェイン:FORWARDの最後の順番にルール追加する
#チェイン:INPUT_PINGの最後の順番にルール追加する
#ルールは「インタフェース名:WANかつICMPプロトコルのパケットの場合、破棄[DROP]する」である
iptables -A INPUT_PING -i $WAN -p icmp -j DROP
#意味:新しいチェイン:logdrop_dnsを作成する
iptables -N logdrop_dns
#意味:チェイン:OUTPUT_DNSの最後の順番にルール追加する
#ルールは「下記のパケットの場合、チェイン:logdrop_dnsというチェインへジャンプさせる」である
# DNSパケット内で一致させるための16進数文字列を指定しています。この16進数文字列は、ドメイン名 “pouytuyiopkjfnf.com” のASCII表現に対応しています。
# Boyer-Moore アルゴリズムを文字列の一致検索に使用
# 検索範囲はパケットの先頭から65535バイト
# 大文字小文字を区別しない検索
#他と同様なので差分のみ
iptables -A OUTPUT_DNS -m string --hex-string "|10706f697579747975696f706b6a666e6603636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “rfjejnfnjefje.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d72666a656a6e666a6e65666a6503636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “rfjemassssaqark.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|1131306166646d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “7mfsdfasdmkgmrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f376d667364666173646d6b676d726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “8masaxsssaqrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d386d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “9fdmasaxsssaqrk.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f3966646d617361787373736171726b03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “efbthmoiuykmkjkjgt.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|1265666274686d6f6975796b6d6b6a6b6a677403636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “hackucdt.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|086861636b7563647403636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “linwudi.f3322.net” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|076c696e77756469056633333232036e657400|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “lkjhgfdsatryuio.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0f6c6b6a68676664736174727975696f03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “mnbvcxzzz12.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0b6d6e627663787a7a7a313203636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “q111333.top” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|077131313133333303746f7000|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “sq520.f3322.net” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|057371353230056633333232036e657400|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “uctkone.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|077563746b6f6e6503636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “zxcvbmnnfjjfwq.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0e7a786376626d6e6e666a6a66777103636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “eummagvnbp.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0a65756d6d6167766e627003636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “routersasus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0b726f75746572736173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “www.router-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|037777770b726f757465722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “www.asuslogin.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0377777709617375736c6f67696e03636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “repeatar-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|0d72657065617461722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#ドメイン名 “ww1.router-asus.com” のASCII表現に対応しています。
iptables -A OUTPUT_DNS -m string --hex-string "|037777310b726f757465722d6173757303636f6d00|" --algo bm --to 65535 --icase -j logdrop_dns
#意味:新しいチェイン:logdrop_ipを作成する
iptables -N logdrop_ip
#意味:チェイン:OUTPUT_IPの最後の順番にルール追加する
#ルールは「ディスティネーションIPアドレス範囲:193.201.224.0/24の場合、チェイン:logdrop_ipというチェインへジャンプさせる」である
#193.201.224.0/24はウクライナです。
#↑と同じなので差分のみ
iptables -A OUTPUT_IP -d 193.201.224.0/24 -j logdrop_ip
#51.15.120.245/32はフランスです。
iptables -A OUTPUT_IP -d 51.15.120.245/32 -j logdrop_ip
#45.33.73.134/32はアメリカです。
iptables -A OUTPUT_IP -d 45.33.73.134/32 -j logdrop_ip
#190.115.18.28はベリーズです。
iptables -A OUTPUT_IP -d 190.115.18.28/32 -j logdrop_ip
#51.159.52.250/32はフランスです。
iptables -A OUTPUT_IP -d 51.159.52.250/32 -j logdrop_ip
#190.115.18.86/32はベリーズです。
iptables -A OUTPUT_IP -d 190.115.18.86/32 -j logdrop_ip
#意味:チェイン:SECURITYの最後の順番にルール追加する
if [ "$PPP" ]
then
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:SYNフラグが設定されたパケットに一致した場合,
  #1秒間に1パケットに制限をかける。制限がない場合はそのまま処理し、制限がある場合はDROPされるである
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m limit --limit 1/sec -j RETURN
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:SYNフラグが設定されたパケットに一致した場合,DROPする
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j DROP
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:RSTフラグが設定されたパケットに一致した場合,
  #1秒間に1パケットに制限をかける。制限がない場合はそのまま処理し、制限がある場合はDROPされるである
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -m limit --limit 1/sec -j RETURN
  #ルールは「tcpプロトコルかつ-tcp-flagsオプションは特定のTCPフラグ:RSTフラグが設定されたパケットに一致した場合,DROPする
  iptables -A SECURITY -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j DROP
  #ルールは「ICMPパケットかつecho-request[icmp-type 8]の場合、1秒間に1パケットに制限し、
  #制限がない場合はそのまま処理し、制限がある場合はDROPされるである」
  iptables -A SECURITY -p icmp -m icmp --icmp-type 8 -m limit --limit 1/sec -j RETURN
  #ルールは「ICMPパケットかつecho-request[icmp-type 8]の場合、DROPされるである」
  iptables -A SECURITY -p icmp -m icmp --icmp-type 8 -j DROP
  #元のチェーンに戻す
  iptables -A SECURITY -j RETURN
fi
#意味:新しいチェイン:logacceptを作成する
iptables -N logaccept
#入ってくるパケット全部ログ出力対象
#iptables -A INPUT -j logaccept
#通過するパケット全部ログ出力対象
#iptables -A FORWARD -j logaccept
#出ていくパケット全部ログ出力対象
#iptables -A OUTPUT -j logaccept
#パケットのログ出力
iptables -A logaccept -m state --state NEW -j LOG --log-prefix "ACCEPT " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logaccept -j ACCEPT

#意味:チェイン:logdropの最後の順番にルール追加する
#ルールは「チェイン:logdropのパケットをログ出力してDROPする」である
#iptables -A logdrop -m state --state NEW -j LOG --log-prefix "DROP " --log-tcp-sequence --log-tcp-options --log-ip-options
#iptables -A logdrop -j DROP
#意味:チェイン:logdrop_dnsの最後の順番にルール追加する
#ルールは「チェイン:logdrop_dnsのパケットをログ出力してDROPする」である
iptables -A logdrop_dns -j LOG --log-prefix "IPTDRP_LDN_DROP_DNS " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logdrop_dns -j DROP
#意味:チェイン:logdrop_ipの最後の順番にルール追加する
#ルールは「チェイン:logdrop_ipのパケットをログ出力してDROPする」である
iptables -A logdrop_ip -j LOG --log-prefix "IPTDRP_LDI_DROP_IP " --log-tcp-sequence --log-tcp-options --log-ip-options
iptables -A logdrop_ip -j DROP

###########################################################################
#nat    #                                                              
###########################################################################
if [ "$PPP" ]
then
  #PPPのIP以外のものをMASQUERADEする
  iptables -t nat -A POSTROUTING ! -s $PPP_IP/32 -o $PPP -j MASQUERADE
fi
#WAN_IP以外のものをMASQUERADEする
iptables -t nat -A POSTROUTING ! -s $WAN_IP/32 -o $WAN -j MASQUERADE
#BR0のものはを10.0.0.0/24サブネット内のものはMASQUERADEする
iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -d 10.0.0.0/24 -o $BR0 -j MASQUERADE

#設定したiptablesの設定を永続化する
apt install iptables-persistent
#下記のコマンドで現在のiptablesの設定を「/etc/iptables/rules.v4」へ反映する。
iptables-save > /etc/iptables/rules.v4

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
