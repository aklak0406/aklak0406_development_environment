# 汎用的なErrorを加味したShellScriptについて

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [汎用的なErrorを加味したShellScriptについて](#汎用的なerrorを加味したshellscriptについて)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [自分用に汎用的なErrorを加味したShellScriptのひな形を作ろう](#自分用に汎用的なerrorを加味したshellscriptのひな形を作ろう)
    - [shellscriptの自分用にチェックすること](#shellscriptの自分用にチェックすること)
      - [作成したshellscriptをいきなり実行する前に文法あっているか確認したい](#作成したshellscriptをいきなり実行する前に文法あっているか確認したい)

## 概要説明

 Linuxの構築は主にコマンドラインがほとんどであるので、繰り返し設定をする可能性が高い。
ただ、複数似たような環境を再構築することも多いので、shellscriptにして標準化しておくことで効率的に進める。

## 設定

### 自分用に汎用的なErrorを加味したShellScriptのひな形を作ろう

作成の目的は毎回エラー発生時のshellscriptを考える/記載することが苦痛になったため、負荷軽減を狙い汎用的なものを考えている。
あくまで自分用のため人によっていいものがものが別にあるので必要に応じて修正していく

```shell
#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###

### ここまで処理実行に必要なコマンドラインを記載する  ###

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

```

### shellscriptの自分用にチェックすること

#### 作成したshellscriptをいきなり実行する前に文法あっているか確認したい

```shell
 bash -n shellscript名
```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
