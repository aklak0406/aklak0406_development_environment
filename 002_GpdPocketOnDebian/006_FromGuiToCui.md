# GUI->CUIへデフォルト設定する

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [GUI-\>CUIへデフォルト設定する](#gui-cuiへデフォルト設定する)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [現在の設定を確認する](#現在の設定を確認する)
    - [CUIへ設定変更する](#cuiへ設定変更する)

## 概要説明

 OS:Debianをインストール時にGUIデフォルト設定した。今回はサーバ運用するためCUIに設定しなおす。

## 設定

### 現在の設定を確認する

目的は現在GUI設定されていることを確認する

```shell

$ systemctl get-default
graphical.target
#上はGUIという意味
```

### CUIへ設定変更する

```shell
#rootユーザになる
su -
#CUIへ設定変更する
systemctl set-default -f multi-user.target
systemctl get-default
multi-user.target
#上はCUIという意味

#再起動させて反映確認
reboot
#起動後、コンソールのみになっていればOK
```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
