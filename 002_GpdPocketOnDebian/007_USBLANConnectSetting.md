# USB-LANの起動が遅い場合がありDHCP割り当てができず接続できない場合の対処

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [USB-LANの起動が遅い場合がありDHCP割り当てができず接続できない場合の対処](#usb-lanの起動が遅い場合がありdhcp割り当てができず接続できない場合の対処)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [状態確認と対処案](#状態確認と対処案)
    - [対処案を起動時に接続できない場合は自動的に設定するようにする](#対処案を起動時に接続できない場合は自動的に設定するようにする)

## 概要説明

起動のたびにUSB-LANの起動が遅い場合がありDHCP割り当てができず接続できない場合の対処する方法について記載する。

## 設定

### 状態確認と対処案

目的は対処案の検討に使うネタを抽出するために現在の状態を確認する

```shell
#問題発生時は下記のようにBus 002がDevice 001のみになっているため、
#USB LANのハブしか認識していない状態となっている。
lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
・
・省略
・
#解消方法
#下記の2つのコマンドを実行することでUSBの切断と接続がコマンドラインでできる。
echo BusID | tee /sys/bus/usb/drivers/usb/unbind ; \
echo BusID | tee /sys/bus/usb/drivers/usb/bind
#BusIDは実際にはUSBごとにユニークに設定された値を入力する。

#BusIDの求め方
#lsusbコマンドの[ID ①4桁:②4桁]の部分を参照する。
#今回はID 1d6b:0003が該当する。
#dmesgコマンドの[Bus ①4桁 Device ②4桁]の部分を参照する。
dmesg | grep 1d6b | grep 0003
[    4.559522] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
#上の時の[usb2]の部分がBusIDの値となる。
#つまり実際に設定するには以下のようになる。
echo usb2 | tee /sys/bus/usb/drivers/usb/unbind ; \
echo usb2 | tee /sys/bus/usb/drivers/usb/bind

```

### 対処案を起動時に接続できない場合は自動的に設定するようにする

目的は毎回起動時に手動で行うことをやめたいため自動化する

```shell
#自動化の方法
#1.Scriptを作成する
nano /etc/rc.local.latest
---
#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

###
# USB LANが結構な頻度で認識が遅れDHCP割り当てができず接続ができないので、起動の一番最後にUSB切断,接続をするように変更しました。
###
ip link show | grep enx00e04c6800a
# 直前のコマンドの終了ステータスを確認
if [ $? -ne 0 ]; then
   #enx00e04c6800aが見つからない場合[USB LANが認識遅くなっている]
   #dmesg | grep 1d6b | grep 0003
   #[    4.559522] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
   VenDerID="1d6b"
   ProductID="0003"
   lsusb -d $VenDerID:$ProductID
   # 直前のコマンドの終了ステータスを確認
   if [ $? -eq 0 ]; then
      dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}'
      # 直前のコマンドの終了ステータスを確認
      if [ $? -eq 0 ]; then
         BusID=`dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}' | sed 's/.$//'`
         echo "BusID=$BusID"
         echo $BusID | tee /sys/bus/usb/drivers/usb/unbind
         echo $BusID | tee /sys/bus/usb/drivers/usb/bind
      fi
   fi
fi
set -eE -o functrace

### ここまで処理実行に必要なコマンドラインを記載する  ###

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace
---
#scriptに実行権限つける
chmod +x /etc/rc.local.latest

#systemdのサービス登録する
nano /lib/systemd/system/rc-local-latest.service
---
[Unit]
#default.target（通常はマルチユーザーターゲット）が完了した後に
#このサービスが起動されることを意味します。つまり、他の必要なサービスが先に起動されることを保証します。
After=default.target

[Install]
#このサービスがdefault.targetに組み込まれることを意味します。
#つまり、通常のマルチユーザーターゲットでこのサービスが有効になります
WantedBy=default.target

[Service]
#このサービスがフォーク型のプロセスを起動することを意味します。
#フォーク型のプロセスはバックグラウンドで実行され、親プロセスとは独立しています。
Type=forking
#サービスを開始するために実行されるコマンドまたはスクリプトのパスを指定します。
#この場合、/etc/rc.local.latestが実行されます。
ExecStart=/etc/rc.local.latest
---
#systemdのサービス登録を有効化する
systemctl --system enable rc-local-latest

#再起動後有効化しているか確認する
reboot
#ログを見る
systemctl status rc-local-latest
#上より詳細は以下でrc-local-latest.serviceのログのみ確認
journalctl -u rc-local-latest.service
#前回[1回前]のブートにしぼってジャーナルログ全部を確認
journalctl -b -1
#前回[1回前]のブートにしぼってrc-local-latest.serviceのログのみ確認
journalctl -b -1 -u rc-local-latest.service
```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
