# DebianノートPCでふたを閉じてもスリープさせない設定

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [DebianノートPCでふたを閉じてもスリープさせない設定](#debianノートpcでふたを閉じてもスリープさせない設定)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [ノートPCをスリープさせない設定方法](#ノートpcをスリープさせない設定方法)

## 概要説明

サーバとしてノートPCを利用するため、蓋は常に閉じておいて運用する。しかし、Debianではデフォルト設定では蓋を閉じた場合スリープするため、スリープさせないように運用するためには設定変更が必要となる。

## 設定

### ノートPCをスリープさせない設定方法

```shell
#下記のファイルを開く
nano /etc/systemd/logind.conf
---
HandleLidSwitch=ignore
---
#上の設定項目を追加されば、ノートPCをスリープさせない設定になる

#PCを再起動させる or 以下のコマンドを実行する
 systemctl restart systemd-logind.service

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
