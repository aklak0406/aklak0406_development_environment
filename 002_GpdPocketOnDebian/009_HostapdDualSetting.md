# 無線APソフトhostapdで2つの無線AP5GHzと2.4GHzを作成する

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [無線APソフトhostapdで2つの無線AP5GHzと2.4GHzを作成する](#無線apソフトhostapdで2つの無線ap5ghzと24ghzを作成する)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [通常のhotapdがすでに起動している場合](#通常のhotapdがすでに起動している場合)
    - [hotapdを2つ起動させる](#hotapdを2つ起動させる)

## 概要説明

今回、ノートPCの内臓無線LANの5GHzのAP[アクセスポイント]と2.4GHzのAP[アクセスポイント]を作成する。
通常、hotapdは1つの設定ファイルしか利用できないが、今回は2つの設定ファイルを利用してそれぞれ起動させてみる。

## 設定

### 通常のhotapdがすでに起動している場合

通常利用するhostapdは今回分けるため、停止させておく。

```shell
#hostapd.serviceを停止させる
systemctl stop hostapd.service

#停止ができたら、自動起動を無効にする
systemctl disable hostapd.service

```

### hotapdを2つ起動させる

```shell
#無線LANのインタフェースごとにhotapdを起動させるため、まずはインタフェースを確認する
ip a

#この時の無線インタフェース名は環境に依存するが、私の場合は以下になる。
wlp1s0
wlx+MACアドレス=wlx001122334455
#上はMACアドレスが00:11:22:33:44:55の場合。
#この部分は環境によってもことなるので、確認しておくこと。

cd /etc/hostap/
#以下のように設定するhostapdの設定ファイルを無線インタフェース名を付けて作成する。5GHzの設定と2.4GHzの設定を2回繰り返す
nano 無線インタフェース名.conf
#実際
nano wlp1s0.conf
nano wlx001122334455.conf

#上記の設定ファイルを無線インタフェース名に合わせて起動する
#フォーマットは以下の通り
systemctl start hostapd@無線インタフェース名.service
#実際は以下
systemctl start hostapd@wlp1s0.service
systemctl start hostapd@wlx001122334455.service

#成功したら、自動起動を有効にする
systemctl enable hostapd@無線インタフェース名.service
#実際は以下
systemctl enable hostapd@wlp1s0.service
systemctl enable hostapd@wlx001122334455.service

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
