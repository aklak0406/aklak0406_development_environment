# エラーしているbluetoothデバイスを無効化する

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [エラーしているbluetoothデバイスを無効化する](#エラーしているbluetoothデバイスを無効化する)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [bluetoothの無効化](#bluetoothの無効化)

## 概要説明

下記のbluetoothデバイスのエラーが発生しているが、初期化して利用しようとしているように見えるため、管理しないデバイスを読み込まないように無効化しておく。

```shell
dmesg
・
・省略
・
[   12.558380] Bluetooth: hci0: BCM: chip id 101
[   12.564062] Bluetooth: hci0: BCM: features 0x2f
[   12.565063] Bluetooth: hci0: BCM4354A2
[   12.565078] Bluetooth: hci0: BCM4356A2 (001.003.015) build 0000
[   12.565150] bluetooth hci0: firmware: failed to load brcm/BCM4356A2.hcd (-2)
[   12.565354] bluetooth hci0: firmware: failed to load brcm/BCM4356A2.hcd (-2)
[   12.565447] bluetooth hci0: firmware: failed to load brcm/BCM.hcd (-2)
[   12.565523] bluetooth hci0: firmware: failed to load brcm/BCM.hcd (-2)
[   12.565582] Bluetooth: hci0: BCM: firmware Patch file not found, tried:
[   12.565639] Bluetooth: hci0: BCM: 'brcm/BCM4356A2.hcd'
[   12.565683] Bluetooth: hci0: BCM: 'brcm/BCM.hcd'
・
・省略
・
[  611.609860] Bluetooth: RFCOMM TTY layer initialized
[  611.609888] Bluetooth: RFCOMM socket layer initialized
[  611.609907] Bluetooth: RFCOMM ver 1.11

```

## 設定

### bluetoothの無効化

```shell
#rootユーザで行う
su -
#現在のbluetoothモジュールの確認
lsmod|grep -i bluetooth
bluetooth             958464  36 btrtl,btqca,btintel,hci_uart,btbcm,bnep,rfcomm
ecdh_generic           16384  2 bluetooth
rfkill                 36864  7 rtlwifi,bluetooth,cfg80211
crc16                  16384  2 bluetooth,ext4

#モジュールをロードしないように /etc/modprobe.d/blacklist.conf 
#というファイルに blacklist <modulename> という形式で記載する
echo "blacklist btrtl
blacklist btqca
blacklist btintel
blacklist hci_uart
blacklist btbcm
blacklist bnep
blacklist rfcomm
blacklist bluetooth
"| tee -a /etc/modprobe.d/blacklist.conf

#再起動する
reboot

#現在のbluetoothモジュールの確認してloadされていないことを確認する。
lsmod|grep -i bluetooth
#成功時は何も表示されない。

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
