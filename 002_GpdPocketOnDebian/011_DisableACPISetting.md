# エラーしているACPIデバイスを無効化する

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [エラーしているACPIデバイスを無効化する](#エラーしているacpiデバイスを無効化する)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [intel\_sst\_acpiの無効化](#intel_sst_acpiの無効化)

## 概要説明

下記のACPIデバイスのエラーが発生しているが、初期化して利用しようとしているように見えるため、管理しないデバイスを読み込まないように無効化しておく。

```shell
cat /var/log/messages | grep intel_sst_acpi
・
・省略
・
May 11 13:36:05 GPD kernel: [    9.260027] intel_sst_acpi 808622A8:00: LPE base: 0xa1600000 size:0x200000
May 11 13:36:05 GPD kernel: [    9.260033] intel_sst_acpi 808622A8:00: IRAM base: 0xa16c0000
May 11 13:36:05 GPD kernel: [    9.260072] intel_sst_acpi 808622A8:00: DRAM base: 0xa1700000
May 11 13:36:05 GPD kernel: [    9.260081] intel_sst_acpi 808622A8:00: SHIM base: 0xa1740000
May 11 13:36:05 GPD kernel: [    9.260088] intel_sst_acpi 808622A8:00: Mailbox base: 0xa1744000
May 11 13:36:05 GPD kernel: [    9.260095] intel_sst_acpi 808622A8:00: DDR base: 0x20000000
May 11 13:36:05 GPD kernel: [    9.266390] intel_sst_acpi 808622A8:00: Got drv data max stream 25
May 11 13:36:05 GPD kernel: [    9.273240] intel_sst_acpi 808622A8:00: Direct firmware load for intel/fw_sst_22a8.bin failed with error -2
May 11 13:40:21 GPD kernel: [    9.188344] intel_sst_acpi 808622A8:00: LPE base: 0xa1600000 size:0x200000
May 11 13:40:21 GPD kernel: [    9.188349] intel_sst_acpi 808622A8:00: IRAM base: 0xa16c0000
May 11 13:40:21 GPD kernel: [    9.188386] intel_sst_acpi 808622A8:00: DRAM base: 0xa1700000
May 11 13:40:21 GPD kernel: [    9.188395] intel_sst_acpi 808622A8:00: SHIM base: 0xa1740000
May 11 13:40:21 GPD kernel: [    9.188403] intel_sst_acpi 808622A8:00: Mailbox base: 0xa1744000
May 11 13:40:21 GPD kernel: [    9.188409] intel_sst_acpi 808622A8:00: DDR base: 0x20000000
May 11 13:40:21 GPD kernel: [    9.197418] intel_sst_acpi 808622A8:00: Got drv data max stream 25
May 11 13:40:21 GPD kernel: [    9.201009] intel_sst_acpi 808622A8:00: Direct firmware load for intel/fw_sst_22a8.bin failed with error -2
May 11 13:48:33 GPD kernel: [   10.392891] intel_sst_acpi 808622A8:00: LPE base: 0xa1600000 size:0x200000
May 11 13:48:33 GPD kernel: [   10.392897] intel_sst_acpi 808622A8:00: IRAM base: 0xa16c0000
May 11 13:48:33 GPD kernel: [   10.392933] intel_sst_acpi 808622A8:00: DRAM base: 0xa1700000
May 11 13:48:33 GPD kernel: [   10.392942] intel_sst_acpi 808622A8:00: SHIM base: 0xa1740000
May 11 13:48:33 GPD kernel: [   10.392952] intel_sst_acpi 808622A8:00: Mailbox base: 0xa1744000
May 11 13:48:33 GPD kernel: [   10.392958] intel_sst_acpi 808622A8:00: DDR base: 0x20000000
May 11 13:48:33 GPD kernel: [   10.402677] intel_sst_acpi 808622A8:00: Got drv data max stream 25
May 11 13:48:33 GPD kernel: [   10.408061] intel_sst_acpi 808622A8:00: Direct firmware load for intel/fw_sst_22a8.bin failed with error -2
```

## 設定

### intel_sst_acpiの無効化

```shell
#rootユーザで行う
su -
#intel_sst_acpiがエラー常にするので利用しないため、読み込まないようにする
lsmod|grep -i intel_sst_acpi
echo "blacklist snd_intel_sst_acpi 
blacklist snd_soc_acpi_intel_match       
blacklist snd_soc_sst_cht_bsw_rt5645     
"| tee -a /etc/modprobe.d/blacklist.conf

#再起動する
reboot

#現在のACPIモジュールの確認してloadされていないことを確認する。
lsmod|grep -i intel_sst_acpi
#成功時は何も表示されない。

```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
