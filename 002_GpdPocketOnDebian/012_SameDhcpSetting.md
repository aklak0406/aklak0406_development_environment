# 2つの無線AP間で同一DHCPを利用する

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [2つの無線AP間で同一DHCPを利用する](#2つの無線ap間で同一dhcpを利用する)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [2つの無線インタフェース名をブリッジする設定](#2つの無線インタフェース名をブリッジする設定)
    - [IPv4アドレスのみをDHCP利用する設定](#ipv4アドレスのみをdhcp利用する設定)

## 概要説明

2つの無線AP間で同一DHCPを利用する。

## 設定

### 2つの無線インタフェース名をブリッジする設定

目的は2つの無線インタフェースを1つにブリッジすることで１つのブリッジインタフェースで設定が一括できることである。

```shell
#2つの無線インタフェース名は「wlp1s0,wlx00a0b0f3d7c0」とする。
#2.4GHzと5GHzの無線LANを1つのインタフェース名まとめる
apt install bridge-utils
cat >> /etc/network/interfaces << EOL
auto br0
allow-hotplug br0
iface br0 inet static
  address 10.0.0.1/24
  bridge_stp off
  bridge_ports wlp1s0,wlx00a0b0f3d7c0
  bridge_maxwait 30
EOL

#独自のsystemdであるrc-local-latest[systemdの一番最後に実行する]でブリッジ設定を起動時に自動的に設定できるようにする
nano /etc/rc.local.latest
---
#インタフェース:br0へwlp1s0を追加
brctl addif br0 wlp1s0
#インタフェース:br0へwlx00a0b0f3d7c0を追加
brctl addif br0 wlx00a0b0f3d7c0
---
#上のフォーマットはbrctl addif インタフェース1 インタフェース2で意味はインタフェース1にインタフェース2を追加するという意味である。
#今回は２つの無線インタフェース:wlp1s0とwlx00a0b0f3d7c0をbr0に設定するようにした。

#設定確認は下記のbridge name:br0に対しinterface:wlp1s0,wlx00a0b0f3d7c0を追加していることが見て取れる。
# brctl show
bridge name     bridge id               STP enabled     interfaces
br0             8000.fe824ab35d17       no              wlp1s0
                                                        wlx00a0b0f3d7c0

```

### IPv4アドレスのみをDHCP利用する設定

```shell
#IPv4アドレスのみをDHCP利用するためにbr0の設定をする.
#br0のインタフェース名は無線アクセスポイント2つのインタフェースをブリッジしたインタフェース名とする。
nano /etc/default/isc-dhcp-server
---
INTERFACESv4="br0"
---

#インタフェース名をINTERFACES="br0";に変更する
nano /etc/dhcp/dhcpd.conf

---
default-lease-time 600;
max-lease-time 7200;
INTERFACES="br0";
option domain-name "";
max-lease-time 7200;
log-facility local7;

subnet 10.0.0.0 netmask 255.255.255.0 {
    range 10.0.0.2 10.0.0.254;
    option routers 10.0.0.1;
    option domain-name-servers 8.8.8.8;
}
---

#カキのコマンドで再度設定を読み込みしてエラーが出ないか確認する。
systemctl restart isc-dhcp-server

```


- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
