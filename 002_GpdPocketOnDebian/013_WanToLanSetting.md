# 有線LANルータにする

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)

## 目次

- [有線LANルータにする](#有線lanルータにする)
  - [目次](#目次)
  - [概要説明](#概要説明)
  - [設定](#設定)
    - [有線LANのインタフェースを確認する](#有線lanのインタフェースを確認する)

## 概要説明

GPD Pocketを有線LANルータにするにはUSB LANを2つ利用する必要があり、ルータ設定もLANに紐づけて設定する必要がある。

## 設定

### 有線LANのインタフェースを確認する

```shell
#HostOSへsshアクセスを行う
ssh reki@10.0.0.1

ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
4: wlp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br0 state UP group default qlen 1000
    link/ether b0:f1:ec:9f:84:ea brd ff:ff:ff:ff:ff:ff
5: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master vmbr0 state UP group default qlen 1000
    link/ether 74:03:bd:7f:64:39 brd ff:ff:ff:ff:ff:ff
6: enx00e04c6800a1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master vmbr1 state UP group default qlen 1000
    link/ether 00:e0:4c:68:00:a1 brd ff:ff:ff:ff:ff:ff
7: br0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether b0:f1:ec:9f:84:ea brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.1/24 scope global br0
       valid_lft forever preferred_lft forever
8: vmbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 74:03:bd:7f:64:39 brd ff:ff:ff:ff:ff:ff
9: vmbr1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:e0:4c:68:00:a1 brd ff:ff:ff:ff:ff:ff
10: tap100i0: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master fwbr100i0 state UNKNOWN group default qlen 1000
    link/ether 32:9a:d3:4c:ed:fe brd ff:ff:ff:ff:ff:ff
11: fwbr100i0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether c2:05:a5:99:a8:7a brd ff:ff:ff:ff:ff:ff
12: fwpr100p0@fwln100i0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master vmbr1 state UP group default qlen 1000
    link/ether 9e:af:2b:d7:39:11 brd ff:ff:ff:ff:ff:ff
13: fwln100i0@fwpr100p0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master fwbr100i0 state UP group default qlen 1000
    link/ether c2:05:a5:99:a8:7a brd ff:ff:ff:ff:ff:ff
14: tap100i1: <BROADCAST,MULTICAST,PROMISC,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master fwbr100i1 state UNKNOWN group default qlen 1000
    link/ether 32:94:83:49:19:b6 brd ff:ff:ff:ff:ff:ff
15: fwbr100i1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether ca:cc:d9:14:50:5f brd ff:ff:ff:ff:ff:ff
16: fwpr100p1@fwln100i1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master vmbr0 state UP group default qlen 1000
    link/ether 46:bd:2d:c6:ab:16 brd ff:ff:ff:ff:ff:ff
17: fwln100i1@fwpr100p1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master fwbr100i1 state UP group default qlen 1000
    link/ether ca:cc:d9:14:50:5f brd ff:ff:ff:ff:ff:ff
```

- [ ] [GPDPKTのDebianの初期設定 FIle:001_Main.mdへ戻る](./001_Main.md)
