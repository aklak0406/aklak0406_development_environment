# 1. Dockerコンテナ

- [ ] [README.mdへ戻る](./../README.md)

## 1.1. 目次

- [1. Dockerコンテナ](#1-dockerコンテナ)
  - [1.1. 目次](#11-目次)
  - [1.2. 概要説明](#12-概要説明)
  - [1.3. 作成するDockerコンテナの種類](#13-作成するdockerコンテナの種類)
    - [1.3.1 http経由でSubversionを構築する](#131-http経由でsubversionを構築する)
    - [1.3.2 Nginx\[プロキシできないので工事中...\]](#132-nginxプロキシできないので工事中)

## 1.2. 概要説明

 Linux上で起動できるDockerコンテナを作成する
前提条件としては以下
・一般的なLinuxの基本的な知識と利用ができる
・エラー等イレギュラーな事態に対応ができる
・Dockerコンテナを使うことができる

あったらよいものは以下
・臨機応変に対応できる
・自分でわからないことを調べることができる
・自分で切り分けができる

## 1.3. 作成するDockerコンテナの種類

### 1.3.1 http経由でSubversionを構築する

目的:Subversionで構成管理システム環境を構築することになったので、Dockerコンテナしてみる。  
追加1:LDAP認証もできるようにしておく。  
追加2:既存のユーザアカウントによるアクセス制御をhttpsへ移行するまでの間に合わせだが、そのまま流用するケースを考えてsvnプロトコルもhttpを同じDBを使用するようにする。  
↑のためにhttpとsvnとプロトコルごとにコンテナを作成した。  

参考文献URL
httpで通信できLDAP認証もできるSubversionコンテナをDocker上で構築する  
<https://qiita.com/potashi_chikash/items/de712dbfb68b079cc82b>  

- [ ] [Subversionの設定ファイル置き場](./svn)

```shell
#構築手順
#Subversionの設定ファイル置き場にあるディレクトリへ移動してからスタートする
#例,下記のディレクトリにある場合を想定
/home/reki/Dck/svn
$ tree
.
┣ Dockerfile
┣ apache
┃ ┗ run.sh
┣ docker-compose.yaml
┣ docker-compose_oneshot.yaml
┣ httpd.conf
┣ localuser.list
┣ subversion
┃ ┗ run.sh
┣ subversion-access-control
┗ svn.conf

2 directories, 9 files

#以下は一番初めに作成するもの[今回は既にsvnディレクトリへ入れてあるので不要]
docker run --rm httpd:latest cat /usr/local/apache2/conf/httpd.conf > httpd.conf

#一度subversionコンテナを起動させて、設定ファイル類をコピーする
#一度記載させるために以下を実行する

$ docker build -t subversion:latest .
...
$ docker-compose -f docker-compose_oneshot.yaml up -d
...

mkdir -p svnapache_data
#設定ファイルをコピー[apatch2の/usr/local/apache2/conf以下を全部コピー]
sudo docker cp subversion:/usr/local/apache2/conf svnapache_data/apache2_conf

#アクセス権をDockerfileに従い999:999に変更する[これをしないと起動時ファイルが見えない]
sudo chown 999:999 -R svnapache_data/apache2_conf

#設定ファイルをコピー[subversionの/etc/subversion以下を全部コピー]
sudo docker cp subversion:/etc/subversion svnapache_data/subversion_access

#アクセス権をDockerfileに従い999:999に変更する[これをしないと起動時ファイルが見えない]
sudo chown 999:999 -R svnapache_data/subversion_access

#設定ファイルをコピー[subversionの/home/svn(一般的ではなく環境固有のものであり、意味とsubversionのリポジトリである)以下を全部コピー]
sudo docker cp subversion:/home/svn svnapache_data/svn_home

#アクセス権をDockerfileに従い999:999に変更する[これをしないと起動時ファイルが見えない]
sudo chown 999:999 -R svnapache_data/svn_home

#次のコマンドを実行して、subversionのコンテナを削除する[この意味は自分で指定したsvnapache_dataディレクトリにあるデータをvolume[Dockerコンテナの用語でデータを格納する領域]としてコンテナへmountするため、管理上、コンテナを作成しなおしても同じデータを使いたいのでこのようにしていることを留意しておいてほしい。つまり、docker-build後にコンテナに入って変更するのではなく、このファイルを変更するようにすればよい。詳細は各設定ファイルを確認してコンテナの構成を把握することがポイントです。]
#コンテナを止める
docker stop subversion
#コンテナを削除する
docker rm subversion
#不要かもしれないが、イメージも削除しておく
docker rmi subversion
#再度コンテナを作成する。今度は「docker-compose.yaml」のほうで実行することになる[-f docker-compose.yamlは不要である。理由は-fを付けない場合はデフォルトでdocker-compose.yamlを読み込むためである。]
$ docker-compose up --build -d

#エラーが出なければOK

```

リポジトリの作成

```shell
# Subversionコンテナの中に入って操作を行う
$ docker exec -it subversion bash
# /home/svn/repos(svn.confファイルのSVNParentPathで設定した値)のパスに移動
subversion$ cd /home/svn/repos
# レポジトリの作成。testrepoというディレクトリが存在しない場合には自動で作成される
subversion$ svnadmin create testrepo
subversion$ ls
testrepo
# レポジトリの設定ファイルなどもsvnadmin createコマンドで自動作成されることが分かる
subversion$ ls testrepo
README.txt  conf  db  format  hooks  locks

#エラー等なければOK
```

以下へアクセスできることを確認する  
<http://[コンテナを作成した環境のIPアドレス]:8080/svn/repos/
>
以下のように表示されればOK！  
![alt text](image.png)  
ユーザ名とパスワードは「localuser.list」に設定したパスワードです。  
この場合はadminとadminになります。  
本番では必ず変えたほうがよいので以下のように変えてください。  

また、この時点で以下のパスでもsvnクライアントからアクセスができることを確認する。  
<svn://[コンテナを作成した環境のIPアドレス]/testrepo>

svn list svn://[コンテナを作成した環境のIPアドレス]/testrepo/

```shell
# 立ち上げたコンテナの中に入ってhtpasswdコマンドを実行して、ユーザーとパスワードを作成しておく
$ docker exec -it subversion bash
subversion:$ cd /home/svn
subversion:$ htpasswd -c -m localuser.list [ユーザ名]
New password:[ここにパスワードを入力する]
Re-type new password:[ここに再度パスワードを入力する]
Adding password for user [ユーザ名]
subversion:$ exit
#このあと、アクセスを確認する。問題あればコンテナを再起動させる
docker restart subversion
# この後に先ほど作成したlocaluser.listをホストOSにコピーして,コンテナを再度作成するときもデータを引き継いでおくこともできるので行う。
docker cp subversion:/home/svn/localuser.list .
```

これで構築作業は終了です。

細かいプロジェクトやユーザの設定は以下のファイルを参照して通常のapache2やsubversionの設定を確認し変更、アクセス確認や変更をしてください。  
Dockerfile_http  
Dockerfile_svn  
docker-compose.yaml  
  
あと、svnプロトコルにおけるユーザのアクセス権限を導入したときのdefault[.orgファイル]からの変更点は下記を参考にしてdiffを取り確認してほしい。  
./svn/sample_svn_conf/testrepo/conf  
  
また懸念レベルだが,もし999のUIDとGIDがない場合はHost環境でchownの権限変更が失敗する可能性[存在しないことによる]があるので、ない場合は適当にuseraddで作成しておく。この場合ユーザ名は任意でよい。この権限のファイルをhost側で変更する場合はrootユーザなどで変更すればよい。[私の環境ではすでに別ユーザで変更だったので不要だったので未実施である]  
useradd [ユーザ名] -u 999 -g 999

### 1.3.2 Nginx[プロキシできないので工事中...]

目的:NginxはWebサーバとしての役割とリバースプロキシとしても利用される。
今回は以下ができるようにする。
・ほかのサーバをhttpsでアクセスをさせるためにリバースプロキシにすること
・複数のWebサービスを1つのhttpsで一元管理する

- [ ] [Nginxの設定ファイル置き場](./nginx)

```shell
#構築手順
#Nginxの設定ファイル置き場にあるディレクトリへ移動してからスタートする

/home/reki/Dck/nginx

$ tree
.
├── docker-compose.yml
└── reverse-proxy
    └── nginx.conf

$ docker build -t reverseproxynginx:latest .
...
$ docker-compose up -d
...

```

- [ ] [README.mdへ戻る](./../README.md)
