#!/bin/bash

# 'already pid is running'というエラーを避けるために、pidファイルを削除
rm -f /run/apache2/httpd.pid

# httpd（Apache）をフォアグラウンドで起動 このscriptの実行をした場合、変化がhttpd-foregroundというプロセスが4->5へ追加されただけのように見えるのであまり意味はなさそう?[httpdのコンテナなので意味はないのかもしれない]
exec /usr/local/apache2/bin/httpd -DFOREGROUND;
