#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#このscriptはrootユーザで行ってください。
#また、OSはDebian12にまずは限定しています。

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#意味
#関数:ShCmdLogは標準出力、標準エラー出力を年月日_時分秒_ミリ秒_CmdHist.logファイルへ書き込みをするための設定を行います。
ShCmdLog() {
  local filename=$(date +'%Y%m%d_%H%M%S_%3N')_CmdHist.log
  exec > ${filename} 2> ${filename}
}

ShCmdLog

#CD-ROMをコメントアウトする
sed -i 's|^deb cdrom:[Debian GNU/Linux 11.8.0 _Bullseye_ - Official amd64 DVD Binary-1 20231007-14:05]/ bullseye contrib main|#deb cdrom:[Debian GNU/Linux 11.8.0 _Bullseye_ - Official amd64 DVD Binary-1 20231007-14:05]/ bullseye contrib main' /etc/apt/sources.list

time apt update -y && time apt upgrade -y

cat >> ~/.bashrc << EOL
case \$TERM in
      linux) LANGUAGE=en:en
               LANG=C ;;
           *) LANGUAGE=ja:en
               LANG=ja_JP.UTF-8 ;;
esac
EOL

echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list
wget https://enterprise.proxmox.com/debian/proxmox-release-bullseye.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg
hostname --ip-address
sed -i 's/^127\.0\.1\.1\s\+MyRouter/#127.0.1.1      MyRouter/' /etc/hosts
echo "192.168.1.221   MyRouter" >> /etc/hosts
hostname --ip-address
time apt update -y && time apt full-upgrade -y
time apt install proxmox-ve postfix open-iscsi -y
sed -i 's|^deb https://enterprise.proxmox.com/debian/pve bullseye pve-enterprise|#deb https://enterprise.proxmox.com/debian/pve bullseye pve-enterprise|' /etc/apt/sources.list.d/pve-enterprise.list
sed -i 's|^iface enp2s0 inet manual|iface enp2s0 inet dhcp|' /etc/network/interfaces

### ここまで処理実行に必要なコマンドラインを記載する  ###

#再起動させて反映する
reboot

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

