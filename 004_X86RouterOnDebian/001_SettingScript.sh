#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#このscriptはrootユーザで行ってください。
#また、OSはDebian12にまずは限定しています。

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#意味
#関数:ShCmdLogは標準出力、標準エラー出力を年月日_時分秒_ミリ秒_CmdHist.logファイルへ書き込みをするための設定を行います。
ShCmdLog() {
  local filename=$(date +'%Y%m%d_%H%M%S_%3N')_CmdHist.log
  exec > ${filename} 2> ${filename}
}

ShCmdLog

#backupを取っておく
cp -pf /etc/apt/sources.list /etc/apt/sources.list.backup
#Debianを11から12へ変更する設定
sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list
sed -i 's/non-free/non-free non-free-firmware/g' /etc/apt/sources.list
sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list.d/pve-install-repo.list

wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg

#Debianを11から12へ変更のためにパッケージを更新
apt update -y
#Debianを11から12へ変更のためにパッケージのinstall
yes | apt-get -y full-upgrade

### ここまで処理実行に必要なコマンドラインを記載する  ###

#再起動させて反映する
reboot

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

