# 1.X86のPCRouterでPromoxVE

- [1.X86のPCRouterでPromoxVE](#1x86のpcrouterでpromoxve)
  - [1.2.概要説明](#12概要説明)
  - [1.3.前提条件](#13前提条件)
  - [1.4.PCの構成](#14pcの構成)
  - [1.5.ProxmoxのInstall](#15proxmoxのinstall)
  - [1.6.WIFIドライバー有効化](#16wifiドライバー有効化)
  - [1.6.1.Intel Corporation Centrino Advanced-N 6205](#161intel-corporation-centrino-advanced-n-6205)
  - [1.6.2.Ugreen AX1800 USB 3.0](#162ugreen-ax1800-usb-30)
  - [1.6.1.WIFIドライバーのインストール](#161wifiドライバーのインストール)
  - [1.6.2.WIFIドライバーの設定と動作確認](#162wifiドライバーの設定と動作確認)
    - [1.6.2.1.WIFIの情報を確認する](#1621wifiの情報を確認する)
    - [1.6.2.2.共通設定](#1622共通設定)
    - [1.6.2.3.Ugreen AX1800 USB 3.0の設定](#1623ugreen-ax1800-usb-30の設定)
    - [1.6.2.4.Ugreen AX1800 USB 3.0の設定続き](#1624ugreen-ax1800-usb-30の設定続き)
    - [1.6.2.5.共通設定の続き](#1625共通設定の続き)
    - [1.6.2.6.DHCPサーバーのインストール](#1626dhcpサーバーのインストール)
  - [1.6.2.6.動作確認を戻し、無線LANのみ自動起動する](#1626動作確認を戻し無線lanのみ自動起動する)
  - [1.6.3.OpenWrtのInstall](#163openwrtのinstall)
  - [1.6.4.電源投入時のみnetwork設定が失敗する対策をする](#164電源投入時のみnetwork設定が失敗する対策をする)
  - [1.6.5.HostOS側のセキュリティ設定Iptables](#165hostos側のセキュリティ設定iptables)
  - [1.6.6.HostOS側のセキュリティ設定Clamav](#166hostos側のセキュリティ設定clamav)
    - [1.6.6.1.ClamavのBuild](#1661clamavのbuild)
    - [1.6.6.2.ClamavのInstall](#1662clamavのinstall)
      - [1.6.6.2.1.ClamavのServer](#16621clamavのserver)
      - [1.6.6.2.2.ClamavのClient](#16622clamavのclient)
  - [1.6.7.セキュリティマネージメント Wazuh](#167セキュリティマネージメント-wazuh)
    - [1.6.7.1.サーバ側の構築手順](#1671サーバ側の構築手順)
    - [1.6.7.2.HostOS側のセキュリティ設定Wazuh Agent](#1672hostos側のセキュリティ設定wazuh-agent)
      - [1.6.7.2.1.ProxmoxVE上にWazuh Agentをインストールする](#16721proxmoxve上にwazuh-agentをインストールする)
      - [1.6.7.2.2.OpenwrtのRouter:Turris上でIPSのSuricataとWazuh Agentをインストールする](#16722openwrtのrouterturris上でipsのsuricataとwazuh-agentをインストールする)
      - [1.6.7.2.3.OpenwrtでsnortのIPS機能を有効化する](#16723openwrtでsnortのips機能を有効化する)
  - [1.6.8.ClamAvでGoogle Safe Browsing を利用する](#168clamavでgoogle-safe-browsing-を利用する)
  - [1.6.9 HiDS/EDRのWazuhを利用する](#169-hidsedrのwazuhを利用する)
  - [1.6.1.OpenWrtののWebUIを外から見えるようにする](#161openwrtののwebuiを外から見えるようにする)

## 1.2.概要説明

このファイルではX86のPCRouterでPromoxVEを構築し、OpenwrtでRouterとして動作させて家庭内ネットワークを作る試みを行います。  

## 1.3.前提条件

OSのインストールは以下参照

- [ ] [インストール](../001_DebianLinuxOnPromoxVE/002_Install.md)

OSのアップグレードは以下参照[こちらはPromoxVEでのOSアップグレード含む]

- [ ] [アップグレード](../000_Common/003_OSDebianChange.md)

日本語インストール後のCLIのみは英語にして文字化け防止する方法

``` shell
nano ~/.bashrc
---
case $TERM in
      linux) LANGUAGE=en:en
               LANG=C ;;
           *) LANGUAGE=ja:en
               LANG=ja_JP.UTF-8 ;;
esac
---
source ~/.bashrc

```

## 1.4.PCの構成

CPU:pentium silver n6000 [4Cores/4Threads]  
<https://www.intel.co.jp/content/www/jp/ja/products/sku/212330/intel-pentium-silver-n6000-processor-4m-cache-up-to-3-30-ghz/specifications.html>  
メモリ:DDR4 2400T 16GB×2  
ストレージ:HDD 1TB×1  
ネットワーク:2.5Gbps×4,WIFI:Intel Advanced Networking N 6205 [802.11a,b,g,n Max:300Mbps]  

## 1.5.ProxmoxのInstall

仮想化基盤にはPromox[KVMでQEMUとLXC/LXDのアプライアンス]を使用します。
また、仮想化基盤上でOpenwrtをインストールします。

PromoxVEのインストールは以下参照

``` bash
#Promox VEのapt リポジトリを登録する
echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list
#リポジトリキーを追加
wget https://enterprise.proxmox.com/debian/proxmox-release-bullseye.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg
#リポジトリとシステム更新を行う。
apt update && apt full-upgrade

#/etc/hosts にホスト名とIPアドレスを書く。
#元々書かれている 127.0.1.1 ホスト名 は削除しておく。
#以下を実行して、127.0.1.1ではないIPアドレスが返ってくれば良し。
#IP確認
hostname --ip-address
127.0.1.1 192.168.1.221
#確認したIP:192.168.1.221のみにするように/etc/hostsに変更
nano /etc/hosts
---
127.0.1.1      MyRouter
↓
#127.0.1.1      MyRouter
---
hostname --ip-address
192.168.1.221

#Promox VEのパッケージをインストールする
apt install proxmox-ve postfix open-iscsi

#enterprizeリポジトリが追加されているので無効にする。enterprize用の機能は有料なので使用しない。
nano /etc/apt/sources.list.d/pve-enterprise.list
---
deb https://enterprise.proxmox.com/debian/pve bullseye pve-enterprise
↓
#deb https://enterprise.proxmox.com/debian/pve bullseye pve-enterprise
---
#再起動して反映確認する。
reboot

#再起動後、アクセスできない場合は再起動をすると復活する場合がある。
#なお、networkの設定は様々だが、私の環境では以下で設定されていた。
#状態確認
systemctl status NetworkManager
#詳細内容
nmcli -a
#設定値の確認
cat /etc/NetworkManager/system-connections/Wired\ connection\ 1
#以下のケースもあり。
systemctl status networking.service
#詳細内容確認
ip a
#設定値
cat /etc/network/interfaces

```

## 1.6.WIFIドライバー有効化

 今回WIFIのアクセスポイントをPCで実行するために以下のWIFIアダプターを使用しました。

## 1.6.1.Intel Corporation Centrino Advanced-N 6205

  2.4Gと5Gの2つのWIFIアダプターが利用できるらしいが、実質AP利用できたのは2.4Gのみだった。  
  11a/b/g/n 300Mbpsまで可  
  補足:IntelのWIFIアダプターは通常市場に出回っているものはほとんどはクライアント向けのためAPモードは2.4Gのみ利用できる。  
  そのため、Intelのものは全般的に2.4Gのみ対応となる。  

## 1.6.2.Ugreen AX1800 USB 3.0

  こちらは5GのWIFIアダプターが利用できるらしい。→5Gだとすべての周波数帯でno IRで利用不可能。2.4GHzしかないが、電波の範囲が非常に広く,2回建ての家屋全部カバーしている。  
　11a/b/g/n/ac/ax 1200Mbpsまで可能らしいが2.4Ghzまでだと300Mbpsが限界。  
  ネットを見ると蟹チップらしい
[This script is for installing the UGREEN AX1800 (Realtek rtl8852au) USB WiFi adapter on SteamOS, may work on other dongles using the same chip but I didn´t test it.](https://gist.github.com/snooptheone/70742ff0c8ef386c45cda8d2ba34ab4e)
↑は誤りだった。時期によってチップが異なる可能性あり。  
正解は以下だった。  
rtl8832BU
ただ、rtl8832BUはrtl8852buと同じもののようです。ので8852のものを利用します。  
<https://github.com/morrownr/rtl8852bu>  

調査方法は以下  

- 1.「Ugreen AX1800 USB 3.0」はWindows版でしかドライバー提供されていない.  
情報源は以下で「Compatible with Windows  
This USB 3.0 WiFi dongle is compatible with Windows 10/11, just click the pre-installed driver to complete the installation in seconds.  
Note: This WiFi adapter is not compatible with other systems such as Linux and Mac OS.」と記載あるのでLinuxおよびMac OSに対応していない。  
<https://groovegadget.com.my/product/ugreen-ax1800-wifi-adapter-with-dual-antenna-wifi6-5g-24g-dual-band-usb-30-wifi-dongle-for-windows-11-10-8-7-pc-laptop>  

- 2.WindowsへインストールしてどのチップのDriverが割り当てられているのか確認する。  
インストールはこのWIFIアダプターはUSB挿入後に自動でWindowsドライバーがインストールはされるので、特に作業は必要なかった。  
インストール語、設定>ネットワークとインターネット>WIFI>WIFI 3[この部分はPCのWIFIアダプターに認識順番によって変わる認識]では「ReadlTek 8832BU WireLess LAN WIFI 6 USB NIC」となっていることがわかる。  
![Windows上での8832BU](image.png)

以下はインストール後のlsusbでの確認結果です。参考まで  

``` shell
lsusb -v | grep "Real"
Bus 002 Device 002: ID 0bda:b832 Realtek Semiconductor Corp. 802.11ax WLAN Adapter
  idVendor           0x0bda Realtek Semiconductor Corp.
  iManufacturer           1 Realtek
```

## 1.6.1.WIFIドライバーのインストール

[Ugreen AX1800 USB 3.0](#162ugreen-ax1800-usb-30)のインストール方法

``` shell
#理由はWindows側でインストールしたところ、ドライバがrtl8852buであることが判明した。
#結果以下のサイトを参照しインストール作業を行う。
https://github.com/morrownr/rtl8852bu?tab=readme-ov-file

sudo apt update && sudo apt upgrade
#以下は既にインストールしている「linux-headers-$(uname -r)」があるためNG
sudo apt install -y linux-headers-$(uname -r) build-essential bc dkms git libelf-dev rfkill iw
#↑のため以下を実施
sudo apt install -y build-essential bc dkms git libelf-dev rfkill iw
#↑が下記の失敗エラーがでた場合、linux-headers-$(uname -r)が失敗なので、代わりにapt install pve-headersを実施
E: パッケージ linux-headers-6.8.12-1-pve が見つかりません
E: 'linux-headers-6.8.12-1-pve' に一致するパッケージは見つかりませんでした

mkdir -p ~/src
cd ~/src
git clone https://github.com/morrownr/rtl8852bu.git
cd ~/src/rtl8852bu
#下記のOSをbuildしたときのgccと現在のgccは一致するので問題ない
cat /proc/version
gcc --version
#installする
sudo ./install-driver.sh

#再起動
reboot

#またVersionUp等でいったんUninstallする必要がある場合は以下
sudo make uninstall
sudo reboot

#以下のコマンドで11axのDriverで起動していることを確認
lsusb
Bus 002 Device 002: ID 0bda:b832 Realtek Semiconductor Corp. 802.11ax WLAN Adapter
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub

#以下のコマンドでLoadしていることを確認
dmesg | tail -n 5
[  100.961813] usb 1-2: USB disconnect, device number 22
[  104.232715] usb 2-2: new SuperSpeed USB device number 2 using xhci_hcd
[  104.253484] usb 2-2: New USB device found, idVendor=0bda, idProduct=b832, bcdDevice= 0.00
[  104.253490] usb 2-2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[  104.253492] usb 2-2: Product: 802.11ax WLAN Adapter
[  104.253493] usb 2-2: Manufacturer: Realtek
[  104.253494] usb 2-2: SerialNumber: 00e04c000001
[  104.664423] eric-tx CALL alloc_txring !!!!
[  104.673166] rtl8852bu 2-2:1.0 wlx6c1ff7124a13: renamed from wlan0
[  104.808161] [WARNING][BB]Becareful it is fwofld mode in BB init !!
[  104.893745] [BB][halbb_la_bb_set_smp_rate] smp_rate_tmp=7, la_smp_rate_log=160 M
[  105.046307] IPv6: ADDRCONF(NETDEV_CHANGE): wlx6c1ff7124a13: link becomes ready
[  105.234349] [WARNING][BB]Skip gain error setting in scan status
以下のコマンドで確認した。
nmcli -a
wlx6c1ff7124a13: disconnected
        "Realtek 802.11ax"
        wifi (rtl8852bu), BE:9C:96:92:FB:0F, hw, mtu 1500

#ip aコマンドでも確認
ip a
・
・省略
・
24: wlx6c1ff7124a13: <NO-CARRIER,BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state DORMANT group default qlen 1000
    link/ether 42:01:53:d8:9d:41 brd ff:ff:ff:ff:ff:ff permaddr 6c:1f:f7:12:4a:13

```

## 1.6.2.WIFIドライバーの設定と動作確認

この目的はHostOS側でhostapdによる無線LANの設定を行うことです。  
Openwrt側ではbrigeによる設定は無線フレームをフィルタしないとダメかもしれません。  
↑の根拠は以下のWLANのWikiの重要な注意事項に記載されています。  
<https://pve.proxmox.com/wiki/WLAN>  

↑のため、できうるならWIFIのAPを設定することは避けた方が無難かもしれないです。  
ちなみに私は当然上のことを承知した上でやります。  
Wikiによると３つ方法があるようですが、マスカレード (NAT)の方を採用します。  
理由は単に私がVMやCT[LXC]をいつもNATで設定しているため、とっつきやすいためですので完全に私自身の都合です。  
余裕があればほかの2つの方法も試してみますが、現在はやる気がないのでご承知のほどよろしくお願いいたします。  

まずはHost側でWIFIデバイスを利用可能にして、通常のLinuxOS上でWIFIアクセスポイントとして利用するところから始めます。  
理由は動作確認をしたいためです。  
これをやらないとWIFIアダプタ自身の問題か[故障?など]、WIFIアクセスポイントの設定の問題か[ソフト面やパラメータなどの問題]、それとも環境面の問題[例、DHCPサーバやクライアント]などの問題が発生する可能性があるのか確認のためやっておいた方がよいです。  

### 1.6.2.1.WIFIの情報を確認する

``` shell
#無線インタフェースは頭文字にwが付いている6、8になります。
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master vmbr0 state UP group default qlen 1000
    link/ether 60:be:b4:19:2b:9c brd ff:ff:ff:ff:ff:ff
3: enp3s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 60:be:b4:19:2b:9d brd ff:ff:ff:ff:ff:ff
4: enp4s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 60:be:b4:19:2b:9e brd ff:ff:ff:ff:ff:ff
5: enp5s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq master vmbr1 state DOWN group default qlen 1000
    link/ether 60:be:b4:19:2b:9f brd ff:ff:ff:ff:ff:ff
6: wlp1s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether c8:15:4e:fe:57:18 brd ff:ff:ff:ff:ff:ff
8: wlx6c1ff7124a13: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq master vmbr1 state UP group default qlen 1000
    link/ether 6c:1f:f7:12:4a:13 brd ff:ff:ff:ff:ff:ff
9: vmbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 60:be:b4:19:2b:9c brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.221/24 brd 192.168.1.255 scope global dynamic vmbr0
       valid_lft 84265sec preferred_lft 84265sec
    inet6 fe80::62be:b4ff:fe19:2b9c/64 scope link
       valid_lft forever preferred_lft forever
10: vmbr1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 60:be:b4:19:2b:9f brd ff:ff:ff:ff:ff:ff
    inet6 fe80::62be:b4ff:fe19:2b9f/64 scope link
       valid_lft forever preferred_lft forever

#iwコマンドでphy#0,2が上のインタフェース8,6の情報を確認する
 iw dev
phy#2
        Interface wlx6c1ff7124a13
                ifindex 8
                wdev 0x200000001
                addr 6c:1f:f7:12:4a:13
                ssid MuRtlwifi
                type AP
                channel 11 (2462 MHz), width: 20 MHz, center1: 2462 MHz
                txpower 12.00 dBm
phy#0
        Interface wlp1s0
                ifindex 6
                wdev 0x1
                addr c8:15:4e:fe:57:18
                type managed
                multicast TXQ:
                        qsz-byt qsz-pkt flows   drops   marks   overlmt hashcol tx-bytes        tx-packets
                        0       0       0       0       0       0       0       0               0
#無線のphyがどのようなパラメータ、機能に対応しているか確認できる。
 iw phy
Wiphy phy2
        wiphy index: 2
        max # scan SSIDs: 9
        max scan IEs length: 2304 bytes
        max # sched scan SSIDs: 0
        max # match sets: 0
        Retry short limit: 7
        Retry long limit: 4
        Coverage class: 0 (up to 0m)
        Device supports AP-side u-APSD.
        Device supports T-DLS.
        Supported Ciphers:
                * WEP40 (00-0f-ac:1)
                * WEP104 (00-0f-ac:5)
                * TKIP (00-0f-ac:2)
                * CCMP-128 (00-0f-ac:4)
                * CMAC (00-0f-ac:6)
                * GCMP-128 (00-0f-ac:8)
                * GCMP-256 (00-0f-ac:9)
                * CCMP-256 (00-0f-ac:10)
                * GMAC-128 (00-0f-ac:11)
                * GMAC-256 (00-0f-ac:12)
                * CMAC-256 (00-0f-ac:13)
        Available Antennas: TX 0 RX 0
        Supported interface modes:
                 * IBSS
                 * managed
                 * AP
                 * monitor
                 * P2P-client
                 * P2P-GO
        Band 1:
                Capabilities: 0x19e3
                        RX LDPC
                        HT20/HT40
                        Static SM Power Save
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 16 usec (0x07)
                HT Max RX data rate: 300 Mbps
                HT TX/RX MCS rate indexes supported: 0-15
                HE Iftypes: AP
                        HE MAC Capabilities (0x0001120a0060):
                                +HTC HE Supported
                                All Ack
                                BSR
                                OM Control
                                Maximum A-MPDU Length Exponent: 2
                                OPS
                                A-MSDU in A-MPDU
                        HE PHY Capabilities: (0x02701a580dc0270e91b900):
                                HE40/2.4GHz
                                Device Class: 1
                                LDPC Coding in Payload
                                HE SU PPDU with 1x HE-LTF and 0.8us GI
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Rx <= 80MHz
                                Doppler Tx
                                DCM Max Constellation Rx: 3
                                Rx HE MU PPDU from Non-AP STA
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 3
                                Ng = 16 SU Feedback
                                Ng = 16 MU Feedback
                                Codebook Size SU Feedback
                                Codebook Size MU Feedback
                                Triggered SU Beamforming Feedback
                                Partial Bandwidth Extended Range
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 1
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 1x HE-LTF 0.8us GI
                                DCM Max BW: 2
                                Longer Than 16HE SIG-B OFDM Symbols
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                HE Iftypes: Station
                        HE MAC Capabilities (0x0801120a8060):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                All Ack
                                BSR
                                OM Control
                                Maximum A-MPDU Length Exponent: 2
                                OPS
                                A-MSDU in A-MPDU
                        HE PHY Capabilities: (0x02701a1f0d00270e91bd00):
                                HE40/2.4GHz
                                Device Class: 1
                                LDPC Coding in Payload
                                HE SU PPDU with 1x HE-LTF and 0.8us GI
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Rx <= 80MHz
                                Doppler Tx
                                DCM Max Constellation: 3
                                DCM Max NSS Tx: 1
                                DCM Max Constellation Rx: 3
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 3
                                Codebook Size SU Feedback
                                Codebook Size MU Feedback
                                Triggered SU Beamforming Feedback
                                Partial Bandwidth Extended Range
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 1
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 1x HE-LTF 0.8us GI
                                DCM Max BW: 2
                                Longer Than 16HE SIG-B OFDM Symbols
                                TX 1024-QAM
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                Bitrates (non-HT):
                        * 1.0 Mbps
                        * 2.0 Mbps
                        * 5.5 Mbps
                        * 11.0 Mbps
                        * 6.0 Mbps
                        * 9.0 Mbps
                        * 12.0 Mbps
                        * 18.0 Mbps
                        * 24.0 Mbps
                        * 36.0 Mbps
                        * 48.0 Mbps
                        * 54.0 Mbps
                Frequencies:
                        * 2412 MHz [1] (20.0 dBm)
                        * 2417 MHz [2] (20.0 dBm)
                        * 2422 MHz [3] (20.0 dBm)
                        * 2427 MHz [4] (20.0 dBm)
                        * 2432 MHz [5] (20.0 dBm)
                        * 2437 MHz [6] (20.0 dBm)
                        * 2442 MHz [7] (20.0 dBm)
                        * 2447 MHz [8] (20.0 dBm)
                        * 2452 MHz [9] (20.0 dBm)
                        * 2457 MHz [10] (20.0 dBm)
                        * 2462 MHz [11] (20.0 dBm)
                        * 2467 MHz [12] (20.0 dBm) (no IR)
                        * 2472 MHz [13] (20.0 dBm) (no IR)
                        * 2484 MHz [14] (disabled)
        Band 2:
                Capabilities: 0x19e3
                        RX LDPC
                        HT20/HT40
                        Static SM Power Save
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 16 usec (0x07)
                HT Max RX data rate: 300 Mbps
                HT TX/RX MCS rate indexes supported: 0-15
                VHT Capabilities (0x03c011b1):
                        Max MPDU length: 7991
                        Supported Channel Width: neither 160 nor 80+80
                        RX LDPC
                        short GI (80 MHz)
                        TX STBC
                        SU Beamformee
                        +HTC-VHT
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: not supported
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 867 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: not supported
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 867 Mbps
                HE Iftypes: AP
                        HE MAC Capabilities (0x0001120a0060):
                                +HTC HE Supported
                                All Ack
                                BSR
                                OM Control
                                Maximum A-MPDU Length Exponent: 2
                                OPS
                                A-MSDU in A-MPDU
                        HE PHY Capabilities: (0x04701a580dc0270e91b900):
                                HE40/HE80/5GHz
                                Device Class: 1
                                LDPC Coding in Payload
                                HE SU PPDU with 1x HE-LTF and 0.8us GI
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Rx <= 80MHz
                                Doppler Tx
                                DCM Max Constellation Rx: 3
                                Rx HE MU PPDU from Non-AP STA
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 3
                                Ng = 16 SU Feedback
                                Ng = 16 MU Feedback
                                Codebook Size SU Feedback
                                Codebook Size MU Feedback
                                Triggered SU Beamforming Feedback
                                Partial Bandwidth Extended Range
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 1
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 1x HE-LTF 0.8us GI
                                DCM Max BW: 2
                                Longer Than 16HE SIG-B OFDM Symbols
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                HE Iftypes: Station
                        HE MAC Capabilities (0x0801120a8060):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                All Ack
                                BSR
                                OM Control
                                Maximum A-MPDU Length Exponent: 2
                                OPS
                                A-MSDU in A-MPDU
                        HE PHY Capabilities: (0x04701a1f0d00270e91bd00):
                                HE40/HE80/5GHz
                                Device Class: 1
                                LDPC Coding in Payload
                                HE SU PPDU with 1x HE-LTF and 0.8us GI
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Rx <= 80MHz
                                Doppler Tx
                                DCM Max Constellation: 3
                                DCM Max NSS Tx: 1
                                DCM Max Constellation Rx: 3
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 3
                                Codebook Size SU Feedback
                                Codebook Size MU Feedback
                                Triggered SU Beamforming Feedback
                                Partial Bandwidth Extended Range
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 1
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 1x HE-LTF 0.8us GI
                                DCM Max BW: 2
                                Longer Than 16HE SIG-B OFDM Symbols
                                TX 1024-QAM
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                Bitrates (non-HT):
                        * 6.0 Mbps
                        * 9.0 Mbps
                        * 12.0 Mbps
                        * 18.0 Mbps
                        * 24.0 Mbps
                        * 36.0 Mbps
                        * 48.0 Mbps
                        * 54.0 Mbps
                Frequencies:
                        * 5180 MHz [36] (20.0 dBm) (no IR)
                        * 5200 MHz [40] (20.0 dBm) (no IR)
                        * 5220 MHz [44] (20.0 dBm) (no IR)
                        * 5240 MHz [48] (20.0 dBm) (no IR)
                        * 5260 MHz [52] (20.0 dBm) (no IR)
                        * 5280 MHz [56] (20.0 dBm) (no IR)
                        * 5300 MHz [60] (20.0 dBm) (no IR)
                        * 5320 MHz [64] (20.0 dBm) (no IR)
                        * 5500 MHz [100] (20.0 dBm) (no IR)
                        * 5520 MHz [104] (20.0 dBm) (no IR)
                        * 5540 MHz [108] (20.0 dBm) (no IR)
                        * 5560 MHz [112] (20.0 dBm) (no IR)
                        * 5580 MHz [116] (20.0 dBm) (no IR)
                        * 5600 MHz [120] (20.0 dBm) (no IR)
                        * 5620 MHz [124] (20.0 dBm) (no IR)
                        * 5640 MHz [128] (20.0 dBm) (no IR)
                        * 5660 MHz [132] (20.0 dBm) (no IR)
                        * 5680 MHz [136] (20.0 dBm) (no IR)
                        * 5700 MHz [140] (20.0 dBm) (no IR)
                        * 5720 MHz [144] (20.0 dBm) (no IR)
                        * 5745 MHz [149] (20.0 dBm) (no IR)
                        * 5765 MHz [153] (20.0 dBm) (no IR)
                        * 5785 MHz [157] (20.0 dBm) (no IR)
                        * 5805 MHz [161] (20.0 dBm) (no IR)
                        * 5825 MHz [165] (20.0 dBm) (no IR)
                        * 5845 MHz [169] (20.0 dBm) (no IR)
                        * 5865 MHz [173] (20.0 dBm) (no IR)
                        * 5885 MHz [177] (20.0 dBm) (no IR)
        Supported commands:
                 * new_interface
                 * set_interface
                 * new_key
                 * start_ap
                 * new_station
                 * set_bss
                 * join_ibss
                 * set_pmksa
                 * del_pmksa
                 * flush_pmksa
                 * remain_on_channel
                 * frame
                 * set_channel
                 * tdls_mgmt
                 * tdls_oper
                 * connect
                 * disconnect
                 * channel_switch
        WoWLAN support:
                 * wake up on anything (device continues operating normally)
        software interface modes (can always be added):
                 * monitor
        interface combinations are not supported
        Device supports SAE with AUTHENTICATE command
        Device supports scan flush.
        Device supports randomizing MAC-addr in scans.
        max # scan plans: 1
        max scan plan interval: -1
        max scan plan iterations: 0
        Supported TX frame types:
                 * IBSS: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * managed: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * AP: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * AP/VLAN: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * P2P-client: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * P2P-GO: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
        Supported RX frame types:
                 * IBSS: 0xd0
                 * managed: 0x40 0xb0 0xd0
                 * AP: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
                 * AP/VLAN: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
                 * P2P-client: 0x40 0xd0
                 * P2P-GO: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
        Supported extended features:
                * [ DFS_OFFLOAD ]: DFS offload
Wiphy phy0
        wiphy index: 0
        max # scan SSIDs: 20
        max scan IEs length: 365 bytes
        max # sched scan SSIDs: 20
        max # match sets: 8
        Retry short limit: 7
        Retry long limit: 4
        Coverage class: 0 (up to 0m)
        Device supports RSN-IBSS.
        Device supports AP-side u-APSD.
        Device supports T-DLS.
        Supported Ciphers:
                * WEP40 (00-0f-ac:1)
                * WEP104 (00-0f-ac:5)
                * TKIP (00-0f-ac:2)
                * CCMP-128 (00-0f-ac:4)
                * GCMP-128 (00-0f-ac:8)
                * GCMP-256 (00-0f-ac:9)
                * CMAC (00-0f-ac:6)
                * GMAC-128 (00-0f-ac:11)
                * GMAC-256 (00-0f-ac:12)
        Available Antennas: TX 0x3 RX 0x3
        Configured Antennas: TX 0x3 RX 0x3
        Supported interface modes:
                 * IBSS
                 * managed
                 * AP
                 * AP/VLAN
                 * monitor
                 * P2P-client
                 * P2P-GO
                 * P2P-device
        Band 1:
                Capabilities: 0x19ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 4 usec (0x05)
                HT Max RX data rate: 300 Mbps
                HT TX/RX MCS rate indexes supported: 0-15
                HE Iftypes: Station
                        HE MAC Capabilities (0x78019a20abc0):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                32-bit BA Bitmap
                                OM Control
                                Maximum A-MPDU Length Exponent: 3
                                RX Control Frame to MultiBSS
                                A-MSDU in A-MPDU
                                Multi-TID Aggregation TX: 7
                                UL 2x996-Tone RU
                        HE PHY Capabilities: (0x0e3f0e00fd098c160ffc01):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                HE160/5GHz
                                Punctured Preamble RX: 15
                                Device Class: 1
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 7
                                Beamformee STS > 80Mhz: 7
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                Triggered SU Beamforming Feedback
                                Triggered MU Beamforming Feedback
                                PPE Threshold Present
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 2
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                20MHz in 40MHz HE PPDU 2.4GHz
                                20MHz in 160/80+80MHz HE PPDU
                                80MHz in 160/80+80MHz HE PPDU
                                TX 1024-QAM
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE RX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                HE Iftypes: AP
                        HE MAC Capabilities (0x78011a000000):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                OM Control
                                Maximum A-MPDU Length Exponent: 3
                        HE PHY Capabilities: (0x06200e000009800401c400):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                PPE Threshold Present
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                TX 1024-QAM
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                Bitrates (non-HT):
                        * 1.0 Mbps
                        * 2.0 Mbps (short preamble supported)
                        * 5.5 Mbps (short preamble supported)
                        * 11.0 Mbps (short preamble supported)
                        * 6.0 Mbps
                        * 9.0 Mbps
                        * 12.0 Mbps
                        * 18.0 Mbps
                        * 24.0 Mbps
                        * 36.0 Mbps
                        * 48.0 Mbps
                        * 54.0 Mbps
                Frequencies:
                        * 2412 MHz [1] (22.0 dBm)
                        * 2417 MHz [2] (22.0 dBm)
                        * 2422 MHz [3] (22.0 dBm)
                        * 2427 MHz [4] (22.0 dBm)
                        * 2432 MHz [5] (22.0 dBm)
                        * 2437 MHz [6] (22.0 dBm)
                        * 2442 MHz [7] (22.0 dBm)
                        * 2447 MHz [8] (22.0 dBm)
                        * 2452 MHz [9] (22.0 dBm)
                        * 2457 MHz [10] (22.0 dBm)
                        * 2462 MHz [11] (22.0 dBm)
                        * 2467 MHz [12] (22.0 dBm)
                        * 2472 MHz [13] (22.0 dBm)
                        * 2484 MHz [14] (22.0 dBm)
        Band 2:
                Capabilities: 0x19ef
                        RX LDPC
                        HT20/HT40
                        SM Power Save disabled
                        RX HT20 SGI
                        RX HT40 SGI
                        TX STBC
                        RX STBC 1-stream
                        Max AMSDU length: 7935 bytes
                        DSSS/CCK HT40
                Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
                Minimum RX AMPDU time spacing: 4 usec (0x05)
                HT Max RX data rate: 300 Mbps
                HT TX/RX MCS rate indexes supported: 0-15
                VHT Capabilities (0x039071f6):
                        Max MPDU length: 11454
                        Supported Channel Width: 160 MHz
                        RX LDPC
                        short GI (80 MHz)
                        short GI (160/80+80 MHz)
                        TX STBC
                        SU Beamformee
                        MU Beamformee
                VHT RX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: not supported
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT RX highest supported: 0 Mbps
                VHT TX MCS set:
                        1 streams: MCS 0-9
                        2 streams: MCS 0-9
                        3 streams: not supported
                        4 streams: not supported
                        5 streams: not supported
                        6 streams: not supported
                        7 streams: not supported
                        8 streams: not supported
                VHT TX highest supported: 0 Mbps
                HE Iftypes: Station
                        HE MAC Capabilities (0x78018a20abc0):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                32-bit BA Bitmap
                                OM Control
                                Maximum A-MPDU Length Exponent: 1
                                RX Control Frame to MultiBSS
                                A-MSDU in A-MPDU
                                Multi-TID Aggregation TX: 7
                                UL 2x996-Tone RU
                        HE PHY Capabilities: (0x0e3f0e00fd098c160ffc01):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                HE160/5GHz
                                Punctured Preamble RX: 15
                                Device Class: 1
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 7
                                Beamformee STS > 80Mhz: 7
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                Triggered SU Beamforming Feedback
                                Triggered MU Beamforming Feedback
                                PPE Threshold Present
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 2
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                20MHz in 40MHz HE PPDU 2.4GHz
                                20MHz in 160/80+80MHz HE PPDU
                                80MHz in 160/80+80MHz HE PPDU
                                TX 1024-QAM
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE RX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                HE Iftypes: AP
                        HE MAC Capabilities (0x78010a000000):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                OM Control
                                Maximum A-MPDU Length Exponent: 1
                        HE PHY Capabilities: (0x06200e000009800401c400):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                PPE Threshold Present
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                TX 1024-QAM
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                Bitrates (non-HT):
                        * 6.0 Mbps
                        * 9.0 Mbps
                        * 12.0 Mbps
                        * 18.0 Mbps
                        * 24.0 Mbps
                        * 36.0 Mbps
                        * 48.0 Mbps
                        * 54.0 Mbps
                Frequencies:
                        * 5180 MHz [36] (22.0 dBm)
                        * 5200 MHz [40] (22.0 dBm)
                        * 5220 MHz [44] (22.0 dBm)
                        * 5240 MHz [48] (22.0 dBm)
                        * 5260 MHz [52] (22.0 dBm)
                        * 5280 MHz [56] (22.0 dBm)
                        * 5300 MHz [60] (22.0 dBm)
                        * 5320 MHz [64] (22.0 dBm)
                        * 5340 MHz [68] (22.0 dBm)
                        * 5360 MHz [72] (22.0 dBm)
                        * 5380 MHz [76] (22.0 dBm)
                        * 5400 MHz [80] (22.0 dBm)
                        * 5420 MHz [84] (22.0 dBm)
                        * 5440 MHz [88] (22.0 dBm)
                        * 5460 MHz [92] (22.0 dBm)
                        * 5480 MHz [96] (22.0 dBm)
                        * 5500 MHz [100] (22.0 dBm)
                        * 5520 MHz [104] (22.0 dBm)
                        * 5540 MHz [108] (22.0 dBm)
                        * 5560 MHz [112] (22.0 dBm)
                        * 5580 MHz [116] (22.0 dBm)
                        * 5600 MHz [120] (22.0 dBm)
                        * 5620 MHz [124] (22.0 dBm)
                        * 5640 MHz [128] (22.0 dBm)
                        * 5660 MHz [132] (22.0 dBm)
                        * 5680 MHz [136] (22.0 dBm)
                        * 5700 MHz [140] (22.0 dBm)
                        * 5720 MHz [144] (22.0 dBm)
                        * 5745 MHz [149] (22.0 dBm)
                        * 5765 MHz [153] (22.0 dBm)
                        * 5785 MHz [157] (22.0 dBm)
                        * 5805 MHz [161] (22.0 dBm)
                        * 5825 MHz [165] (22.0 dBm)
                        * 5845 MHz [169] (22.0 dBm)
                        * 5865 MHz [173] (22.0 dBm)
                        * 5885 MHz [177] (22.0 dBm)
                        * 5905 MHz [181] (22.0 dBm)
        Band 4:
                HE Iftypes: Station
                        HE MAC Capabilities (0x78018a20abc0):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                32-bit BA Bitmap
                                OM Control
                                Maximum A-MPDU Length Exponent: 1
                                RX Control Frame to MultiBSS
                                A-MSDU in A-MPDU
                                Multi-TID Aggregation TX: 7
                                UL 2x996-Tone RU
                        HE PHY Capabilities: (0x0e3f0e00fd098c160ffc01):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                HE160/5GHz
                                Punctured Preamble RX: 15
                                Device Class: 1
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                SU Beamformee
                                Beamformee STS <= 80Mhz: 7
                                Beamformee STS > 80Mhz: 7
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                Triggered SU Beamforming Feedback
                                Triggered MU Beamforming Feedback
                                PPE Threshold Present
                                Power Boost Factor ar
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                Max NC: 2
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                20MHz in 40MHz HE PPDU 2.4GHz
                                20MHz in 160/80+80MHz HE PPDU
                                80MHz in 160/80+80MHz HE PPDU
                                TX 1024-QAM
                                RX 1024-QAM
                                RX Full BW SU Using HE MU PPDU with Compression SIGB
                                RX Full BW SU Using HE MU PPDU with Non-Compression SIGB
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE RX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set 160 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                HE Iftypes: AP
                        HE MAC Capabilities (0x78010a000000):
                                +HTC HE Supported
                                Trigger Frame MAC Padding Duration: 2
                                Multi-TID Aggregation Support: 7
                                OM Control
                                Maximum A-MPDU Length Exponent: 1
                        HE PHY Capabilities: (0x06200e000009800401c400):
                                HE40/2.4GHz
                                HE40/HE80/5GHz
                                LDPC Coding in Payload
                                NDP with 4x HE-LTF and 3.2us GI
                                STBC Tx <= 80MHz
                                STBC Rx <= 80MHz
                                Sounding Dimensions <= 80Mhz: 1
                                Sounding Dimensions > 80Mhz: 1
                                PPE Threshold Present
                                HE SU PPDU & HE PPDU 4x HE-LTF 0.8us GI
                                HE ER SU PPDU 4x HE-LTF 0.8us GI
                                TX 1024-QAM
                        HE RX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        HE TX MCS and NSS set <= 80 MHz
                                         1 streams: MCS 0-11
                                         2 streams: MCS 0-11
                                         3 streams: not supported
                                         4 streams: not supported
                                         5 streams: not supported
                                         6 streams: not supported
                                         7 streams: not supported
                                         8 streams: not supported
                        PPE Threshold 0x61 0x1c 0xc7 0x71
                Bitrates (non-HT):
                        * 6.0 Mbps
                        * 9.0 Mbps
                        * 12.0 Mbps
                        * 18.0 Mbps
                        * 24.0 Mbps
                        * 36.0 Mbps
                        * 48.0 Mbps
                        * 54.0 Mbps
                Frequencies:
                        * 5955 MHz [1] (22.0 dBm)
                        * 5975 MHz [5] (22.0 dBm)
                        * 5995 MHz [9] (22.0 dBm)
                        * 6015 MHz [13] (22.0 dBm)
                        * 6035 MHz [17] (22.0 dBm)
                        * 6055 MHz [21] (22.0 dBm)
                        * 6075 MHz [25] (22.0 dBm)
                        * 6095 MHz [29] (22.0 dBm)
                        * 6115 MHz [33] (22.0 dBm)
                        * 6135 MHz [37] (22.0 dBm)
                        * 6155 MHz [41] (22.0 dBm)
                        * 6175 MHz [45] (22.0 dBm)
                        * 6195 MHz [49] (22.0 dBm)
                        * 6215 MHz [53] (22.0 dBm)
                        * 6235 MHz [57] (22.0 dBm)
                        * 6255 MHz [61] (22.0 dBm)
                        * 6275 MHz [65] (22.0 dBm)
                        * 6295 MHz [69] (22.0 dBm)
                        * 6315 MHz [73] (22.0 dBm)
                        * 6335 MHz [77] (22.0 dBm)
                        * 6355 MHz [81] (22.0 dBm)
                        * 6375 MHz [85] (22.0 dBm)
                        * 6395 MHz [89] (22.0 dBm)
                        * 6415 MHz [93] (22.0 dBm)
                        * 6435 MHz [97] (22.0 dBm)
                        * 6455 MHz [101] (22.0 dBm)
                        * 6475 MHz [105] (22.0 dBm)
                        * 6495 MHz [109] (22.0 dBm)
                        * 6515 MHz [113] (22.0 dBm)
                        * 6535 MHz [117] (22.0 dBm)
                        * 6555 MHz [121] (22.0 dBm)
                        * 6575 MHz [125] (22.0 dBm)
                        * 6595 MHz [129] (22.0 dBm)
                        * 6615 MHz [133] (22.0 dBm)
                        * 6635 MHz [137] (22.0 dBm)
                        * 6655 MHz [141] (22.0 dBm)
                        * 6675 MHz [145] (22.0 dBm)
                        * 6695 MHz [149] (22.0 dBm)
                        * 6715 MHz [153] (22.0 dBm)
                        * 6735 MHz [157] (22.0 dBm)
                        * 6755 MHz [161] (22.0 dBm)
                        * 6775 MHz [165] (22.0 dBm)
                        * 6795 MHz [169] (22.0 dBm)
                        * 6815 MHz [173] (22.0 dBm)
                        * 6835 MHz [177] (22.0 dBm)
                        * 6855 MHz [181] (22.0 dBm)
                        * 6875 MHz [185] (22.0 dBm)
                        * 6895 MHz [189] (22.0 dBm)
                        * 6915 MHz [193] (22.0 dBm)
                        * 6935 MHz [197] (22.0 dBm)
                        * 6955 MHz [201] (22.0 dBm)
                        * 6975 MHz [205] (22.0 dBm)
                        * 6995 MHz [209] (22.0 dBm)
                        * 7015 MHz [213] (22.0 dBm)
                        * 7035 MHz [217] (22.0 dBm)
                        * 7055 MHz [221] (22.0 dBm)
                        * 7075 MHz [225] (22.0 dBm)
                        * 7095 MHz [229] (22.0 dBm)
                        * 7115 MHz [233] (22.0 dBm)
        Supported commands:
                 * new_interface
                 * set_interface
                 * new_key
                 * start_ap
                 * new_station
                 * new_mpath
                 * set_mesh_config
                 * set_bss
                 * authenticate
                 * associate
                 * deauthenticate
                 * disassociate
                 * join_ibss
                 * join_mesh
                 * remain_on_channel
                 * set_tx_bitrate_mask
                 * frame
                 * frame_wait_cancel
                 * set_wiphy_netns
                 * set_channel
                 * tdls_mgmt
                 * tdls_oper
                 * start_sched_scan
                 * probe_client
                 * set_noack_map
                 * register_beacons
                 * start_p2p_device
                 * set_mcast_rate
                 * connect
                 * disconnect
                 * channel_switch
                 * set_qos_map
                 * add_tx_ts
                 * set_multicast_to_unicast
        WoWLAN support:
                 * wake up on disconnect
                 * wake up on magic packet
                 * wake up on pattern match, up to 20 patterns of 16-128 bytes,
                   maximum packet offset 0 bytes
                 * can do GTK rekeying
                 * wake up on GTK rekey failure
                 * wake up on EAP identity request
                 * wake up on 4-way handshake
                 * wake up on rfkill release
                 * wake up on network detection, up to 8 match sets
        software interface modes (can always be added):
                 * AP/VLAN
                 * monitor
        valid interface combinations:
                 * #{ managed } <= 1, #{ AP, P2P-client, P2P-GO } <= 1, #{ P2P-device } <= 1,
                   total <= 3, #channels <= 2
        HT Capability overrides:
                 * MCS: ff ff ff ff ff ff ff ff ff ff
                 * maximum A-MSDU length
                 * supported channel width
                 * short GI for 40 MHz
                 * max A-MPDU length exponent
                 * min MPDU start spacing
        Device supports TX status socket option.
        Device supports HT-IBSS.
        Device supports SAE with AUTHENTICATE command
        Device supports low priority scan.
        Device supports scan flush.
        Device supports per-vif TX power setting
        P2P GO supports CT window setting
        P2P GO supports opportunistic powersave setting
        Driver supports full state transitions for AP/GO clients
        Driver supports a userspace MPM
        Driver/device bandwidth changes during BSS lifetime (AP/GO mode)
        Device adds DS IE to probe requests
        Device can update TPC Report IE
        Device supports static SMPS
        Device supports dynamic SMPS
        Device supports WMM-AC admission (TSPECs)
        Device supports configuring vdev MAC-addr on create.
        Device supports randomizing MAC-addr in scans.
        Device supports randomizing MAC-addr in sched scans.
        Device supports randomizing MAC-addr in net-detect scans.
        max # scan plans: 2
        max scan plan interval: 65535
        max scan plan iterations: 254
        Supported TX frame types:
                 * IBSS: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * managed: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * AP: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * AP/VLAN: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * mesh point: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * P2P-client: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * P2P-GO: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
                 * P2P-device: 0x00 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xe0 0xf0
        Supported RX frame types:
                 * IBSS: 0x40 0xb0 0xc0 0xd0
                 * managed: 0x40 0xb0 0xd0
                 * AP: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
                 * AP/VLAN: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
                 * mesh point: 0xb0 0xc0 0xd0
                 * P2P-client: 0x40 0xd0
                 * P2P-GO: 0x00 0x20 0x40 0xa0 0xb0 0xc0 0xd0
                 * P2P-device: 0x40 0xd0
        Supported extended features:
                * [ VHT_IBSS ]: VHT-IBSS
                * [ RRM ]: RRM
                * [ MU_MIMO_AIR_SNIFFER ]: MU-MIMO sniffer
                * [ SCAN_START_TIME ]: scan start timestamp
                * [ BSS_PARENT_TSF ]: BSS last beacon/probe TSF
                * [ FILS_STA ]: STA FILS (Fast Initial Link Setup)
                * [ CONTROL_PORT_OVER_NL80211 ]: control port over nl80211
                * [ TXQS ]: FQ-CoDel-enabled intermediate TXQs
                * [ ENABLE_FTM_RESPONDER ]: enable FTM (Fine Time Measurement) responder
                * [ CONTROL_PORT_NO_PREAUTH ]: disable pre-auth over nl80211 control port support
                * [ PROTECTED_TWT ]: protected Target Wake Time (TWT) support
                * [ DEL_IBSS_STA ]: deletion of IBSS station support
                * [ BEACON_PROTECTION_CLIENT ]: beacon prot. for clients support
                * [ SCAN_FREQ_KHZ ]: scan on kHz frequency support
                * [ CONTROL_PORT_OVER_NL80211_TX_STATUS ]: tx status for nl80211 control port support

```

上Phy1はhostapdの設定をしたところ、エラー。その後再度iw listするとdisableやno IRになり、利用はできないようになっていた。

### 1.6.2.2.共通設定

``` shell
#インタフェースを固定IPに設定しておく
nano /etc/network/interfaces
#USB-AP
auto wlx6c1ff7124a13
iface wlx6c1ff7124a13 inet static
        address 10.10.10.1/24

#ネットワークの再起動
systemctl restart networking.service

#ネットワークの確認 10.10.10.1を確認
ip a
8: wlx6c1ff7124a13: <NO-CARRIER,BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state DORMANT group default qlen 1000
    link/ether 5a:f4:bc:a9:5d:eb brd ff:ff:ff:ff:ff:ff permaddr 6c:1f:f7:12:4a:13
    inet 10.10.10.1/24 scope global wlx6c1ff7124a13

#hostapdのインストール
sudo apt-get install hostapd
#hostapdの設定する
nano /etc/hostapd/[インタフェース名].conf

```

### 1.6.2.3.Ugreen AX1800 USB 3.0の設定

``` shell
nano /etc/hostapd/wlx6c1ff7124a13.conf
---
#インタフェース名
interface=wlx6c1ff7124a13

#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MuRtlwifi
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=g
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=11
wpa=3 # WPA2
#認証パスワード
wpa_passphrase=aklak278354
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定(11acを使うにあたってこれは省略できない)
ieee80211n=1
#require_ht=1
#ht_capab=[MAX-AMSDU-3839][HT40+][SHORT-GI-20][SHORT-GI-40][DSSS_CCK-40]

# 11acの有効化
ieee80211ac=1
# 11axの有効化
#ieee80211ax=1
# [MAX-AMSDU-3839]:
# [SHORT-GI-80]: 80GHzショートGI
#vht_capab=[MAX-AMSDU-3839][SHORT-GI-80]

# ステーション側が11acをサポートしていなければ接続を拒否する
#require_vht=1
# チャネル幅
# 0: 20/40 MHz
# 1: 80 MHz
# 2: 160 MHz
# 3: 80+80 MHz
#vht_oper_chwidth=1
# 中心周波数
# 5Ghz+(5*index)
# channel+6を指定すればいいらしい
#vht_oper_centr_freq_seg0_idx=42
#CLI有効化
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0
---

```

### 1.6.2.4.Ugreen AX1800 USB 3.0の設定続き

``` shell
#上記の設定ファイルを無線インタフェース名に合わせて起動する
#フォーマットは以下の通り
systemctl start hostapd@無線インタフェース名.service

systemctl start hostapd@wlx6c1ff7124a13.service
```

### 1.6.2.5.共通設定の続き

``` shell
#IP フォワーディングの有効化
echo 1 > /proc/sys/net/ipv4/ip_forward
#IPマスカレード
#enp2s0=WAN側になります。
#以下のように無線WANのサブネット[10.10.10.0/24]をすべて有線LAN側[enp2s0]へIPマスカレードを行う
iptables -t nat -A POSTROUTING -s 10.10.10.0/255.255.255.0 -o enp2s0 -j MASQUERADE

#ip address add 10.0.0.1/24 dev wlx6c1ff7124a13

```

### 1.6.2.6.DHCPサーバーのインストール

```shell
#DHCPサーバーのインストール
apt install isc-dhcp-server
#DHCPサーバーの設定
mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf_org
nano /etc/dhcp/dhcpd.conf
---
default-lease-time 600;
max-lease-time 7200;
INTERFACES="wlp1s0";
option domain-name "";
max-lease-time 7200;
log-facility local7;

subnet 10.10.10.0 netmask 255.255.255.0 {
    range 10.10.10.10 10.10.10.254;
    option routers 10.10.10.1;
    option domain-name-servers 8.8.8.8;
}
---
#DHCPサーバーの対象となるインタフェース名を設定
nano /etc/default/isc-dhcp-server
---
INTERFACESv4="wlx6c1ff7124a13"
---

#DHCPサーバーの起動
systemctl start isc-dhcp-server.service

```

これでWIFIアダプターの設定が完了してインターネットへ接続できます。[ただしWAN側がインターネットへ接続できていることが条件ですが...]  

以下がアクセス情報なので、WIFI端末から接続して確認ください。  
SSID:MuRtlwifi
パスワード:aklak278354

また、トラブルシューティングとしては以下です。

``` shell
#下記のコマンドを上でUnit名:XXXXの部分を変更して実行していますが、エラー発生時はエラーメッセージに従ってください。エラー内容の確認ができます。  
systemctl start XXXX
#上で確認したエラー内容に応じた処置、切り分けをしてください。  
#具体的な内容はありませんので割愛します。  
#そのほかのエラーについては、エラーメッセージや問題の切り分け方法を考えて実行お願いします。 

#こちらでは以下を確認するとよいときもあります。
#WIFIアダプターの情報を確認したい.
iw list
```

## 1.6.2.6.動作確認を戻し、無線LANのみ自動起動する

``` shell
#インタフェースを#で子マントアウトする
nano /etc/network/interfaces
#iface wlx6c1ff7124a13 inet static
#        address 10.10.10.1/24

#DHCPサーバを停止して削除しておく。これはVMにあるOpenwrtのDHCPを利用するためです。
systemctl stop isc-dhcp-server.service
#DHCPサーバを自動起動OFF
systemctl disable isc-dhcp-server.service

#無線LANのhostapdを有効にして自動起動ONにする.
systemctl start hostapd@wlx6c1ff7124a13
systemctl enable hostapd@wlx6c1ff7124a13

```

## 1.6.3.OpenWrtのInstall

ここではOpenwrtをLXCへインストールします。

```shell
#OpenwrtのイメージをDL、ちなみにDL先は日ごとに変わるので要注意です。
wget -bc -o /var/lib/vz/template/cache/rootfs.tar.xz https://sgp1lxdmirror01.do.letsbuildthe.cloud/images/openwrt/23.05/amd64/default/20240814_11%3A57/rootfs.tar.xz
#LXCのVMを作成
pct create 501 /var/lib/vz/template/cache/rootfs.tar.xz --arch amd64 --hostname openwrt --rootfs local:4 --memory 512 --cores 1 --ostype unmanaged --unprivileged 1

```

Host側のProxmoxVEでは以下の設定をしています。  
| Host側のインタフェース | Host側の仮想インタフェース | Openwrt側のインタフェース |備考|
| --- | --- | --- | --- |
| vmbr0 | enp2s0:有線LAN1 1/2.5G | eth0 |WAN側|
| vmbr1 | enp5s0:有線LAN4 1/2.5G wlp1s0:無線AP1 2.4G wlx6c1ff7124a13:無線AP2 2.4G | eth1 |LAN側|
| vmbr2 | なし | eth2 |LAN側,Host側の仮想NIC|

無線APはそれぞれ、仕事用とそれ以外で分けています。  

Openwrtを起動してLANのネットワーク設定をします。  

``` shell
#確認
pct list
VMID       Status     Lock         Name
501        stopped                 openwrt

#VMを起動
pct start 501

#ログイン
pct enter 501

#ネットワーク設定
uci batch <<EOL
add network device
set network.@device[0].name='br-lan'
set network.@device[0].type='bridge'
set network.@device[0].ports='eth1'
set network.lan='interface'
set network.lan.device='br-lan'
set network.lan.proto='static'
set network.lan.ipaddr='192.168.11.1'
set network.lan.netmask='255.255.255.0'
EOL
uci commit
service network restart

#ルーティング確認 br-lanに192.168.11.1があるはず。。
ip a
#抜ける
exit

```

これでOpenwrtで無線AP2が利用できるようになっています。
また、HostOSもネットワーク接続できているのでこれで更新作業もできます。
ただし、インターネットへ接続するので、Firewallやウィルス対策ソフトなどの設定は必要そうです。

参考文献
ProxmoxにWiFiアクセスポイントを設定してWiFi経由でアクセスする  
<https://www.nofuture.tv/set-up-a-wifi-access-point-on-proxmox-and-access-via-wifi>  
ミニPC「HUNSN RJ03」へProxmoxとOpenWrtをインストール、最強ルーターを自作  
<https://wifi-manual.net/proxmox-with-openwrt-on-hunsn-rj03/#toc18>  

## 1.6.4.電源投入時のみnetwork設定が失敗する対策をする

おそらく機器固有の問題かもしれないが、電源投入時のみUSBのnetwork設定がほぼ失敗して、関連するLinuxブリッジの仮想NIC:vmbr1やvmbr0も引きずられて失敗し続ける。  

そのため以下のscriptでまとめて設定を実施した。

```shell
#自動化の方法
#1.Scriptを作成する
nano /etc/rc.local.latest
---
#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

###
# USB LANが結構な頻度で認識が遅れDHCP割り当てができず接続ができないので、起動の一番最後にUSB切断,接続をするように変更しました。
###
WIFILAN=wlx6c1ff7124a13
ip link show | grep $WIFILAN
# 直前のコマンドの終了ステータスを確認
if [ $? -ne 0 ]; then
   #$WIFILANが見つからない場合[USB LANが認識遅くなっている]
   # lsusb
   #Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
   #Bus 001 Device 004: ID 8087:0032 Intel Corp.
   #Bus 001 Device 005: ID 070a:0056 Oki Electric Industry Co., Ltd
   #Bus 001 Device 003: ID 0451:2036 Texas Instruments, Inc. TUSB2036 Hub
   #Bus 001 Device 002: ID 0bda:1a2b Realtek Semiconductor Corp. RTL8188GU 802.11n WLAN Adapter (Driver CDROM Mode)
   #Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
   #dmesg | grep 0bda | grep 1a2b
   #[    3.260958] usb 1-1: New USB device found, idVendor=0bda, idProduct=1a2b, bcdDevice= 0.00
   VenDerID="0bda"
   ProductID="1a2b"
   lsusb -d $VenDerID:$ProductID
   # 直前のコマンドの終了ステータスを確認
   if [ $? -eq 0 ]; then
      dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}'
      # 直前のコマンドの終了ステータスを確認
      if [ $? -eq 0 ]; then
         #Outputが以下なら、1-1の部分を抜き出す
         #[    3.260958] usb 1-1: New USB device found, idVendor=0bda, idProduct=1a2b, bcdDevice= 0.00
         BusID=`dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}' | sed 's/.$//'`
         echo "BusID=$BusID"
         echo $BusID | tee /sys/bus/usb/drivers/usb/unbind
         echo $BusID | tee /sys/bus/usb/drivers/usb/bind
      fi
   fi
   #ip link show | grep $WIFILAN
   # 直前のコマンドの終了ステータスを確認
   #if [ $? -ne 0 ]; then
   #   #$WIFILANが見つからない場合[USB LANが認識遅くなっている]
   #   # lsusb
   #   #Bus 001 Device 006: ID 0bda:b832 Realtek Semiconductor Corp. 802.11ac WLAN Adapter
   #   #Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
   #   #dmesg | grep 0bda | grep b832 | tail -1
   #   #[  780.446044] usb 2-1: New USB device found, idVendor=0bda, idProduct=b832, bcdDevice= 0.00
   #   VenDerID="0bda"
   #   ProductID="b832"
   #   lsusb -d $VenDerID:$ProductID
   #   # 直前のコマンドの終了ステータスを確認
   #   if [ $? -eq 0 ]; then
   #      dmesg | grep $VenDerID | grep $ProductID | tail -1 | awk '{print $4}'
   #      # 直前のコマンドの終了ステータスを確認
   #      if [ $? -eq 0 ]; then
   #         #Outputが以下なら、1-1の部分を抜き出す
   #         #[    3.260958] usb 1-1: New USB device found, idVendor=0bda, idProduct=1a2b, bcdDevice= 0.00
   #         BusID=`dmesg | grep $VenDerID | grep $ProductID | tail -1 | awk '{print $4}' | sed 's/.$//'`
   #         echo "BusID=$BusID"
   #         echo $BusID | tee /sys/bus/usb/drivers/usb/unbind
   #         echo $BusID | tee /sys/bus/usb/drivers/usb/bind
   #      fi
   #   fi
   #fi
fi
set -eE -o functrace

echo "Enable NetWorking Service Start!"
date
systemctl start networking.service
systemctl start hostapd@wlp1s0.service
sleep 20
systemctl start hostapd@wlx6c1ff7124a13.service

systemctl start pve-cluster.service
systemctl start pve-firewall.service
systemctl start pvestatd.service
systemctl start pve-ha-crm.service
systemctl start pve-ha-lrm.service
systemctl start pvescheduler.service

sleep 20

#OpenWrt Continar
systemctl stop pve-container@501.service
systemctl start pve-container@501.service

#電源投入時や電源ボタン押下時にのみ起動失敗になるので追加
systemctl stop systemd-udev-settle.service
systemctl start systemd-udev-settle.service

echo "Enable NetWorking Service End!"
date
### ここまで処理実行に必要なコマンドラインを記載する  ###

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

---
#scriptに実行権限つける
chmod +x /etc/rc.local.latest

#systemdのサービス登録する
nano /lib/systemd/system/rc-local-latest.service
---
[Unit]
#default.target（通常はマルチユーザーターゲット）が完了した後に
#このサービスが起動されることを意味します。つまり、他の必要なサービスが先に起動されることを保証します。
After=default.target
#networking.serviceより前に実行します。
#Before=networking.service
[Install]
#このサービスがdefault.targetに組み込まれることを意味します。
#つまり、通常のマルチユーザーターゲットでこのサービスが有効になります
WantedBy=default.target

[Service]
#このサービスがフォーク型のプロセスを起動することを意味します。
#フォーク型のプロセスはバックグラウンドで実行され、親プロセスとは独立しています。
Type=forking
#サービスを開始するために実行されるコマンドまたはスクリプトのパスを指定します。
#この場合、/etc/rc.local.latestが実行されます。
ExecStart=/etc/rc.local.latest
---

#以下の自動起動しているサービスを止める
systemctl disable networking.service
systemctl disable hostapd@wlp1s0.service
systemctl disable hostapd@wlx6c1ff7124a13.service

#systemdのサービス登録を有効化する
systemctl --system enable rc-local-latest

#再起動後有効化しているか確認する
reboot
#ログを見る
systemctl status rc-local-latest
#上より詳細は以下でrc-local-latest.serviceのログのみ確認
journalctl -u rc-local-latest.service
#前回[1回前]のブートにしぼってジャーナルログ全部を確認
journalctl -b -1
#前回[1回前]のブートにしぼってrc-local-latest.serviceのログのみ確認
journalctl -b -1 -u rc-local-latest.service
#今回のログをみたいのであれば-b 0とすればよいです
```

## 1.6.5.HostOS側のセキュリティ設定Iptables

Iptablesによるファイヤーウォール、パケットフィルターを利用することで不要な通信を遮断することができます。  

ポイントは以下
・LAN側のみssh[Port:22],web[Port:8006]を許可する
・ネットワーク上、Openwrtと仮想NICを接続しておく
・NTT Routerとweb接続をLANから許可すること
・aptなどのパッケージマネージャからUpdateを許可すること
・その他の不要な通信は遮断すること

## 1.6.6.HostOS側のセキュリティ設定Clamav

ClamavはOpenSourceのウィルス対策ソフトです。  

ポイントは以下
・定時フルスキャン
・範囲は全ディレクトリが望ましいが、出来ない場合もあるので調査して考えておく
・カスタムしたClamAVのため、ClamAVのサーバ/クライアントのbuild環境および、適用方法を自動化しておきたい。
・オンアクセススキャンを適用する
・結果を個別ログファイルへログとして残す
・ログローテートを過去5回分まで残すようにする
・ClamAVの定義ファイルのプライベートミラーサーバにサーバ側はする

### 1.6.6.1.ClamavのBuild
### 1.6.6.2.ClamavのInstall
#### 1.6.6.2.1.ClamavのServer
#### 1.6.6.2.2.ClamavのClient

## 1.6.7.セキュリティマネージメント Wazuh

WazuhとはXDRと呼ばれる攻撃アプリケーションのセキュリティ対策ソフトです。
ウィルス対策ソフトやEDR、NDRと異なり、XDRであるWazuhを利用することにより、包括的なセキュリティ対策を実現することができます。

これは様々なプラットフォーム上で利用できる対策ソフトです。

ただ、サーバとクライアントの両方が必要です。

X86ルータ上にVMとしてサーバを設置してOpenwrtのネットワーク上にWazuhをインストールすると、セキュリティ対策を実現することができます。
OpenwrtにはWazuhのパッケージはありませんので独自に構築する必要があります。

WazuhはOpenSourceのウィルス対策ソフトですので、費用はかかりません。
また、セキュリティのインシデントにも利用できます。

### 1.6.7.1.サーバ側の構築手順

VMで以下の手順に従いインストールする
Debian 12 と Proxmox で Wazuh を SIEM として設定する: ステップバイステップ ガイド
<https://maikroservice.com/setting-up-wazuh-as-your-siem-on-debian-12-proxmox-a-step-by-step-guide>

Wazuhをインストールする
以下を参照
<https://documentation.wazuh.com/current/quickstart.html#installing-wazuh>

``` shell
# first we become root so that we can install packages
su -
# next install curl
apt-get install curl
# and install wazuh
curl -sO https://packages.wazuh.com/4.9/wazuh-install.sh && sudo bash ./wazuh-install.sh -a

13/09/2024 22:17:33 INFO: You can access the web interface https://<wazuh-dashboard-ip>:443
    User: admin
    Password: vSdl?SDVS9eF81UiHBnTJFhSMLyZHwol
```
アクセス方法は以下上のユーザとパスワードで確認
<https://192.168.11.101:443>

これでアクセスできればインストールはOKです。

最後にシステム時刻を日本時間へ変更します。

``` shell
#現在のタイムゾーンを確認:
timedatectl
#このコマンドで現在のタイムゾーンが表示されます。
#タイムゾーンを日本時間に変更:
sudo timedatectl set-timezone Asia/Tokyo

#変更を確認:
timedatectl
```

### 1.6.7.2.HostOS側のセキュリティ設定Wazuh Agent

Wazuh AgentはIDS/IPSであり、侵入検知・防止に役立ソフトです。  

ポイントは以下
・適切な設定と方法の調査

#### 1.6.7.2.1.ProxmoxVE上にWazuh Agentをインストールする

<https://maikroservice.com/setting-up-wazuh-as-your-siem-on-debian-12-proxmox-a-step-by-step-guide>
↓
<https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html>

WAZUH_MANAGER="192.168.11.101" apt-get install wazuh-agent

#### 1.6.7.2.2.OpenwrtのRouter:Turris上でIPSのSuricataとWazuh Agentをインストールする

<https://deadbadger.cz/blog/wazuh-and-suricata-on-turris>

だめだった。sricataがinstallできず。
wazuh-agentのみインストールする
以下参考
<https://documentation.wazuh.com/current/deployment-options/wazuh-from-sources/wazuh-agent/index.html>

``` shell
opkg install perl perlbase-findbin perlbase-pod perlbase-storable perlbase-feature libpthread librt coreutils-install
ar -rc /usr/lib/libdl.a
ar -rc /usr/lib/libl.a
ar -rc /usr/lib/libpthread.a
ar -rc /usr/lib/librt.a
opkg install curl
curl -Ls https://github.com/wazuh/wazuh/archive/v4.9.0.tar.gz | tar zx

wget https://www.openssl.org/source/openssl-1.1.1u.tar.gz
 mv c255b370-1659-40ef-b1b2-c3c5ccc478ee\?X-Amz-Algorithm=AWS4-HMAC-SHA256 openssl-1.1.1u.tar.gz
tar -zxvf openssl-1.1.1u.tar.gz
cd openssl-1.1.1u
./config
make
make install

wget https://cmake.org/files/v3.21/cmake-3.21.0.tar.gz
tar -zxvf cmake-3.21.0.tar.gz
cd cmake-3.21.0
./bootstrap && make && make install

cd wazuh-4.9.0
./install.sh

#途中以下のエラーがあった
CMake Error at CMakeLists.txt:229 (add_subdirectory):
  The source directory

    /root/wazuh-4.9.0/src/shared_modules/http-request

  does not contain a CMakeLists.txt file.
#原因は以下のディレクトリが空の状態です。
/root/wazuh-4.9.0/src/shared_modules/http-request
#上はどうやら下記のアーカイブファイルが解凍できていないようなので手動で解凍します。
/root/wazuh-4.9.0/src/shared_modules/http-request.tar

cd /root/wazuh-4.9.0/src/shared_modules/
tar xvf http-request.tar
rm http-request
mv wazuh-wazuh-http-request-615317a http-request

usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `mkstemp64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `ftello64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `fstat64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `freopen64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `glob_pattern_p'
/usr/bin/ld: ../lib/libsysinfo.so: undefined reference to `backtrace_symbols'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `gnu_dev_major'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `gnu_dev_minor'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `stat64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `pwrite64'
/usr/bin/ld: ../lib/libsysinfo.so: undefined reference to `backtrace'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `open64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `getcontext'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `mmap64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `ftruncate64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `fseeko64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `fopen64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `makecontext'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `setcontext'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `__strdup'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `__rawmemchr'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `tmpfile64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `lstat64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `lseek64'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `pread64'
/usr/bin/ld: ../lib/libsysinfo.so: undefined reference to `srandom_r'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `gnu_dev_makedev'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazuhext.so: undefined reference to `__strtok_r'
/usr/bin/ld: ../lib/libsysinfo.so: undefined reference to `random_r'
/usr/bin/ld: ../lib/libsysinfo.so: undefined reference to `initstate_r'
/usr/bin/ld: /root/wazuh-4.9.0/src/libwazu

解決不
4.9.0->4.1.5にしてもinstallはできたが、agent起動がシンボル解決できずったのでやらない

代替案として、sshを用いた監視に切り替える

```

#### 1.6.7.2.3.OpenwrtでsnortのIPS機能を有効化する



## 1.6.8.ClamAvでGoogle Safe Browsing を利用する

APIキーを利用できることは確認したが、メモリ使い過ぎ[12GB]なのでやらない

１．APIキー有効化方法

「Google API key を取得する」をチェック、しかし、これだけでは「Safe Browsing API 」がライブラリとして無効化されているのでもうひと手間必要です。  
①
<https://www.fumibu.com/clamav-safebrowsing-with-google-api/#toc3>
下記の「Google Safe Browsing API キーを生成する方法」の「6.メニュー パネルで [ライブラリ] をクリックします。」を見て有効化してください。
②
<https://kb.synology.com/ja-jp/SRM/tutorial/How_to_generate_Google_Safe_Browsing_API_keys>

しかし、①に「clamav-Safebrowsing を起動するには、少なくとも１２GBのメモリー（RAM）が必要です。」と記載があるのでリソースがかなり使用するようだ。  

ウィルス対策ソフトのブラウザ機能である程度防げるのでRouterとしては過剰防御の可能性がある。  

## 1.6.9 HiDS/EDRのWazuhを利用する

サーバクライアントモデルのWazuhを導入して、ファイル改ざん侵入検知、インシデントまでを一括管理することができます。  

これを利用すると、リアルタイム検知はWazuhに任せて、ClamAVは定時スキャンにしてリソースを少なくできる。  

## 1.6.1.OpenWrtののWebUIを外から見えるようにする

この目的はLANではなくWAN側からへアクセスしたいためです。

LAN側へ接続すれば問題なく有効になります。
