#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#このscriptはrootユーザで行ってください。
#また、OSはDebian12にまずは限定しています。

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#意味
#関数:ShCmdLogは標準出力、標準エラー出力を年月日_時分秒_ミリ秒_CmdHist.logファイルへ書き込みをするための設定を行います。
ShCmdLog() {
  local filename=$(date +'%Y%m%d_%H%M%S_%3N')_CmdHist.log
  exec > ${filename} 2> ${filename}
}

ShCmdLog

sudo apt update && sudo apt upgrade
#以下は既にインストールしている「linux-headers-$(uname -r)」があるためNG
sudo apt install -y pve-headers build-essential bc dkms git libelf-dev rfkill iw
mkdir -p ~/src
cd ~/src
git clone https://github.com/morrownr/rtl8852bu.git
cd ~/src/rtl8852bu
#下記のOSをbuildしたときのgccと現在のgccは一致するので問題ない
cat /proc/version
gcc --version
#installする
./install-driver.sh NoPrompt

sudo apt-get install -y hostapd

cat > /etc/hostapd/wlx6c1ff7124a13.conf << EOL
#インタフェース名
interface=wlx6c1ff7124a13

bridge=vmbr1

#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MuRtlwifi
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=g
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=11
wpa=2 # WPA2
#認証パスワード
wpa_passphrase=aklak278354
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定
ieee80211n=1

#CLI有効化
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0

EOL

systemctl start hostapd@wlx6c1ff7124a13

cat > /etc/hostapd/wlp1s0.conf << EOL
#インタフェース名
interface=wlp1s0

bridge=vmbr1

#無線LANアダプタのドライバ
driver=nl80211
#クライアントに表示される無線LANアクセス・ポイント（SSID）名
ssid=MuRtlwifi_2
#国コード
country_code=JP
# 動作モード (a = IEEE 802.11a (5 GHz), b = IEEE 802.11b (2.4 GHz),
# g = IEEE 802.11g (2.4 GHz), ad = IEEE 802.11ad (60 GHz))
# IEEE 802.11n (HT)はa/gオプションを使用
# IEEE 802.11ac (VHT)はaオプションを使用
# IEEE 802.11ax (HE)の6 GHzはaオプションを使用
#デバイス上,11acまでなのでaを指定
hw_mode=g
# 使用するチャネル番号
# 2.4GHzでは1-13
# 5GHzではW52(36-48), W53(52-64), W56(100-104)
# W53とW56はDFSとTPCのサポートが必要
# ラズパイではW52しかサポートしていないので36か44だけが選択可能
# 帯域80MHzを使う場合は36しか選べない
channel=13
wpa=2 # WPA2
#認証パスワード
wpa_passphrase=aklak278354
wpa_key_mgmt=WPA-PSK
# RSN/WPA2用サイファースイート
# CCMP: AES with 128ビットCBC-MAC
# TKIP: TKIP
# CCMP-256: AES with 256ビットCBC-MAC
rsn_pairwise=CCMP

## 11nの設定
ieee80211n=1
EOL

systemctl start hostapd@wlp1s0

#OpenwrtのイメージをDL、ちなみにDL先は日ごとに変わるので要注意です。
TYearManth=$(date +'%Y%m')
TDay=$(date +'%d')
DATETIME=${TYearManth}${TDay}

# 指定した時刻（11:57）をUNIX時間で表現
specified_time=$(date --date "11:57" "+%s")
# 現在の時刻をUNIX時間で表現
current_time=$(date "+%s")
if [ "$current_time" -gt "$specified_time" ]; then
    echo "現在の時刻は11時57分を過ぎています。"
    #今日のrootfs.tar.gzを利用できるのでそのまま
else
    echo "現在の時刻は11時57分を過ぎていません。"
    #前日の11:57のrootfs.tar.gzを利用できるようにパス変更
    TDay_tmp=$((TDay - 1))
    DATETIME=${TYearManth}${TDay_tmp}
fi

wget https://images.linuxcontainers.org/images/openwrt/23.05/amd64/default/${DATETIME}_11%3A57/rootfs.tar.xz
#LXCのVMを作成
pct create 501 rootfs.tar.xz --arch amd64 --hostname openwrt --rootfs local:4 --memory 512 --cores 1 --ostype unmanaged --unprivileged 1

##ここでeth0[WAN]と1[LAN]を設定するが、ProxmoxVEのGUIで設定している。自動化したいのでコマンドラインでできないか模索中

#VMを起動
pct start 501

#ログイン
pct enter 501

uci batch <<EOL
add network device
set network.@device[0].name='br-lan'
set network.@device[0].type='bridge'
set network.@device[0].ports='eth1'
set network.lan='interface'
set network.lan.device='br-lan'
set network.lan.proto='static'
set network.lan.ipaddr='192.168.11.1'
set network.lan.netmask='255.255.255.0'
EOL

uci commit

service network restart

#ログアウト
exit

#""中の$などの特殊文字がだめなのでnanoで記載すること。回避策は模索中...

cat > /etc/rc.local.latest << EOL
#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###
#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

###
# USB LANが結構な頻度で認識が遅れDHCP割り当てができず接続ができないので、起動の一番最後にUSB切断,接続をするように変更しました。
###
WIFILAN=wlx6c1ff7124a13
ip link show | grep $WIFILAN
# 直前のコマンドの終了ステータスを確認
if [ $? -ne 0 ]; then
   #$WIFILANが見つからない場合[USB LANが認識遅くなっている]
   # lsusb
   #Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
   #Bus 001 Device 004: ID 8087:0032 Intel Corp.
   #Bus 001 Device 005: ID 070a:0056 Oki Electric Industry Co., Ltd
   #Bus 001 Device 003: ID 0451:2036 Texas Instruments, Inc. TUSB2036 Hub
   #Bus 001 Device 002: ID 0bda:1a2b Realtek Semiconductor Corp. RTL8188GU 802.11n WLAN Adapter (Driver CDROM Mode)
   #Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
   #dmesg | grep 0bda | grep 1a2b
   #[    3.260958] usb 1-1: New USB device found, idVendor=0bda, idProduct=1a2b, bcdDevice= 0.00
   VenDerID="0bda"
   ProductID="1a2b"
   lsusb -d $VenDerID:$ProductID
   # 直前のコマンドの終了ステータスを確認
   if [ $? -eq 0 ]; then
      dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}'
      # 直前のコマンドの終了ステータスを確認
      if [ $? -eq 0 ]; then
         #Outputが以下なら、1-1の部分を抜き出す
         #[    3.260958] usb 1-1: New USB device found, idVendor=0bda, idProduct=1a2b, bcdDevice= 0.00
         BusID=`dmesg | grep $VenDerID | grep $ProductID | awk '{print $4}' | sed 's/.$//'`
         echo "BusID=$BusID"
         echo $BusID | tee /sys/bus/usb/drivers/usb/unbind
         echo $BusID | tee /sys/bus/usb/drivers/usb/bind
      fi
   fi
fi
set -eE -o functrace

echo "Enable NetWorking Service Start!"
date
systemctl start networking.service
systemctl start hostapd@wlp1s0.service
sleep 20
systemctl start hostapd@wlx6c1ff7124a13.service

systemctl start pve-cluster.service
systemctl start pve-firewall.service
systemctl start pvestatd.service
systemctl start pve-ha-crm.service
systemctl start pve-ha-lrm.service
systemctl start pvescheduler.service

sleep 20

#OpenWrt Continar
systemctl stop pve-container@501.service
systemctl start pve-container@501.service

#電源投入時や電源ボタン押下時にのみ起動失敗になるので追加
systemctl stop systemd-udev-settle.service
systemctl start systemd-udev-settle.service

echo "Enable NetWorking Service End!"
date
### ここまで処理実行に必要なコマンドラインを記載する  ###

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

EOL

chmod +x /etc/rc.local.latest

cat > /lib/systemd/system/rc-local-latest.service << EOL
[Unit]
#default.target（通常はマルチユーザーターゲット）が完了した後に
#このサービスが起動されることを意味します。つまり、他の必要なサービスが先に起動されることを保証します。
After=default.target
#networking.serviceより前に実行します。
#Before=networking.service
[Install]
#このサービスがdefault.targetに組み込まれることを意味します。
#つまり、通常のマルチユーザーターゲットでこのサービスが有効になります
WantedBy=default.target

[Service]
#このサービスがフォーク型のプロセスを起動することを意味します。
#フォーク型のプロセスはバックグラウンドで実行され、親プロセスとは独立しています。
Type=forking
#サービスを開始するために実行されるコマンドまたはスクリプトのパスを指定します。
#この場合、/etc/rc.local.latestが実行されます。
ExecStart=/etc/rc.local.latest
EOL

systemctl disable networking.service
systemctl disable hostapd@wlp1s0.service
systemctl disable hostapd@wlx6c1ff7124a13.service
#systemdのサービス登録を有効化する
systemctl --system enable rc-local-latest

### ここまで処理実行に必要なコマンドラインを記載する  ###

#再起動後有効化しているか確認する
reboot

#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

