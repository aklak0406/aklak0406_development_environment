#!/bin/bash
#意味
#script実行時に/bin/bashを利用する

#このscriptはrootユーザで行ってください。
#また、OSはDebian12にまずは限定しています。

#意味
# set はshellのオプションを設定/確認できる。今回は設定の方。
# -e はエラー発生時終了する。
# -E はscriptでエラー発生時に行番号を表示する。
# -o functraceはシェルスクリプト内でエラーが発生した際に関数呼び出しのトレースを有効にするオプション
set -eE -o functrace

#意味
#関数:failureはエラー発生時にfilename:ファイル名,lineno:行番号,msg:エラー発生時に実行したコマンドラインを出力します。
#例. [Failed at ./002_BrcmWiFiSetting.sh is 18: cat ./test2]だと、ファイル名:./002_BrcmWiFiSetting.sh,行番号:18,コマンドライン:cat ./test2となる。
#また、↑のメッセージは「Failed at $filename is $lineno: $msg」の出力構文となる。
#↑の最終行の前にエラーメッセージの前にコマンド実行後のエラーメッセージ本体が出力されるため、コマンドラインの失敗原因を確認できる。
failure() {
  local filename=$0
  local lineno=$1
  local msg=$2
  echo "Failed at Shell's $filename is $lineno: $msg"
}

#意味
#trapにより、ERR[shellscript内でコマンドラインのエラー発生時,コマンドラインの返り値が0出ない場合]の場合、関数:failureを実行する
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

### ここから処理実行に必要なコマンドラインを記載する  ###

#意味
#関数:ShCmdLogは標準出力、標準エラー出力を年月日_時分秒_ミリ秒_CmdHist.logファイルへ書き込みをするための設定を行います。
ShCmdLog() {
  local filename=$(date +'%Y%m%d_%H%M%S_%3N')_CmdHist.log
  exec > ${filename} 2> ${filename}
}

ShCmdLog

#ポート開放しているものは最低限,22[ssh,制御のコンソール操作用途],80/443[http/https、主にaptやwgetなどのパッケージインストール用途],またこのポートあてのhttpのDOS攻撃用のルール記載。123[ntp],53[DNS、これもaptやwgetなど]とした。
      iptables -F && \
      iptables -X && \
      iptables -P INPUT DROP                                                                                                                                                                                                                  && \
      iptables -P FORWARD DROP                                                                                                                                                                                                                && \
      iptables -P OUTPUT ACCEPT                                                                                                                                                                                                               && \
      iptables -A INPUT -i lo -j ACCEPT                                                                                                                                                                                                       && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "DropInPktDataNotFound:"                                                                                                                   && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP                                                                                                                                                        && \
      iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "DropInPktSYNflood:"                                                                                                         && \
      iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j DROP                                                                                                                                          && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "DropInPktStealthScan:"                                                                                                 && \
      iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j DROP                                                                                                                                     && \
      iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m hashlimit --hashlimit-upto 1/min --hashlimit-burst 10 --hashlimit-mode srcip --hashlimit-name t_icmp --hashlimit-htable-expire 120000 -j LOG --log-prefix "DropPingOfDeathAttack:"   && \
      iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m hashlimit --hashlimit-upto 1/min --hashlimit-burst 10 --hashlimit-mode srcip --hashlimit-name t_icmp --hashlimit-htable-expire 120000 -j ACCEPT                                      && \
      iptables -A INPUT -f -j LOG --log-prefix 'fragment_packet:'                                                                                                                                                                             && \
      iptables -A INPUT -f -j DROP                                                                                                                                                                                                            && \
      iptables -N HTTP_DOS                                                                                                                                                                                                                    && \
      iptables -A HTTP_DOS -p tcp -m multiport --dports 80,443 -m hashlimit --hashlimit 1/s --hashlimit-burst 100 --hashlimit-htable-expire 300000 --hashlimit-mode srcip --hashlimit-name t_HTTP_DOS -j RETURN                               && \
      iptables -A HTTP_DOS -j LOG --log-prefix "http_dos_attack: "                                                                                                                                                                            && \
      iptables -A HTTP_DOS -j DROP                                                                                                                                                                                                            && \
      iptables -A INPUT -p tcp -m multiport --dports 80,443 -j HTTP_DOS                                                                                                                                                                       && \
      iptables -A INPUT -p tcp -m multiport --dports 113 -j REJECT --reject-with tcp-reset                                                                                                                                                    && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --set                                                                                                                                               && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --rcheck --seconds 60 --hitcount 5 -j LOG --log-prefix "ssh_brute_force: "                                                                          && \
      iptables -A INPUT -p tcp --syn -m multiport --dports 22 -m recent --name ssh_attack --rcheck --seconds 60 --hitcount 5 -j REJECT --reject-with tcp-reset                                                                                && \
      iptables -A INPUT -d 224.0.0.1       -j LOG --log-prefix "drop_broadcast: "                                                                                                                                                             && \
      iptables -A INPUT -d 224.0.0.1       -j DROP                                                                                                                                                                                            && \
      iptables -A INPUT -p udp -m multiport --sports 53,123 -m udp -j ACCEPT                                                                                                                                                                  && \
      iptables -A INPUT -p tcp -m multiport --dports 22 -m tcp -j ACCEPT                                                                                                                                                             && \
      iptables -A INPUT -p tcp -m multiport --sports 80,443 -m tcp -j ACCEPT

#設定したiptablesの設定を永続化する。
apt install iptables-persistent
#下記のコマンドで現在のiptablesの設定を「/etc/iptables/rules.v4」へ反映する。
iptables-save > /etc/iptables/rules.v4

### ここまで処理実行に必要なコマンドラインを記載する  ###


#「set -eE -o functrace」で有効にしたオプションを無効にする
set +eE +o functrace

