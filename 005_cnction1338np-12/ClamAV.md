# 1. 起動時におけるClamAVの簡易Scanを自動化

- [ ] [README.mdへ戻る](./../README.md)

## 1.1. 目次

- [1. 起動時におけるClamAVの簡易Scanを自動化](#1-起動時におけるclamavの簡易scanを自動化)
  - [1.1. 目次](#11-目次)
  - [1.2. 概要説明](#12-概要説明)
  - [1.3. 方法](#13-方法)
  - [1.3.1 systemdによるscan設定](#131-systemdによるscan設定)
  - [1.3.2 systemdによる通知設定](#132-systemdによる通知設定)

## 1.2. 概要説明

　目的はマルチプラットフォームに対応したウィルス対策ソフトClamAVにおいて起動時にウィルススキャンを自動化することである。今回はLiunxが対象である。
目的の理由はClamAVはデフォルトでも手動、自動、ファイルシステムの通知からのオンラインScanに対応しているが、全範囲を包含するようなScanはいずれも動作が重くなることが課題であるので、私は独自のタイミングとで簡易的なScanを起動時に行い、時刻による自動Scanはデフォルトの設定で対応することで動作を妨げずにある程度のウィルス対策もできるようにしたい。
この方法がベストではなくあくまでベターではあるが今後の改良含めて第一歩としたい。

## 1.3. 方法

　以下のいくつか方法を試したところ、最終的には「通知」はユーザ権限、「Scan」は管理者権限で行う。
・cronで再起動時に300秒後に1回のみscriptでscanと通知を実施する
結果:scanは管理者権限でOK。通知は管理者権限[root権限でcrontab -eでscriptを追加]でユーザ権限でのデスクトップ通知は失敗。
どうやらユーザ権限でないと通知がD-Busの受け取れないような動作をしている。
切り分けはrootでのcronとユーザでのcronで通知の有無が異なり、ユーザ権限でのみ通知がくるようだ。
・systemdのtimerで5分[300秒]後にserviceでscriptを実行して通知を行う
結果:scanは管理者権限でOK。通知は管理者権限[root権限のdunstifyでscriptを追加]でユーザ権限でのデスクトップ通知は失敗。
どうやらユーザ権限でないと通知がD-Busの受け取れないような動作をしている。
切り分けはrootでのcronとユーザでのcronで通知の有無が異なり、ユーザ権限でのみ通知がくるようだ。

↑からの課題として、scanはrootユーザで行わないとアクセスできないファイルが多いのでrootで行い、通知はユーザ権限で行わないと正しくデスクトップ上へ通知を上げることが困難である。
やりたいことをするためにはscan→通知をする必要があるのでscriptを2つに分け、順序性を確保する必要がある。
ここでcronの場合はユーザとrootの2つのcrontabが存在するので可能。しかし、順序が確保するためには時間をsleepなどで合わせる必要があり、scanが完了する正しい時間を考慮する必要がある。sleepによる順序担保は設定直後はOKだが、時間経過とともにファイル内容が変わることでsleep時間の微調整が必要になるので恒久策としては非常に難しいと考えた。

systemdの場合はunitファイル[設定ファイル]にAfter=自身とは別のunitファイル名を記載することで、例えばあるserviceのあとに自身のserviceを実行することが可能となる。

またchatgptによるとあるserviceのscriptが実行されたあとに自身のserviceが実行されるとあるため、検証は必要だが、順序は確保されていると判断している。

全体イメージ[あとで図にしたい...]

Scan
ファイルのScan<--ClamD<--Clamコマンドライン<--shell script
<--service用のsystemdのunitファイル<--timer用のsystemdのunitファイル
<--systemdからの起動トリガー[timer指定でシステム起動から5分とunitファイルの記載通りに実行]

通知[shell scriptで条件発動時]
Linuxのデスクトップ上の通知<--dunstifyコマンド<--shell script
<--ユーザ権限で実行する通知用のunitファイル<--systemd<--Scanのshell scriptの実行が終了した場合

通知[shell scriptで条件発動なし時]
shell script<--ユーザ権限で実行する通知用のunitファイル<--systemd
<--Scanのshell scriptの実行が終了した場合

## 1.3.1 systemdによるscan設定

以下のコマンドを実行して環境構築する

``` sh
#rootになる
sudo -i
#rootのカレントディレクトリへ移動
cd /root
#script格納用のディレクトリ作成
mkdir -p script
#scriptの作成
cat << EOF >> ClamavScan.sh
#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

#systemctl stop clamav-freshclam: clamav-freshclamサービスを停止します。
systemctl stop clamav-freshclam
#freshclam: ウイルス定義データベースを更新します。
freshclam
#systemctl start clamav-freshclam: clamav-freshclamサービスを再起動します。
systemctl start clamav-freshclam

filename=\`date "+%Y_%m_%d_%H_%M_%S"\`_clamScan.log
FindstdOutErrFilename=\`date "+%Y_%m_%d_%H_%M_%S"\`_Findstdout_and_stderr.log
stdOutErrFilename=\`date "+%Y_%m_%d_%H_%M_%S"\`_stdout_and_stderr.log

SCAN_DIR=/var/log/clamav/schedule_scan_result 
# スキャン結果の一時ファイル
SCAN_RESULT_FILE=\$SCAN_DIR/\$filename

# 過去1ヶ月分のファイルのみを保持
find "\$SCAN_DIR" -type f -mtime +30 -exec rm -f {} \;

#find / -type f -not \\
#\( -path '/sys/*' -o -path '/proc/*' -o -path '/dev/*' \) -print0 2> /var/log/#clamav/schedule_scan_result/\$FindstdOutErrFilename \\
#| xargs -0 -P $(nproc) \\
#clamdscan --log=\$SCAN_RESULT_FILE >& /var/log/clamav/schedule_scan_result/#\$stdOutErrFilename

find / -type f ! \\
\( \\
-path '/proc/*' -o \\
-path '/sys/*'  -o \\
-path '/dev/*'  -o \\
-path '/snap/*' -o \\
-path '/opt/google/chrome/*' -o \\
-path '/usr/sbin/*' -o \\
-path '/var/lib/dpkg/*' -o \\
-path '/var/*' -o \\
-path '/tmp/*' -o \\
-path '/home/reki/.vscode/extensions/*' -o \\
-path '/home/reki/.vscode/argv.json' -o \\
-path '/home/reki/.gnupg/*' -o \\
-path '/home/reki/.bashrc' -o \\
-path '/dbus-1/services/org.freedesktop.Notifications.service' -o \\
-path '/etc/*'    \\
\) -print0 2> /var/log/clamav/schedule_scan_result/\$FindstdOutErrFilename \\
| xargs -0 -L 1 -P 4 \\
clamdscan >& \$SCAN_RESULT_FILE


#一般ユーザがログをチェックできるように一般ユーザの沢れる場所へコピーと権限つけておく
cp \$SCAN_RESULT_FILE /home/reki/script/
chmod 777 /home/reki/script/\$filename
chown reki:reki /home/reki/script/\$filename

EOF
#実行権限を付与
chmod +x ClamavScan.sh

#systemdのunitファイルの作成
#内容:scriptを起動させるservice用のunitファイルの作成
cat << EOF >> /etc/systemd/system/admin-notify.service
[Unit]
Description=Send Admin's Notification using a script

[Service]
Type=oneshot
ExecStart=/root/script/ClamavScan.sh
User=root

[Install]
WantedBy=multi-user.target
EOF
#内容:scriptのservice用を起動させるtimer用のunitファイルの作成
#ここではOnBootSec=5minとして起動後5分したらadmin-notify.serviceを実行する意味になります。
cat << EOF >> /etc/systemd/system/admin-notify.timer
[Unit]
Description=Run admin-notify.service 5 minutes after boot

[Timer]
OnBootSec=5min
Persistent=false

[Install]
WantedBy=timers.target
EOF

#systemdに新しいUnitファイルを認識させるために再読込[reload]させる
sudo systemctl daemon-reload
#サービス有効化
sudo systemctl enable admin-notify.timer
sudo systemctl status admin-notify.timer
sudo systemctl start admin-notify.timer
sudo systemctl status admin-notify.timer
#一般ユーザに戻る
exit
```

## 1.3.2 systemdによる通知設定

``` sh
USER_NAME=reki
#一般ユーザ:rekiのカレントディレクトリへ移動
cd /home/$USER_NAME
#script格納用のディレクトリ作成
mkdir -p script
cd script
#scriptの作成
cat << EOF >> notifyDesktop.sh
#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# スキャン結果が保存されているディレクトリ
SCAN_DIR=/home/reki/script
# 最も新しいファイルを検出
latest_file=\$(ls -t "\$SCAN_DIR"/*.log | head -n 1)
# ファイルの完全なパスを生成
SCAN_RESULT_FILE="\$latest_file"

# 過去1ヶ月分のファイルのみを保持
find "\$SCAN_DIR" -type f -mtime +30 -exec rm -f {} \;


# 環境変数を設定
export DISPLAY=:0
# Waylandセッション情報を取得
export WAYLAND_DISPLAY=$(echo \$WAYLAND_DISPLAY)
# DBusセッションアドレスを設定
USER_NAME=reki
# ユーザーセッションからDBUS_SESSION_BUS_ADDRESSを取得
export DBUS_SESSION_BUS_ADDRESS=\$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/\$(pgrep -u \$USER_NAME gnome-session | head -n 1)/environ | tr -d '\0' | cut -d= -f2-)

# スキャン結果を読み込み、感染ファイルの数を取得
INFECTED_FILES=()
while IFS= read -r line; do
    if [[ \$line == *"Infected files:"* ]]; then
        count=\$(echo \$line | awk '{print \$3}')
        INFECTED_FILES+=(\$count)
    fi
done < \$SCAN_RESULT_FILE

# 感染ファイル数が1つでも0以外の場合に通知を表示
for infected in "\${INFECTED_FILES[@]}"; do
    if [ "\$infected" -ne 0 ]; then
        ((infected_notzerocount++))
    else
        ((infected_zerocount++))
    fi
done

# 結果を表示
echo "Infected filesが0の場合の回数: \$infected_zerocount"
echo "Infected filesが0以外の場合の回数: \$infected_notzerocount"

if [ "\$infected_notzerocount" -ne 0 ]; then
    dunstify "Virus Alert!" "One or more directories have infected file:\$SCAN_RESULT_FILE."
fi
EOF

#実行権限付与
chmod +x notifyDesktop.sh
#文法チェック、エラーが出れば文法エラー発生。
bash -n notifyDesktop.sh

#rootになる
sudo -i
#systemdのunitファイルの作成
#内容:ユーザ権限で実行する通知用のunitファイルの作成
cat << EOF >> /etc/systemd/system/userreki-notify.service
[Unit]
Description=Run userreki-notify.service after admin-notify.service
After=admin-notify.service

[Service]
Type=oneshot
ExecStart=/home/reki/script/notifyDesktop.sh
User=reki
Environment=WAYLAND_DISPLAY=wayland-0

[Install]
WantedBy=multi-user.target

EOF

exit

#systemdに新しいUnitファイルを認識させるために再読込[reload]させる
sudo systemctl daemon-reload
#サービス有効化
sudo systemctl enable userreki-notify.service
sudo systemctl status userreki-notify.service
sudo systemctl start userreki-notify.service
sudo systemctl status userreki-notify.service

```

- [ ] [README.mdへ戻る](./../README.md)
