# DevelopmentEnvironment

## 目次

- [DevelopmentEnvironment](#developmentenvironment)
  - [目次](#目次)
  - [概要説明](#概要説明)
    - [留意事項](#留意事項)
  - [このProjectのDownload方法](#このprojectのdownload方法)
  - [表紙](#表紙)

## 概要説明

　このProjectは記事の作者が考えをまとめるために家庭用に利用している開発環境の備忘録やマニュアル化を目的として作成しました。

### 留意事項

　このProjectの著作物のあらゆる責任の放棄を宣言します。
　したがってこの記事を参考に設定した内容によるあらゆる利害や訴訟などを起こされても、いかなるものであってもすべての責任は設定を適用・および判断した組織・個人に帰属するものとし、記事の作者自身は無関係とします。
記事の作者はビジネスでやっているわけでも、セキュリティの権威でもありませんため、社会的/民事/刑事的責任は取れません。
また、著作物をどのようなことに利用しても構いませんが、悪用はしてほしくはありませんので悪用はしないようにお願いいたします。
悪用できるようなものを記載しているつもりは一切ございませんが、例えペンでもハサミでも人の良心次第でいかようにもなることを私は知っています。
いかなるものも悪意のある人は悪用できるでしょう。
また、この記事およびこの記事の作者はコメント等もらっても返信はない可能性が高いでしょう。

## このProjectのDownload方法

下記のコマンドを使用してDownloadできる。

```shell
git clone https://gitlab.com/aklak0406/aklak0406_development_environment.git
cd aklak0406_development_environment
```

## 表紙

- [ ] [DebianLinuxOnPromoxVE](./001_DebianLinuxOnPromoxVE/001_Main.md)

- [ ] [GpdPocketOnDebian](./002_GpdPocketOnDebian/001_Main.md)

- [ ] [Dockerコンテナ](./003_DockerContainer/001_Main.md)

- [ ] [X86のPCRouterでPromoxVE](./004_X86RouterOnDebian/001_index.md)  
